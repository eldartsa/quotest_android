package com.oyeapps.quotest.utils;

public class FlavorConsts {
    public static final String DYNAMIC_LINK_DOMAIN_URL_PREFIX = "https://quotest.page.link";
    public static final String DYNAMIC_LINK_BASE_LINK = "https://quotest.oyeapps.com/?invitedby=";
    public static final int ANDROID_APP_MINIMUM_VERSION = 1;
    // TODO: 9/19/2019
    public static final String IOS_APP_PACKAGE_NAME = "com.oyeapps.quotest";
    public static final String IOS_APP_STORE_ID = "123456789";
    public static final String IOS_APP_MINIMUM_VERSION = "1.0.0";

    public static final String COLLECTION_USERS = "UsersQa";
    public static final String COLLECTION_GENERAL = "GeneralQa";
    public static final String SUB_COLLECTION_INVITEES = "invitees";
    public static final String DOCUMENT_GENERAL_DATA = "generalData";
    public static final String DOCUMENT_QUOTES = "quotesData";
    public static final String DOCUMENT_SPECIAL_QUOTES = "specialQuotesData";

    public static final String BASE_IMAGES_URL = "https://yossios.com/quotest/israel/qa";
}
