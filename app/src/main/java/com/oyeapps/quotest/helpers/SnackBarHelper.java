package com.oyeapps.quotest.helpers;

import android.view.View;

import com.google.android.material.snackbar.Snackbar;

public class SnackBarHelper {
    private static final SnackBarHelper ourInstance = new SnackBarHelper();

    public static SnackBarHelper getInstance() {
        return ourInstance;
    }

    private SnackBarHelper() {
    }

    private Snackbar snackbar;

    public void showLongSnackBar(View view, String message) {
        snackbar = Snackbar
                .make(view, message, Snackbar.LENGTH_LONG);

        snackbar.show();
    }

    public void showIndefiniteSnackBar(View view, String message) {
        snackbar = Snackbar
                .make(view, message, Snackbar.LENGTH_INDEFINITE);

        snackbar.show();
    }

    public void dismissSnackBar() {
        snackbar.dismiss();
    }
}
