package com.oyeapps.quotest.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.oyeapps.quotest.app.MyApplication;

public class SharedPreferencesHelper {

    private static final String SP_NAME = "SharedPreferences";

    private static Context getAppContext() {
        return MyApplication.getInstance().getApplicationContext();
    }

    private static SharedPreferences getSharedPreferences() {
        return getAppContext().getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
    }

    public static void putString(String key, String value) {
        getSharedPreferences()
                .edit()
                .putString(key, value)
                .apply();
    }

    public static String getString(String key, String defaultValue) {
        return getSharedPreferences()
                .getString(key, defaultValue);
    }

    public static void putBoolean(String key, boolean value) {
        getSharedPreferences()
                .edit()
                .putBoolean(key, value)
                .apply();
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        return getSharedPreferences()
                .getBoolean(key, defaultValue);
    }
}
