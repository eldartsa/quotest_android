package com.oyeapps.quotest.interfaces;

public interface ILevel {

    int TYPE_LEVEL_OPEN = 0;
    int TYPE_LEVEL_LOCK = 1;
    int TYPE_LEVEL_SOON = 2;
    int TYPE_LEVEL_SPECIALS = 3;
    int TYPE_LEVEL_AD = 4;

    int getType();
}
