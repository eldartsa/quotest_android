package com.oyeapps.quotest.interfaces;

public interface IOnAppTerminationErrorOccurredDialogListener {

    void onTermination();
}
