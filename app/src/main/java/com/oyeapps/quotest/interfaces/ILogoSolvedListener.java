package com.oyeapps.quotest.interfaces;

public interface ILogoSolvedListener {

    void onLogoSolved(int level);
}
