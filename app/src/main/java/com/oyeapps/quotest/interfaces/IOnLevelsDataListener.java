package com.oyeapps.quotest.interfaces;

public interface IOnLevelsDataListener {

    void onSuccess();

    void onFailure();
}
