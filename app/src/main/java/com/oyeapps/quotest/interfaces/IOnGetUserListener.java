package com.oyeapps.quotest.interfaces;

import com.oyeapps.quotest.services.UserInvite;

public interface IOnGetUserListener {

    void onUserExists(UserInvite userInvite);

    void onUserDoesntExist();

    void onFailure();
}
