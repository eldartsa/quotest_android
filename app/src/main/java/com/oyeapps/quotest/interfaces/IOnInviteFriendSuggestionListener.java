package com.oyeapps.quotest.interfaces;

public interface IOnInviteFriendSuggestionListener {

    void onGoToInviteFriendClick();
}
