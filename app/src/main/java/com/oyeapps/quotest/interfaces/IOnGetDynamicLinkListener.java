package com.oyeapps.quotest.interfaces;

import android.net.Uri;

public interface IOnGetDynamicLinkListener {

    void onSuccess(Uri dynamicLink);

    void onFailure();
}
