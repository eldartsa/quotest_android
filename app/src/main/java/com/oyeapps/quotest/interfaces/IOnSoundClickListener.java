package com.oyeapps.quotest.interfaces;

public interface IOnSoundClickListener {

    void onSoundChange(boolean sound);
}
