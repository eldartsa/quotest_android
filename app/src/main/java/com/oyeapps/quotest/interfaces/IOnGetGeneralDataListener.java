package com.oyeapps.quotest.interfaces;

public interface IOnGetGeneralDataListener {

    void onAppMustUpdate(String updateUrl);

    void onDatabaseUpdate(boolean isDatabaseNeedToBeUpdated, boolean isSpecialDbNeedToBeUpdated);

    void onFailure();
}
