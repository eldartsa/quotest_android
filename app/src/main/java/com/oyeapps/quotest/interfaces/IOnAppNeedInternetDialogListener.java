package com.oyeapps.quotest.interfaces;

public interface IOnAppNeedInternetDialogListener {

    void onTryAgain();
}
