package com.oyeapps.quotest.interfaces;

public interface IGetCoins {

    int TYPE_HEADER = 0;
    int TYPE_IN_APP_BILLING = 1;
    int TYPE_NORMAL = 2;
    int TYPE_RANK_US = 3;
    int TYPE_SPECIAL_DEAL = 4;

    int getType();
}
