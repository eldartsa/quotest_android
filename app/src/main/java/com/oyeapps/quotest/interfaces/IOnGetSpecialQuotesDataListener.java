package com.oyeapps.quotest.interfaces;

public interface IOnGetSpecialQuotesDataListener {

    void onSuccess();

    void onFailure();
}
