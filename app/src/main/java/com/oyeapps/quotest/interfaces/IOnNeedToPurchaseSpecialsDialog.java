package com.oyeapps.quotest.interfaces;

public interface IOnNeedToPurchaseSpecialsDialog {

    void onPurchaseAllSpecials();

    void onPurchaseSpecialLevel();
}
