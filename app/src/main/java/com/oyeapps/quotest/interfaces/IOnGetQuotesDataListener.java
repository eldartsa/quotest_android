package com.oyeapps.quotest.interfaces;

public interface IOnGetQuotesDataListener {

    void onSuccess();

    void onFailure();
}
