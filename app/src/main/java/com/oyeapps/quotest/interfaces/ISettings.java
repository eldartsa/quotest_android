package com.oyeapps.quotest.interfaces;

public interface ISettings {

    int TYPE_HEADER = 0;
    int TYPE_NORMAL = 1;

    int getType();
}
