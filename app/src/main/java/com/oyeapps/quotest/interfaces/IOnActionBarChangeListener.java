package com.oyeapps.quotest.interfaces;

import com.oyeapps.quotest.data_obj.ActionBarItem;

public interface IOnActionBarChangeListener {

    void onActionBarStateChange(ActionBarItem actionBarItem);
}
