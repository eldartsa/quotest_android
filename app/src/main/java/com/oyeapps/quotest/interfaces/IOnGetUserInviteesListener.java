package com.oyeapps.quotest.interfaces;

import com.oyeapps.quotest.services.Invitee;

import java.util.List;

public interface IOnGetUserInviteesListener {

    void onSuccess(List<Invitee> inviteeList);

    void onFailure();
}
