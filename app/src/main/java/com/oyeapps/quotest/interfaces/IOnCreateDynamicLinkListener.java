package com.oyeapps.quotest.interfaces;

public interface IOnCreateDynamicLinkListener {

    void onSuccess(String invitationLink);

    void onFailure();
}
