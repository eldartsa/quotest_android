package com.oyeapps.quotest.interfaces;

public interface IOnGameFinishedListener {

    void onAcceptLeaderboard();
}
