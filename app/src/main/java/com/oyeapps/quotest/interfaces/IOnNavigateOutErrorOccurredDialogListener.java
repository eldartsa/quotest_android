package com.oyeapps.quotest.interfaces;

public interface IOnNavigateOutErrorOccurredDialogListener {

    void onNavigateOut();
}
