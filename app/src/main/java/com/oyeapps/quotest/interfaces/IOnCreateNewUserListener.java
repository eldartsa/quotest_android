package com.oyeapps.quotest.interfaces;

public interface IOnCreateNewUserListener {

    void onSuccess();

    void onFailure();
}
