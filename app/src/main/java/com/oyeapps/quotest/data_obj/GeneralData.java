package com.oyeapps.quotest.data_obj;

import java.util.HashMap;

public class GeneralData {

    private boolean isEnteredApp = false;
    private boolean isSoundWork = true;
    private String invitationLink = "";
    private int inviteesCount = 0;
    private String mLastAppVersion;
    private String mDbVersion = "0";
    private String specialDbVersion = "0";
    private String mAndroidUrl = "";
    private int mCoins = 0;
    private long mGameTime = 0;
    private long numOfQuotesBetweenAds = 5;
    private long mNumberOfLevelsToDisplay;
    private boolean isPurchaseSpecials = false;
    private boolean mIsAdsRemoved = false;
    private boolean mIsLikedFacebookPage = false;
    private boolean mIsFollowedOnInstagramPage = false;
    private boolean mIsDownloadLogotest = false;
    private boolean isGameFinished = false;
    private int inviteFriendSuggestionCounter = 0;
    private String instagramPageUsername = "";

    private boolean isShowAllLogosFragmentOnBoarding = false;

    public GeneralData() {
    }

    private HashMap<String, Boolean> mShowPromotion = new HashMap<String, Boolean>() {{
        put("50", false);
        put("150", false);
        put("300", false);
        put("600", false);
    }};

    public boolean isShowAllLogosFragmentOnBoarding() {
        return isShowAllLogosFragmentOnBoarding;
    }

    public void setShowAllLogosFragmentOnBoarding(boolean showAllLogosFragmentOnBoarding) {
        isShowAllLogosFragmentOnBoarding = showAllLogosFragmentOnBoarding;
    }

    public void setGameFinished(boolean gameFinished) {
        isGameFinished = gameFinished;
    }

    public boolean isGameFinished() {
        return isGameFinished;
    }

    public int getInviteesCount() {
        return inviteesCount;
    }

    public void addInviteesToCounter(int inviteesCount) {
        this.inviteesCount += inviteesCount;
    }

    public void resetInviteesCount() {
        this.inviteesCount = 0;
    }

    public boolean isEnteredApp() {
        return isEnteredApp;
    }

    public void setEnteredApp(boolean enteredApp) {
        isEnteredApp = enteredApp;
    }

    public String getInvitationLink() {
        return invitationLink;
    }

    public void setInvitationLink(String invitationLink) {
        this.invitationLink = invitationLink;
    }

    public boolean isSoundWork() {
        return isSoundWork;
    }

    public void setSoundWork(boolean soundWork) {
        isSoundWork = soundWork;
    }

    public void setmShowPromotion(HashMap<String, Boolean> mShowPromotion) {
        this.mShowPromotion = mShowPromotion;
    }

    public HashMap<String, Boolean> getmShowPromotion() {
        return mShowPromotion;
    }

    public long getNumOfQuotesBetweenAds() {
        return numOfQuotesBetweenAds;
    }

    public void setNumOfQuotesBetweenAds(long numOfQuotesBetweenAds) {
        this.numOfQuotesBetweenAds = numOfQuotesBetweenAds;
    }

    public void addGameTime(long time) {
        mGameTime += time;
    }

    public boolean ismIsLikedFacebookPage() {
        return mIsLikedFacebookPage;
    }

    public void setmIsLikedFacebookPage(boolean mIsLikedFacebookPage) {
        this.mIsLikedFacebookPage = mIsLikedFacebookPage;
    }

    public boolean ismIsFollowedOnInstagramPage() {
        return mIsFollowedOnInstagramPage;
    }

    public void setmIsFollowedOnInstagramPage(boolean mIsFollowedOnInstagramPage) {
        this.mIsFollowedOnInstagramPage = mIsFollowedOnInstagramPage;
    }

    public boolean ismIsDownloadLogotest() {
        return mIsDownloadLogotest;
    }

    public void setmIsDownloadLogotest(boolean mIsDownloadLogotest) {
        this.mIsDownloadLogotest = mIsDownloadLogotest;
    }

    public boolean ismIsAdsRemoved() {
        return mIsAdsRemoved;
    }

    public void setmIsAdsRemoved(boolean mIsAdsRemoved) {
        this.mIsAdsRemoved = mIsAdsRemoved;
    }

    public boolean isPurchaseSpecials() {
        return isPurchaseSpecials;
    }

    public void setPurchaseSpecials(boolean purchaseSpecials) {
        this.isPurchaseSpecials = purchaseSpecials;
    }

    public long getmNumberOfLevelsToDisplay() {
        return mNumberOfLevelsToDisplay;
    }

    public void setmNumberOfLevelsToDisplay(long mNumberOfLevelsToDisplay) {
        this.mNumberOfLevelsToDisplay = mNumberOfLevelsToDisplay;
    }

    public String getmLastAppVersion() {
        return mLastAppVersion;
    }

    public void setmLastAppVersion(String mLastAppVersion) {
        this.mLastAppVersion = mLastAppVersion;
    }

    public String getmDbVersion() {
        return mDbVersion;
    }

    public void setmDbVersion(String mDbVersion) {
        this.mDbVersion = mDbVersion;
    }

    public String getSpecialDbVersion() {
        return specialDbVersion;
    }

    public void setSpecialDbVersion(String specialDbVersion) {
        this.specialDbVersion = specialDbVersion;
    }

    public String getmAndroidUrl() {
        return mAndroidUrl;
    }

    public void setmAndroidUrl(String mAndroidUrl) {
        this.mAndroidUrl = mAndroidUrl;
    }

    public int getmCoins() {
        return mCoins;
    }

    public void setmCoins(int mCoins) {
        this.mCoins = mCoins;
    }

    public long getmGameTime() {
        return mGameTime;
    }

    public void setmGameTime(long mGameTime) {
        this.mGameTime = mGameTime;
    }

    public int getInviteFriendSuggestionCounter() {
        return inviteFriendSuggestionCounter;
    }

    public void setInviteFriendSuggestionCounter(int inviteFriendSuggestionCounter) {
        this.inviteFriendSuggestionCounter = inviteFriendSuggestionCounter;
    }

    public String getInstagramPageUsername() {
        return instagramPageUsername;
    }

    public void setInstagramPageUsername(String instagramPageUsername) {
        this.instagramPageUsername = instagramPageUsername;
    }
}
