package com.oyeapps.quotest.data_obj;

import com.oyeapps.quotest.managers.DataManager;

public class ActionBarItem {

    private String title;
    private String coins;
    private boolean shouldShowBack;
    private boolean shouldShowCoins;

    public static ActionBarItem createSimpleActionBar(String title) {
        return new ActionBarItem(title, true, true);
    }

    public static ActionBarItem createSimpleActionNoBack(String title) {
        return new ActionBarItem(title, false, true);
    }

    private ActionBarItem(String title, boolean shouldShowBack, boolean shouldShowCoins) {
        this.title = title;
        this.coins = DataManager.getInstance().getCoinsAmountAsStr();
        this.shouldShowBack = shouldShowBack;
        this.shouldShowCoins = shouldShowCoins;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCoins() {
        return coins;
    }

    public void setCoins(String coins) {
        this.coins = coins;
    }

    public boolean isShouldShowBack() {
        return shouldShowBack;
    }

    public void setShouldShowBack(boolean shouldShowBack) {
        this.shouldShowBack = shouldShowBack;
    }

    public boolean isShouldShowCoins() {
        return shouldShowCoins;
    }

    public void setShouldShowCoins(boolean shouldShowCoins) {
        this.shouldShowCoins = shouldShowCoins;
    }
}
