package com.oyeapps.quotest.extraData;

import android.os.Parcel;
import android.os.Parcelable;

public class ExtraDataItem implements Parcelable {

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public ExtraDataItem() {
    }

    protected ExtraDataItem(Parcel in) {
    }

    public static final Parcelable.Creator<ExtraDataItem> CREATOR = new Parcelable.Creator<ExtraDataItem>() {
        @Override
        public ExtraDataItem createFromParcel(Parcel source) {
            return new ExtraDataItem(source);
        }

        @Override
        public ExtraDataItem[] newArray(int size) {
            return new ExtraDataItem[size];
        }
    };
}
