package com.oyeapps.quotest.extraData;

public class QuoteFragmentExtraData extends ExtraDataItem {

    public enum SourceView {
        LEVELS_FRAGMENT,
        SPECIAL_LEVELS_FRAGMENT
    }

    private SourceView sourceView;
    private int quoteId;
    private int quoteNum;
    private String levelName;

    public QuoteFragmentExtraData(SourceView sourceView, int quoteId, int quoteNum, String levelName) {
        this.sourceView = sourceView;
        this.quoteId = quoteId;
        this.quoteNum = quoteNum;
        this.levelName = levelName;
    }

    public String getLevelName() {
        return levelName;
    }

    public int getQuoteNum() {
        return quoteNum;
    }

    public void setQuoteNum(int quoteNum) {
        this.quoteNum = quoteNum;
    }

    public SourceView getSourceView() {
        return sourceView;
    }

    public void setSourceView(SourceView sourceView) {
        this.sourceView = sourceView;
    }

    public int getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(int quoteId) {
        this.quoteId = quoteId;
    }

}
