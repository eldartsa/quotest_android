package com.oyeapps.quotest.extraData;

public class AllQuotesFragmentExtraData extends ExtraDataItem {

    public enum SourceView {
        LEVELS_FRAGMENT,
        SPECIAL_LEVELS_FRAGMENT
    }

    private SourceView sourceView;
    private int levelNum;
    private String levelName;

    public AllQuotesFragmentExtraData(SourceView sourceView, int levelNum, String levelName) {
        this.sourceView = sourceView;
        this.levelNum = levelNum;
        this.levelName = levelName;
    }

    public SourceView getSourceView() {
        return sourceView;
    }

    public void setSourceView(SourceView sourceView) {
        this.sourceView = sourceView;
    }

    public int getLevelNum() {
        return levelNum;
    }

    public void setLevelNum(int levelNum) {
        this.levelNum = levelNum;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }
}
