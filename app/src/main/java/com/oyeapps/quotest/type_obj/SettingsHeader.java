package com.oyeapps.quotest.type_obj;

import com.oyeapps.quotest.interfaces.ISettings;

public class SettingsHeader implements ISettings {

    private String title = "";

    public String getTitle() {
        return title;
    }

    @Override
    public int getType() {
        return ISettings.TYPE_HEADER;
    }
}
