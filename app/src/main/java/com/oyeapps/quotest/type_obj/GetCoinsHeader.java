package com.oyeapps.quotest.type_obj;

import com.oyeapps.quotest.interfaces.IGetCoins;

public class GetCoinsHeader implements IGetCoins {

    private String title;

    public GetCoinsHeader(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int getType() {
        return IGetCoins.TYPE_HEADER;
    }
}
