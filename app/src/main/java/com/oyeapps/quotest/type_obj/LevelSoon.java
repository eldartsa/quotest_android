package com.oyeapps.quotest.type_obj;

import com.oyeapps.quotest.interfaces.ILevel;

public class LevelSoon implements ILevel {

    private int levelNum;

    public LevelSoon(int levelNum) {
        this.levelNum = levelNum;
    }

    public int getLevelNum() {
        return levelNum;
    }

    @Override
    public int getType() {
        return ILevel.TYPE_LEVEL_SOON;
    }
}
