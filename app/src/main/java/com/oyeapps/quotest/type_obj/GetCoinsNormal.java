package com.oyeapps.quotest.type_obj;

import com.oyeapps.quotest.interfaces.IGetCoins;

public class GetCoinsNormal implements IGetCoins {

    public enum rowType {
        FACEBOOK,
        INSTAGRAM,
        INVITE_FRIEND,
        VIDEO,
        DOWNLOAD_LOGOTEST
    }

    private rowType rowType;

    private int imgRes;
    private String desc;
    private String rewardAmount;
    private int bgColor;
    private boolean isRewarded = false;

    public GetCoinsNormal(int imgRes, String desc, String rewardAmount, int bgColor, rowType rowType) {
        this.imgRes = imgRes;
        this.desc = desc;
        this.rewardAmount = rewardAmount;
        this.bgColor = bgColor;
        this.rowType = rowType;
    }

    public GetCoinsNormal.rowType getRowType() {
        return rowType;
    }

    public void setRowType(GetCoinsNormal.rowType rowType) {
        this.rowType = rowType;
    }

    public int getBgColor() {
        return bgColor;
    }

    public void setBgColor(int bgColor) {
        this.bgColor = bgColor;
    }

    public int getImgRes() {
        return imgRes;
    }

    public void setImgRes(int imgRes) {
        this.imgRes = imgRes;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(String rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public boolean isRewarded() {
        return isRewarded;
    }

    public void setRewarded(boolean rewarded) {
        isRewarded = rewarded;
    }

    @Override
    public int getType() {
        return IGetCoins.TYPE_NORMAL;
    }
}
