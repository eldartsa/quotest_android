package com.oyeapps.quotest.type_obj;

import com.oyeapps.quotest.interfaces.ILevel;

public class LevelSpecials implements ILevel {
    @Override
    public int getType() {
        return TYPE_LEVEL_SPECIALS;
    }
}
