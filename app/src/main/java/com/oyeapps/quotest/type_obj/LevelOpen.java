package com.oyeapps.quotest.type_obj;

import com.oyeapps.quotest.interfaces.ILevel;

public class LevelOpen implements ILevel {

    private int levelNum;
    private int totalSolvedQuotesCount = 0;
    private int totalQuotesCount = 0;

    public int getLevelNum() {
        return levelNum;
    }

    public void setLevelNum(int levelNum) {
        this.levelNum = levelNum;
    }

    public int getTotalSolvedQuotesCount() {
        return totalSolvedQuotesCount;
    }

    public void setTotalSolvedQuotesCount(int totalSolvedQuotesCount) {
        this.totalSolvedQuotesCount = totalSolvedQuotesCount;
    }

    public int getTotalQuotesCount() {
        return totalQuotesCount;
    }

    public void setTotalQuotesCount(int totalQuotesCount) {
        this.totalQuotesCount = totalQuotesCount;
    }

    public boolean isLevelCompleted() {
        return totalQuotesCount == totalSolvedQuotesCount;
    }

    public float getCompletionPercent() {
        return ((float)totalSolvedQuotesCount / totalQuotesCount) * 100;
    }

    @Override
    public int getType() {
        return ILevel.TYPE_LEVEL_OPEN;
    }
}
