package com.oyeapps.quotest.type_obj;

import com.oyeapps.quotest.interfaces.IGetCoins;

public class GetCoinsRankUs implements IGetCoins {

    @Override
    public int getType() {
        return IGetCoins.TYPE_RANK_US;
    }
}
