package com.oyeapps.quotest.type_obj;

import com.oyeapps.quotest.interfaces.IGetCoins;

public class GetCoinsInAppBilling implements IGetCoins {

    private int imgRes;
    private String price;
    private String desc;
    private String productId;
    private boolean isVisible = true;

    public GetCoinsInAppBilling(int imgRes, String price, String desc, String productId) {
        this.imgRes = imgRes;
        this.price = price;
        this.desc = desc;
        this.productId = productId;
    }

    public int getImgRes() {
        return imgRes;
    }

    public void setImgRes(int imgRes) {
        this.imgRes = imgRes;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    @Override
    public int getType() {
        return IGetCoins.TYPE_IN_APP_BILLING;
    }
}
