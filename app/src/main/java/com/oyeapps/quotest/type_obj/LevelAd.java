package com.oyeapps.quotest.type_obj;

import com.oyeapps.quotest.interfaces.ILevel;

public class LevelAd implements ILevel {

    private boolean isAdLoaded = false;

    public boolean isAdLoaded() {
        return isAdLoaded;
    }

    public void setAdLoaded(boolean adLoaded) {
        isAdLoaded = adLoaded;
    }

    public LevelAd() {
    }

    @Override
    public int getType() {
        return ILevel.TYPE_LEVEL_AD;
    }
}
