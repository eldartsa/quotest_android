package com.oyeapps.quotest.type_obj;

import com.oyeapps.quotest.interfaces.ILevel;

public class LevelLock implements ILevel {

    private int levelNum;
    private int logosToOpen = 0;

    public int getLevelNum() {
        return levelNum;
    }

    public void setLevelNum(int levelNum) {
        this.levelNum = levelNum;
    }

    public int getLogosToOpen() {
        return logosToOpen;
    }

    public void setLogosToOpen(int logosToOpen) {
        this.logosToOpen = logosToOpen;
    }

    @Override
    public int getType() {
        return ILevel.TYPE_LEVEL_LOCK;
    }
}
