package com.oyeapps.quotest.type_obj;

import com.oyeapps.quotest.interfaces.ISettings;

public class SettingsNormal implements ISettings {

    public enum RowType {
        SOUNDS,
        RESET_GAME,
        STATISTICS,
        PRIVACY_POLICY,
        TOKEN
    }

    private RowType rowType;
    private String title;
    private int iconRes = 0;
    private boolean shouldShowIcon = false;

    public SettingsNormal(String title, RowType rowType) {
        this.title = title;
        this.rowType = rowType;
    }

    public RowType getRowType() {
        return rowType;
    }

    public void setRowType(RowType rowType) {
        this.rowType = rowType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIconRes() {
        return iconRes;
    }

    public void setIconRes(int iconRes) {
        this.iconRes = iconRes;
    }

    public boolean isShouldShowIcon() {
        return shouldShowIcon;
    }

    public void setShouldShowIcon(boolean shouldShowIcon) {
        this.shouldShowIcon = shouldShowIcon;
    }

    @Override
    public int getType() {
        return ISettings.TYPE_NORMAL;
    }
}
