package com.oyeapps.quotest.type_obj;

import com.oyeapps.quotest.interfaces.IGetCoins;
import com.oyeapps.quotest.managers.PurchasesManager;

import static com.oyeapps.quotest.managers.PurchasesManager.SKU_PURCHASE_SPECIALS;

public class GetCoinsSpecialDeal implements IGetCoins {

    private String price;
    private boolean isVisible = true;

    @Override
    public int getType() {
        return IGetCoins.TYPE_SPECIAL_DEAL;
    }

    public GetCoinsSpecialDeal() {
        price = PurchasesManager.Product.getPriceForProductId(SKU_PURCHASE_SPECIALS);
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }
}


