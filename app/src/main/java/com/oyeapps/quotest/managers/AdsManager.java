package com.oyeapps.quotest.managers;

import android.content.Context;
import android.graphics.Color;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.oyeapps.quotest.BuildConfig;
import com.oyeapps.quotest.R;
import com.oyeapps.quotest.activities.BaseActivity;
import com.oyeapps.quotest.utils.Logger;

import java.lang.ref.WeakReference;

import static com.oyeapps.quotest.utils.Consts.FREE_COINS_EVENT_NAME;
import static com.oyeapps.quotest.utils.Consts.FREE_COINS_REWARD_VIDEO_PARAMETER_VALUE;
import static com.oyeapps.quotest.utils.Consts.REWARD_VIDEO_AMOUNT;
import static com.oyeapps.quotest.utils.Consts.SOUND_REWARD;

public class AdsManager {

    public interface GetCoinsRewardListener {
        void onAdClosed();

        void onAdFailedToLoad();
    }

    public interface IOnNativeAdListener {
        void onAdLoaded(UnifiedNativeAd unifiedNativeAd);
    }

    private WeakReference<GetCoinsRewardListener> mListener;

    private static AdsManager instance = null;

    public static synchronized AdsManager getInstance() {
        if (instance == null) {
            instance = new AdsManager();
        }
        return instance;
    }

    private int count = 0;

    public void loadBannerAd(final Context context, final AdView adView) {
        try {
//            AdSize adSize = AdSize.SMART_BANNER;
//            adView.getLayoutParams().width = adSize.getWidthInPixels(context);
//            adView.getLayoutParams().height = adSize.getHeightInPixels(context);
            String[] testDevicesArray = context.getResources().getStringArray(R.array.test_devices);
            final AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
            if (BuildConfig.DEBUG) {
                for (String aTestDevicesArray : testDevicesArray) {
                    adRequestBuilder.addTestDevice(aTestDevicesArray);
                }
            }
            adView.setBackgroundColor(Color.BLACK);
            adView.loadAd(adRequestBuilder.build());
            adView.setVisibility(View.VISIBLE);

            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    count = 0;
                    Logger.d(Logger.TEST, AdsManager.class.getSimpleName(), "Banner has loaded successfully");
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    try {
                        Logger.d(Logger.TEST, AdsManager.class.getSimpleName(), "Banner failed to load");
                        if (count < 3) {
                            loadBannerAd(context, adView);
                            count++;
                        } else {
                            count = 0;
                        }
                    } catch (Exception e) {
                        Logger.e(Logger.TEST, e.getMessage(), e);
                    }
                }
            });
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void setRewardedVideoAd(final RewardedVideoAd rewardedVideoAd, final String rewardAdUnitId) {
        try {
            rewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
                @Override
                public void onRewardedVideoAdLoaded() {
                }

                @Override
                public void onRewardedVideoAdOpened() {
                }

                @Override
                public void onRewardedVideoStarted() {
                }

                @Override
                public void onRewardedVideoAdClosed() {
                    try {
                        loadGetCoinsRewardVideo(rewardedVideoAd, rewardAdUnitId);
                        if (mListener.get() != null)
                            mListener.get().onAdClosed();
                    } catch (Exception e) {
                        Logger.e(Logger.TEST, e.getMessage(), e);
                    }
                }

                @Override
                public void onRewarded(RewardItem rewardItem) {
                    try {
                        rewardPlayer();
                    } catch (Exception e) {
                        Logger.e(Logger.TEST, e.getMessage(), e);
                    }
                }

                @Override
                public void onRewardedVideoAdLeftApplication() {
                }

                @Override
                public void onRewardedVideoAdFailedToLoad(int i) {
                    try {
                        loadGetCoinsRewardVideo(rewardedVideoAd, rewardAdUnitId);
                    } catch (Exception e) {
                        Logger.e(Logger.TEST, e.getMessage(), e);
                    }
                }

                @Override
                public void onRewardedVideoCompleted() {
                }
            });
            loadGetCoinsRewardVideo(rewardedVideoAd, rewardAdUnitId);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void setmListener(GetCoinsRewardListener listener) {
        mListener = new WeakReference<>(listener);
    }

    private void loadGetCoinsRewardVideo(RewardedVideoAd mGetCoinsRewardVideoAd, String rewardAdUnitId) {
        try {
            AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
            if (BuildConfig.DEBUG) {
                mGetCoinsRewardVideoAd.loadAd("ca-app-pub-3940256099942544/5224354917", adRequestBuilder.build());
            } else
                mGetCoinsRewardVideoAd.loadAd(rewardAdUnitId, adRequestBuilder.build());
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void showGetCoinsRewardVideo(RewardedVideoAd rewardedVideoAd, String rewardAdUnitId) {
        try {
            if (rewardedVideoAd.isLoaded()) {
                rewardedVideoAd.show();
            } else {
                loadGetCoinsRewardVideo(rewardedVideoAd, rewardAdUnitId);
                if (mListener.get() != null) {
                    mListener.get().onAdFailedToLoad();
                }
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void rewardPlayer() {
        SoundManager.getInstance().playSound(SOUND_REWARD);
        DataManager.getInstance().increaseCoinsByAmount(REWARD_VIDEO_AMOUNT);
        FirebaseManager.getInstance().reportEvent(FREE_COINS_EVENT_NAME, FREE_COINS_REWARD_VIDEO_PARAMETER_VALUE);
    }

    public void loadLogosActivityInterstitialAd(final Context context, final InterstitialAd interstitialAd) {
        try {
            if (toShowInterstitialAds() && !interstitialAd.isLoaded()) {
                final String[] testDevicesArray = context.getResources().getStringArray(R.array.test_devices);
                AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
                for (String aTestDevicesArray : testDevicesArray) {
                    adRequestBuilder.addTestDevice(aTestDevicesArray);
                }
                interstitialAd.loadAd(adRequestBuilder.build());
                interstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        try {
                            loadLogosActivityInterstitialAd(context, interstitialAd);
                        } catch (Exception e) {
                            Logger.e(Logger.TEST, e.getMessage(), e);
                        }
                    }
                });
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void showLogosActivityInterstitialAd(InterstitialAd interstitialAd, boolean isRunning) {
        try {
            if (!toShowInterstitialAds())
                return;
            if (toShowLogosActivityInterstitialAd()) {
                if (isRunning) {
                    if (interstitialAd.isLoaded()) {
                        interstitialAd.show();
                    } else {
                        Logger.d(Logger.TEST, getClass().getSimpleName(), "The interstitial ad has not loaded yet.");
                    }
                }
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void loadFastGameInterstitialAd(final Context context, final InterstitialAd interstitialAd) {
        try {
            if (toShowInterstitialAds() && !interstitialAd.isLoaded()) {
                final String[] testDevicesArray = context.getResources().getStringArray(R.array.test_devices);
                AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
                for (String aTestDevicesArray : testDevicesArray) {
                    adRequestBuilder.addTestDevice(aTestDevicesArray);
                }
                interstitialAd.loadAd(adRequestBuilder.build());
                interstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        try {
                            loadFastGameInterstitialAd(context, interstitialAd);
                        } catch (Exception e) {
                            Logger.e(Logger.TEST, e.getMessage(), e);
                        }
                    }
                });
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void showFastGameInterstitialAd(InterstitialAd interstitialAd, boolean isRunning) {
        try {
            if (!toShowInterstitialAds())
                return;
            if (isRunning) {
                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                } else {
                    Logger.d(Logger.TEST, getClass().getSimpleName(), "The interstitial has not loaded yet.");
                }
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private boolean toShowInterstitialAds() {
        try {
            if (DataManager.getInstance().isPurchaseSpecials()) {
                return false;
            } else return !DataManager.getInstance().isAdsRemoved();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
            return true;
        }
    }

    private int quoteFragmentCreatedCount = 0;

    private boolean toShowLogosActivityInterstitialAd() {
        try {
            long numOfQuotesToShowAd = DataManager.getInstance().getNumOfQuotesBetweenAds();
            quoteFragmentCreatedCount++;
            Logger.d(Logger.TEST, getClass().getSimpleName(),
                    "Logos entered: " + quoteFragmentCreatedCount + " | Ad every " + numOfQuotesToShowAd + " quotes");
            return quoteFragmentCreatedCount % numOfQuotesToShowAd == 0;
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
            return false;
        }
    }

    public void loadLevelsNativeAd(BaseActivity baseActivity, final IOnNativeAdListener iOnNativeAdListener) {
        MobileAds.initialize(baseActivity, baseActivity.getString(R.string.admob_app_id));
        AdLoader.Builder builder = new AdLoader.Builder(
                baseActivity,
                baseActivity.getString(BuildConfig.DEBUG ? R.string.native_ad_levels_test : R.string.native_ad_levels));
        builder.forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
            @Override
            public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                iOnNativeAdListener.onAdLoaded(unifiedNativeAd);
            }
        });
    }
}

