package com.oyeapps.quotest.managers;

import com.oyeapps.quotest.app.MyApplication;
import com.oyeapps.quotest.data_obj.GeneralData;
import com.oyeapps.quotest.widgets.ComplexPreferences;

import static android.content.Context.MODE_PRIVATE;
import static com.oyeapps.quotest.utils.Consts.GENERAL_DATA_KEY;
import static com.oyeapps.quotest.utils.Consts.MY_PREFERENCES;

public class UserAccountManager {

    private static UserAccountManager instance = null;

    public static UserAccountManager getInstance() {
        if (instance == null) {
            instance = new UserAccountManager();
        }
        return instance;
    }

    public GeneralData getGeneral() {
        ComplexPreferences complexPreferences =
                ComplexPreferences.getComplexPreferences(MyApplication.getInstance().getContext(), MY_PREFERENCES, MODE_PRIVATE);
        return complexPreferences.getObject(GENERAL_DATA_KEY, GeneralData.class);
    }

    public void updateGeneral(GeneralData mGeneral) {
        ComplexPreferences complexPreferences =
                ComplexPreferences.getComplexPreferences(MyApplication.getInstance().getContext(), MY_PREFERENCES, MODE_PRIVATE);
        complexPreferences.putObject(GENERAL_DATA_KEY, mGeneral);
        complexPreferences.commit();
    }
}
