package com.oyeapps.quotest.managers;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.Constants;
import com.anjlab.android.iab.v3.SkuDetails;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.oyeapps.quotest.R;
import com.oyeapps.quotest.activities.BaseActivity;
import com.oyeapps.quotest.app.MyApplication;
import com.oyeapps.quotest.database.MyDatabase;
import com.oyeapps.quotest.type_obj.GetCoinsInAppBilling;
import com.oyeapps.quotest.type_obj.GetCoinsSpecialDeal;
import com.oyeapps.quotest.utils.Consts;
import com.oyeapps.quotest.utils.Logger;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.realm.Realm;

import static com.oyeapps.quotest.utils.Consts.PURCHASE_REMOVE_ADS_EVENT_NAME;
import static com.oyeapps.quotest.utils.Consts.PURCHASE_REMOVE_ADS_EVENT_PARAMETER_NAME;
import static com.oyeapps.quotest.utils.Consts.PURCHASE_SUBSCRIPTION_EVENT_PARAMETER_NAME;
import static com.oyeapps.quotest.utils.Consts.SOUND_REWARD;

public class PurchasesManager implements BillingProcessor.IBillingHandler {

    private static final String SKU_PURCHASE_SPECIAL_LEVEL_START = "purchase_special_level";
    private static final String SKU_TEST = "android.test.purchased";

    public enum Product {
        DEFAULT_PRODUCT("", IMG_RES_DEFAULT, DEFAULT_REWARD_AMOUNT, R.string.default_purchase_price, R.string.default_desc),
        PURCHASE_SPECIALS(SKU_PURCHASE_SPECIALS, 0, REWARD_AMOUNT_SKU_PURCHASE_SPECIALS, R.string.purchase_specials_default_price, 0),
        REMOVE_ADS(SKU_REMOVE_ADS, IMG_RES_SKU_REMOVE_ADS, DEFAULT_REWARD_AMOUNT, R.string.remove_ads_price, R.string.remove_ads),
        PURCHASE1(SKU_PURCHASE1, IMG_RES_SKU_PURCHASE1, REWARD_AMOUNT_SKU_PURCHASE1, R.string.purchase1_price, R.string.purchase1_reward_amount),
        PURCHASE2(SKU_PURCHASE2, IMG_RES_SKU_PURCHASE2, REWARD_AMOUNT_SKU_PURCHASE2, R.string.purchase2_price, R.string.purchase2_reward_amount),
        PURCHASE3(SKU_PURCHASE3, IMG_RES_SKU_PURCHASE3, REWARD_AMOUNT_SKU_PURCHASE3, R.string.purchase3_price, R.string.purchase3_reward_amount),
        PURCHASE4(SKU_PURCHASE4, IMG_RES_SKU_PURCHASE4, REWARD_AMOUNT_SKU_PURCHASE4, R.string.purchase4_price, R.string.purchase4_reward_amount);

        private String productId;
        private int imgRes;
        private int rewardAmount;
        private int defaultPriceStrRes;
        private int descStrRes;

        Product(String productId, int imgRes, int rewardAmount, int defaultPriceStrRes, int descStrRes) {
            this.productId = productId;
            this.imgRes = imgRes;
            this.rewardAmount = rewardAmount;
            this.defaultPriceStrRes = defaultPriceStrRes;
            this.descStrRes = descStrRes;
        }

        private static int getImgResForProductId(String productId) {
            for (Product product : values()) {
                if (product.productId.equals(productId))
                    return product.imgRes;
            }
            return DEFAULT_PRODUCT.imgRes;
        }

        private static int getRewardAmountForProductId(String productId) {
            for (Product product : values()) {
                if (product.productId.equals(productId))
                    return product.rewardAmount;
            }
            return DEFAULT_PRODUCT.rewardAmount;
        }

        public static String getPriceForProductId(String productId) {
            Context context = MyApplication.getInstance().getContext();
            for (Product product : values()) {
                if (product.productId.equals(productId))
                    return context.getString(product.defaultPriceStrRes);
            }
            return context.getString(DEFAULT_PRODUCT.defaultPriceStrRes);
        }

        public static String getDescForProductId(String productId) {
            Context context = MyApplication.getInstance().getContext();
            for (Product product : values()) {
                if (product.productId.equals(productId))
                    return context.getString(product.descStrRes);
            }
            return context.getString(DEFAULT_PRODUCT.descStrRes);
        }

        private static ArrayList<String> getListOfProductListings() {
            return new ArrayList<>(Arrays.asList(
                    REMOVE_ADS.productId,
                    PURCHASE1.productId,
                    PURCHASE2.productId,
                    PURCHASE3.productId,
                    PURCHASE4.productId,
                    PURCHASE_SPECIALS.productId));
        }
    }

    public static final String SKU_PURCHASE_SPECIALS = "specials_pack";
    public static final String SKU_REMOVE_ADS = "remove_ads";
    private static final String SKU_PURCHASE1 = "purchase1";
    private static final String SKU_PURCHASE2 = "purchase2";
    private static final String SKU_PURCHASE3 = "purchase3";
    private static final String SKU_PURCHASE4 = "purchase4";
    private static final int DEFAULT_REWARD_AMOUNT = 0;
    private static final int REWARD_AMOUNT_SKU_PURCHASE1 = 6000;
    private static final int REWARD_AMOUNT_SKU_PURCHASE2 = 50000;
    private static final int REWARD_AMOUNT_SKU_PURCHASE3 = 110000;
    private static final int REWARD_AMOUNT_SKU_PURCHASE4 = 1000000;
    private static final int REWARD_AMOUNT_SKU_PURCHASE_SPECIALS = 20000;
    private static final int IMG_RES_DEFAULT = 0;
    private static final int IMG_RES_SKU_REMOVE_ADS = R.drawable.ic_remove_ads;
    private static final int IMG_RES_SKU_PURCHASE1 = R.drawable.ic_purchase1;
    private static final int IMG_RES_SKU_PURCHASE2 = R.drawable.ic_purchase2;
    private static final int IMG_RES_SKU_PURCHASE3 = R.drawable.ic_purchase3;
    private static final int IMG_RES_SKU_PURCHASE4 = R.drawable.ic_purchase4;

    private BillingProcessor bp;

    private Consts.PurchaseSubscriptionFrom purchaseSubscriptionFrom;

    private List<GetCoinsInAppBilling> getCoinsInAppBillingList = new ArrayList<>();
    private GetCoinsSpecialDeal getCoinsSpecialDeal = new GetCoinsSpecialDeal();

    public interface IOnPurchasesListener {
        void onRemoveAdsPurchased();

        void onSpecialsPurchased();

        void onSpecialLevelPurchased(int levelId, String levelName);

        void onBillingError(int errorCode);
    }

    private WeakReference<IOnPurchasesListener> iOnPurchasesListenerWeakReference;

    private PurchasesManager() {
    }

    private static PurchasesManager instance = null;

    public static PurchasesManager getInstance() {
        if (instance == null) {
            instance = new PurchasesManager();
        }
        return instance;
    }

    public void initBillingProcessor(Context context) {
        Logger.d(Logger.TEST, getClass().getSimpleName(), "init billing processor");
        bp = new BillingProcessor(context, context.getString(R.string.in_app_billing_license_key), this);
        bp.initialize();
    }

    public void setListener(IOnPurchasesListener listener) {
        this.iOnPurchasesListenerWeakReference = new WeakReference<>(listener);
    }

    public void consumeProduct(BaseActivity baseActivity, String productId) {
        try {
            bp.consumePurchase(productId);
            bp.purchase(baseActivity, productId);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void purchaseRemoveAds(BaseActivity baseActivity) {
        try {
            this.sku = SKU_REMOVE_ADS;
            bp.purchase(baseActivity, sku);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void purchaseSpecials(Activity activity, Consts.PurchaseSubscriptionFrom purchaseSubscriptionFrom) {
        try {
            this.sku = SKU_PURCHASE_SPECIALS;
            this.purchaseSubscriptionFrom = purchaseSubscriptionFrom;
            bp.purchase(activity, sku);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private int levelId = -1;
    private String levelName = "";
    private String sku = "";
    private Realm realm;

    public void purchaseSpecialLevel(BaseActivity baseActivity, int levelId, String levelName, Realm realm) {
        try {
            this.sku = SKU_PURCHASE_SPECIAL_LEVEL_START + levelId;
            this.levelId = levelId;
            this.levelName = levelName;
            this.realm = realm;
            bp.purchase(baseActivity, sku);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void userHasHistoryRemoveAdsPurchase(boolean notifyView) {
        try {
            DataManager.getInstance().setAdsRemoved();
            if (notifyView && iOnPurchasesListenerWeakReference.get() != null)
                iOnPurchasesListenerWeakReference.get().onRemoveAdsPurchased();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void userHasHistorySpecialsPurchase(boolean notifyView) {
        try {
            DataManager.getInstance().setPurchaseSpecials(true);
            if (notifyView && iOnPurchasesListenerWeakReference.get() != null)
                iOnPurchasesListenerWeakReference.get().onSpecialsPurchased();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void userHasHistoryOfSpecialLevelPurchase(boolean notifyView) {
        try {
            MyDatabase.getInstance().setSpecialLevelPurchased(realm, levelId);
            if (notifyView && iOnPurchasesListenerWeakReference.get() != null)
                iOnPurchasesListenerWeakReference.get().onSpecialLevelPurchased(levelId, levelName);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void userPurchasedRemoveAds() {
        try {
            SoundManager.getInstance().playSound(SOUND_REWARD);
            DataManager.getInstance().setAdsRemoved();
            FirebaseManager.getInstance().reportEvent(PURCHASE_REMOVE_ADS_EVENT_NAME, PURCHASE_REMOVE_ADS_EVENT_PARAMETER_NAME, Consts.PURCHASE_REMOVE_ADS_EVENT_PURCHASE_FROM_PARAMETER_VALUES.get(Consts.PurchaseRemoveAdsFrom.MAIN_VIEW));

            if (iOnPurchasesListenerWeakReference.get() != null)
                iOnPurchasesListenerWeakReference.get().onRemoveAdsPurchased();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void userPurchasedSpecials() {
        try {
            SoundManager.getInstance().playSound(SOUND_REWARD);
            DataManager.getInstance().increaseCoinsByAmount(REWARD_AMOUNT_SKU_PURCHASE_SPECIALS);
            DataManager.getInstance().setPurchaseSpecials(true);
            FirebaseManager.getInstance().reportEvent(PURCHASE_SUBSCRIPTION_EVENT_PARAMETER_NAME,
                    PURCHASE_SUBSCRIPTION_EVENT_PARAMETER_NAME, Consts.PURCHASE_SUBSCRIPTION_EVENT_PURCHASE_FROM_PARAMETER_VALUES.get(purchaseSubscriptionFrom));
            if (iOnPurchasesListenerWeakReference.get() != null)
                iOnPurchasesListenerWeakReference.get().onSpecialsPurchased();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void userPurchasedSpecialLevel() {
        SoundManager.getInstance().playSound(SOUND_REWARD);
        if (iOnPurchasesListenerWeakReference.get() != null)
            iOnPurchasesListenerWeakReference.get().onSpecialLevelPurchased(levelId, levelName);
    }

    private void userConsumedProduct(int coins) {
        try {
            SoundManager.getInstance().playSound(SOUND_REWARD);
            DataManager.getInstance().increaseCoinsByAmount(coins);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void initPurchasesData() {
        getCoinsInAppBillingList.clear();
        for (String productId : Product.getListOfProductListings()) {
            if (!productId.equals(SKU_PURCHASE_SPECIALS)) {
                GetCoinsInAppBilling getCoinsInAppBilling =
                        new GetCoinsInAppBilling(
                                Product.getImgResForProductId(productId),
                                Product.getPriceForProductId(productId),
                                Product.getDescForProductId(productId),
                                productId);
                getCoinsInAppBillingList.add(getCoinsInAppBilling);
            }
        }
    }

    List<GetCoinsInAppBilling> getPurchasesListingsDetails() {
        initPurchasesData();
        List<SkuDetails> skuDetailsList = bp.getPurchaseListingDetails(Product.getListOfProductListings());
        if (skuDetailsList != null && !skuDetailsList.isEmpty()) {
            for (GetCoinsInAppBilling getCoinsInAppBilling : getCoinsInAppBillingList) {
                String productId = getCoinsInAppBilling.getProductId();
                for (SkuDetails skuDetails : skuDetailsList) {
                    if (skuDetails.productId.equals(productId)) {
                        getCoinsInAppBilling.setPrice(skuDetails.priceText);
                    }
                }
                if (productId.equals(SKU_REMOVE_ADS) && (DataManager.getInstance().isAdsRemoved() || DataManager.getInstance().isPurchaseSpecials())) {
                    getCoinsInAppBilling.setVisible(false);
                }
            }
            for (SkuDetails skuDetails : skuDetailsList) {
                if (skuDetails.productId.equals(SKU_PURCHASE_SPECIALS)) {
                    getCoinsSpecialDeal.setPrice(skuDetails.priceText);
                }
            }
            if (DataManager.getInstance().isPurchaseSpecials()) {
                getCoinsSpecialDeal.setVisible(false);
            }
        }
        return getCoinsInAppBillingList;
    }

    public GetCoinsSpecialDeal getGetCoinsSpecialDeal() {
        return getCoinsSpecialDeal;
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        try {
            switch (productId) {
                case SKU_PURCHASE1:
                case SKU_PURCHASE2:
                case SKU_PURCHASE3:
                case SKU_PURCHASE4:
                    userConsumedProduct(Product.getRewardAmountForProductId(productId));
                    break;
                case SKU_REMOVE_ADS:
                    userPurchasedRemoveAds();
                    break;
                case SKU_PURCHASE_SPECIALS:
                    userPurchasedSpecials();
                    break;
                default:
                    if (productId.startsWith(SKU_PURCHASE_SPECIAL_LEVEL_START)) {
                        userPurchasedSpecialLevel();
                    }
                    break;
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        try {
            if (errorCode == Constants.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED) {
                switch (sku) {
                    case SKU_REMOVE_ADS:
                        if (bp.isPurchased(SKU_REMOVE_ADS))
                            userHasHistoryRemoveAdsPurchase(true);
                        break;
                    case SKU_PURCHASE_SPECIALS:
                        if (bp.isPurchased(SKU_PURCHASE_SPECIALS))
                            userHasHistorySpecialsPurchase(true);
                        break;
                    default:
                        if (sku.startsWith(SKU_PURCHASE_SPECIAL_LEVEL_START) && bp.isPurchased(sku)) {
                            userHasHistoryOfSpecialLevelPurchase(true);
                        }
                        break;
                }
            }
            if (iOnPurchasesListenerWeakReference.get() != null)
                iOnPurchasesListenerWeakReference.get().onBillingError(errorCode);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @Override
    public void onBillingInitialized() {
        try {
            if (bp != null) {
                bp.loadOwnedPurchasesFromGoogle();
                if (!DataManager.getInstance().isPurchaseSpecials() && bp.isPurchased(SKU_PURCHASE_SPECIALS)) {
                    userHasHistorySpecialsPurchase(false);
                }
                if (!DataManager.getInstance().isAdsRemoved() && bp.isPurchased(SKU_REMOVE_ADS)) {
                    userHasHistoryRemoveAdsPurchase(false);
                }
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void releaseBillingProcessor() {
        try {
            if (bp != null) {
                Logger.d(Logger.TEST, getClass().getSimpleName(), "Release billing processor");
                bp.release();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public BillingProcessor getBp() {
        return bp;
    }
}
