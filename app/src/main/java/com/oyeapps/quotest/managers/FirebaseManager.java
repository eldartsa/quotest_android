package com.oyeapps.quotest.managers;

import android.app.Activity;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.oyeapps.quotest.app.MyApplication;

public class FirebaseManager {

    private static FirebaseManager instance = null;
    private FirebaseAnalytics mFirebaseAnalytics;

    public static FirebaseManager getInstance() {
        if (instance == null) {
            instance = new FirebaseManager();
        }
        return instance;
    }

    private FirebaseManager() {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(MyApplication.getInstance().getContext());
    }

    public void reportEvent(String event, String itemName) {
        Bundle param = new Bundle();
        param.putString(FirebaseAnalytics.Param.ITEM_NAME, itemName);
        mFirebaseAnalytics.logEvent(event, param);
    }

    public void reportEvent(String event, String itemName, String item) {
        Bundle param = new Bundle();
        param.putString(FirebaseAnalytics.Param.ITEM_NAME, itemName);
        param.putString(itemName, item);
        mFirebaseAnalytics.logEvent(event, param);
    }

    public void reportViewEntrance(Activity activity, String screenName) {
        mFirebaseAnalytics.setCurrentScreen(activity, screenName, screenName);
    }
}
