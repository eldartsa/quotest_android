package com.oyeapps.quotest.managers;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.widget.Toast;

import androidx.core.content.res.ResourcesCompat;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.oyeapps.quotest.BuildConfig;
import com.oyeapps.quotest.R;
import com.oyeapps.quotest.activities.BaseActivity;
import com.oyeapps.quotest.database.MyDatabase;
import com.oyeapps.quotest.database.SpecialLevel;
import com.oyeapps.quotest.dialogs.PurchaseSpecialsDialog;
import com.oyeapps.quotest.interfaces.IGetCoins;
import com.oyeapps.quotest.interfaces.ILevel;
import com.oyeapps.quotest.interfaces.IOnAppNeedInternetDialogListener;
import com.oyeapps.quotest.interfaces.IOnCreateDynamicLinkListener;
import com.oyeapps.quotest.interfaces.IOnCreateNewUserListener;
import com.oyeapps.quotest.interfaces.IOnGetDynamicLinkListener;
import com.oyeapps.quotest.interfaces.IOnGetGeneralDataListener;
import com.oyeapps.quotest.interfaces.IOnGetQuotesDataListener;
import com.oyeapps.quotest.interfaces.IOnGetSpecialQuotesDataListener;
import com.oyeapps.quotest.interfaces.IOnGetUserInviteesListener;
import com.oyeapps.quotest.interfaces.IOnGetUserListener;
import com.oyeapps.quotest.interfaces.IOnInviteFriendSuggestionListener;
import com.oyeapps.quotest.interfaces.IOnSoundClickListener;
import com.oyeapps.quotest.interfaces.ISettings;
import com.oyeapps.quotest.services.Invitee;
import com.oyeapps.quotest.services.UserInvite;
import com.oyeapps.quotest.type_obj.GetCoinsHeader;
import com.oyeapps.quotest.type_obj.GetCoinsInAppBilling;
import com.oyeapps.quotest.type_obj.GetCoinsNormal;
import com.oyeapps.quotest.type_obj.GetCoinsRankUs;
import com.oyeapps.quotest.type_obj.LevelAd;
import com.oyeapps.quotest.type_obj.LevelLock;
import com.oyeapps.quotest.type_obj.LevelOpen;
import com.oyeapps.quotest.type_obj.LevelSoon;
import com.oyeapps.quotest.type_obj.LevelSpecials;
import com.oyeapps.quotest.type_obj.SettingsNormal;
import com.oyeapps.quotest.utils.Consts;
import com.oyeapps.quotest.utils.DialogUtils;
import com.oyeapps.quotest.utils.Logger;
import com.oyeapps.quotest.utils.MyUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.realm.Realm;

import static com.oyeapps.quotest.utils.Consts.FREE_COINS_DOWNLOAD_LOGOTEST_PARAMETER_VALUE;
import static com.oyeapps.quotest.utils.Consts.FREE_COINS_EVENT_NAME;
import static com.oyeapps.quotest.utils.Consts.FREE_COINS_FACEBOOK_PARAMETER_VALUE;
import static com.oyeapps.quotest.utils.Consts.FREE_COINS_INSTAGRAM_PARAMETER_VALUE;
import static com.oyeapps.quotest.utils.Consts.LOGOTEST_APP_PACKAGE_NAME;
import static com.oyeapps.quotest.utils.Consts.OPEN_LEVEL_COMPLETION_PERCENT;
import static com.oyeapps.quotest.utils.Consts.REWARD_DOWNLOAD_LOGOTEST_APP_AMOUNT;
import static com.oyeapps.quotest.utils.Consts.REWARD_FACEBOOK_LIKE_AMOUNT;
import static com.oyeapps.quotest.utils.Consts.REWARD_INSTAGRAM_FOLLOW_AMOUNT;
import static com.oyeapps.quotest.utils.Consts.REWARD_INVITE_FRIEND_INVITEE_AMOUNT;
import static com.oyeapps.quotest.utils.Consts.SOUND_CHEERS;
import static com.oyeapps.quotest.utils.Consts.SOUND_REWARD;

public class GeneralManager {

    private static GeneralManager instance = null;

    public synchronized static GeneralManager getInstance() {
        if (instance == null) {
            instance = new GeneralManager();
        }
        return instance;
    }

    public void goToInviteFriendView(final BaseActivity baseActivity, final boolean isGoToScreenNav) {
        if (!isGoToScreenNav) baseActivity.showLoader();
        DataManager.getInstance().createDynamicLink(baseActivity, new IOnCreateDynamicLinkListener() {
            @Override
            public void onSuccess(String invitationLink) {
                if (!isGoToScreenNav) baseActivity.dismissLoader();
                DataManager.getInstance().setInvitationLink(invitationLink);
                NavigationManager.getInstance().goToInviteFriendActivity(baseActivity);
            }

            @Override
            public void onFailure() {
                if (!isGoToScreenNav) baseActivity.dismissLoader();
                String invitationLink = DataManager.getInstance().getInvitationLink();
                if (invitationLink.isEmpty()) {
                    if (isGoToScreenNav) {
                        NavigationManager.getInstance().goToMainActivity(baseActivity);
                    } else {
                        DialogUtils.showNetworkErrorOccurredDialog(baseActivity);
                    }
                } else {
                    NavigationManager.getInstance().goToInviteFriendActivity(baseActivity);
                }
            }
        });
    }

    private void deepLinkRewardCheck(final BaseActivity baseActivity) {
        if (!DataManager.getInstance().isEnteredApp()) {
            DataManager.getInstance().getDynamicLink(baseActivity, new IOnGetDynamicLinkListener() {
                @Override
                public void onSuccess(Uri dynamicLink) {
                    final String referrerUid = dynamicLink.getQueryParameter("invitedby");
                    DataManager.getInstance().getUser(MyUtils.getDeviceId(baseActivity), new IOnGetUserListener() {
                        @Override
                        public void onUserExists(UserInvite userInvite) {
                            NavigationManager.getInstance().handleAfterSplashNavigation(baseActivity);
                        }

                        @Override
                        public void onUserDoesntExist() {
                            UserInvite newUser = new UserInvite();
                            newUser.setReferred_by(referrerUid);
                            newUser.setUdid(MyUtils.getDeviceId(baseActivity));
                            DataManager.getInstance().createUser(baseActivity, newUser, new IOnCreateNewUserListener() {
                                @Override
                                public void onSuccess() {
                                    DataManager.getInstance().increaseCoinsByAmount(REWARD_INVITE_FRIEND_INVITEE_AMOUNT);
                                    NavigationManager.getInstance().handleAfterSplashNavigation(baseActivity);
                                }

                                @Override
                                public void onFailure() {
                                    NavigationManager.getInstance().handleAfterSplashNavigation(baseActivity);
                                }
                            });
                        }

                        @Override
                        public void onFailure() {
                            NavigationManager.getInstance().handleAfterSplashNavigation(baseActivity);
                        }
                    });
                }

                @Override
                public void onFailure() {
                    checkUserNeedToBeRewardedForInvitees(baseActivity);
                }
            });
        } else {
            checkUserNeedToBeRewardedForInvitees(baseActivity);
        }
    }

    private void checkUserNeedToBeRewardedForInvitees(final BaseActivity baseActivity) {
        DataManager.getInstance().getUserInvitees(MyUtils.getDeviceId(baseActivity), new IOnGetUserInviteesListener() {
            @Override
            public void onSuccess(List<Invitee> inviteeList) {
                int count = 0;
                for (Invitee invitee : inviteeList) {
                    if (!invitee.isIs_rewarded()) {
                        DataManager.getInstance().setUserRewardedForInvitee(MyUtils.getDeviceId(baseActivity), invitee.getUdid());
                        count++;
                    }
                }
                DataManager.getInstance().addInviteesToCounter(count);
                NavigationManager.getInstance().handleAfterSplashNavigation(baseActivity);
            }

            @Override
            public void onFailure() {
                NavigationManager.getInstance().handleAfterSplashNavigation(baseActivity);
            }
        });
    }

    public void checkRewardForInvitees(BaseActivity baseActivity) {
        int inviteesNum = DataManager.getInstance().getInviteesCounter();
        if (inviteesNum > 0) {
            DataManager.getInstance().resetInviteesCounter();
            DialogUtils.showRewardedForReferringUsersDialog(baseActivity, inviteesNum);
            int rewardAmount = Consts.REWARD_INVITE_FRIEND_INVITER_AMOUNT * inviteesNum;
            SoundManager.getInstance().playSound(SOUND_REWARD);
            DataManager.getInstance().increaseCoinsByAmount(rewardAmount);
        }
    }

    public void shareDynamicLink(BaseActivity baseActivity) {
        String txt = baseActivity.getString(R.string.share_link_message);
        String invitationLink = DataManager.getInstance().getInvitationLink();
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, invitationLink + " " + txt);
        intent.setType("text/plain");
        baseActivity.startActivity(Intent.createChooser(intent, "Share"));
    }

    public void onAppCreation(final BaseActivity baseActivity) {
        getGeneralData(baseActivity);
    }

    private void getGeneralData(final BaseActivity baseActivity) {
        DataManager.getInstance().getGeneralData(new IOnGetGeneralDataListener() {

            @Override
            public void onAppMustUpdate(String updateUrl) {
                DialogUtils.showAppMustUpdateDialog(baseActivity, updateUrl);
            }

            @Override
            public void onDatabaseUpdate(boolean isDatabaseNeedToBeUpdated, boolean isSpecialDbNeedToBeUpdated) {
                if (isDatabaseNeedToBeUpdated) {
                    getQuotesData(baseActivity, isSpecialDbNeedToBeUpdated);
                } else {
                    if (isSpecialDbNeedToBeUpdated) {
                        getSpecialQuotesData(baseActivity);
                    } else {
                        deepLinkRewardCheck(baseActivity);
                    }
                }
            }

            @Override
            public void onFailure() {
                if (DataManager.getInstance().isEnteredApp()) {
                    NavigationManager.getInstance().handleAfterSplashNavigation(baseActivity);
                } else {
                    DialogUtils.showAppNeedInternetAtFirstLaunch(baseActivity, new IOnAppNeedInternetDialogListener() {
                        @Override
                        public void onTryAgain() {
                            getGeneralData(baseActivity);
                        }
                    });
                }
            }
        });
    }

    private void getQuotesData(final BaseActivity baseActivity, final boolean isSpecialDbNeedToBeUpdated) {
        DataManager.getInstance().getQuotesData(new IOnGetQuotesDataListener() {
            @Override
            public void onSuccess() {
                if (isSpecialDbNeedToBeUpdated) {
                    getSpecialQuotesData(baseActivity);
                } else {
                    deepLinkRewardCheck(baseActivity);
                }
            }

            @Override
            public void onFailure() {
                if (DataManager.getInstance().isEnteredApp()) {
                    deepLinkRewardCheck(baseActivity);
                } else {
                    DialogUtils.showAppNeedInternetAtFirstLaunch(baseActivity, new IOnAppNeedInternetDialogListener() {
                        @Override
                        public void onTryAgain() {
                            onAppCreation(baseActivity);
                        }
                    });
                }
            }
        });
    }

    private void getSpecialQuotesData(final BaseActivity baseActivity) {
        DataManager.getInstance().getSpecialQuotesData(new IOnGetSpecialQuotesDataListener() {
            @Override
            public void onSuccess() {
                deepLinkRewardCheck(baseActivity);
            }

            @Override
            public void onFailure() {
                if (DataManager.getInstance().isEnteredApp()) {
                    deepLinkRewardCheck(baseActivity);
                } else {
                    DialogUtils.showAppNeedInternetAtFirstLaunch(baseActivity, new IOnAppNeedInternetDialogListener() {
                        @Override
                        public void onTryAgain() {
                            onAppCreation(baseActivity);
                        }
                    });
                }
            }
        });
    }

    public void checkForAppNewUpdateAvailable(BaseActivity activity) {
        String currentVersion = BuildConfig.VERSION_NAME;

        if (MyUtils.versionCompare(currentVersion, DataManager.getInstance().getLastAppVersion()) < 0) {
            DialogUtils.showAppHasNewUpdateAvailableDialog(activity, DataManager.getInstance().getAndroidUpdateUrl());
        }
    }

    public void returnFromFacebookPage() {
        if (!DataManager.getInstance().isLikedFacebookPage()) {
            DataManager.getInstance().setFacebookPageLiked();
            DataManager.getInstance().increaseCoinsByAmount(REWARD_FACEBOOK_LIKE_AMOUNT);
            SoundManager.getInstance().playSound(SOUND_REWARD);
            FirebaseManager.getInstance().reportEvent(FREE_COINS_EVENT_NAME, FREE_COINS_FACEBOOK_PARAMETER_VALUE);
        }
    }

    public void returnFromInstagramPage() {
        if (!DataManager.getInstance().isFollowedInstagramPage()) {
            DataManager.getInstance().setInstagramPageFollowed();
            DataManager.getInstance().increaseCoinsByAmount(REWARD_INSTAGRAM_FOLLOW_AMOUNT);
            SoundManager.getInstance().playSound(SOUND_REWARD);
            FirebaseManager.getInstance().reportEvent(FREE_COINS_EVENT_NAME, FREE_COINS_INSTAGRAM_PARAMETER_VALUE);
        }
    }

    public void returnFromLogotestDownload() {
        if (!DataManager.getInstance().isDownloadLogotestApp()) {
            DataManager.getInstance().setDownloadedLogotestApp();
            DataManager.getInstance().increaseCoinsByAmount(REWARD_DOWNLOAD_LOGOTEST_APP_AMOUNT);
            SoundManager.getInstance().playSound(SOUND_REWARD);
            FirebaseManager.getInstance().reportEvent(FREE_COINS_EVENT_NAME, FREE_COINS_DOWNLOAD_LOGOTEST_PARAMETER_VALUE);
        }
    }

    public void onSoundClick(IOnSoundClickListener iOnSoundClickListener) {
        boolean sound = !DataManager.getInstance().isSoundWork();
        DataManager.getInstance().setSound(sound);
        SoundManager.getInstance().setMuted(!sound);
        iOnSoundClickListener.onSoundChange(sound);
    }

    public void shareQuoteOnWhatsapp(BaseActivity baseActivity, String quote, String
            quoteHolder, boolean isQuoteSolved) {
        try {
            String whatsappPackage = "com.whatsapp";
            String shareText;
            if (isQuoteSolved) {
                shareText = baseActivity.getString(R.string.share_after_solved_message, quote, quoteHolder);
            } else {
                shareText = baseActivity.getString(R.string.share_before_solved_message, quote);
            }
            String url = "http://onelink.to/r5zqcq";

            PackageManager pm = baseActivity.getPackageManager();
            try {
                @SuppressWarnings("unused")
                PackageInfo info = pm.getPackageInfo(whatsappPackage, PackageManager.GET_META_DATA);

                Intent waIntent = new Intent(Intent.ACTION_SEND);
                waIntent.setType("text/plain");
                waIntent.setPackage(whatsappPackage);
                waIntent.putExtra(Intent.EXTRA_TEXT, shareText + " \n" + url);
                baseActivity.startActivity(waIntent);
            } catch (Exception e) {
                Logger.e(Logger.TEST, "Error on sharing", e);
                Toast.makeText(baseActivity, baseActivity.getString(R.string.whatsapp_app_no_installed_on_device_toast_message),
                        Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void shareQuoteOnFacebook(BaseActivity baseActivity, String quote, String
            quoteHolder, boolean isQuoteSolved) {
        try {
            String facebookPackage = "com.facebook.katana";
            String shareText;

            if (isQuoteSolved) {
                shareText = baseActivity.getString(R.string.share_after_solved_message, quote, quoteHolder);
            } else {
                shareText = baseActivity.getString(R.string.share_before_solved_message, quote);
            }

            PackageManager pm = baseActivity.getPackageManager();
            try {
                @SuppressWarnings("unused")
                PackageInfo info = pm.getPackageInfo(facebookPackage, PackageManager.GET_META_DATA);

                Intent waIntent = new Intent(Intent.ACTION_SEND);
                waIntent.setType("text/plain");
                waIntent.setPackage(facebookPackage);
                waIntent.putExtra(Intent.EXTRA_TEXT, shareText);
                baseActivity.startActivity(waIntent);
            } catch (Exception e) {
                Logger.e(Logger.TEST, "Error on sharing", e);
                Toast.makeText(baseActivity, baseActivity.getString(R.string.facebook_app_no_installed_on_device_toast_message), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void notificationRewardCheck(String rewardAmount) {
        int amount = 0;
        try {
            amount = Integer.parseInt(rewardAmount);
        } catch (Exception ignored) {
        }
        if (amount > 0) {
            DataManager.getInstance().increaseCoinsByAmount(amount);
        }
    }

    public void onGameFinished(final BaseActivity baseActivity) {
        SoundManager.getInstance().playSound(SOUND_CHEERS);
        DialogUtils.showGameCompletedDialog(baseActivity);
        DataManager.getInstance().setGameFinished(true);
    }

    public void goToLeaderboard(BaseActivity baseActivity) {
        GoogleSignInAccount googleSignInAccount = GoogleSignIn.getLastSignedInAccount(baseActivity);
        GamesServicesManager.showLeaderboard(baseActivity, googleSignInAccount);
    }

    public void updateGameTimeInLeaderboard(BaseActivity baseActivity) {
        GoogleSignInAccount googleSignInAccount = GoogleSignIn.getLastSignedInAccount(baseActivity);
        GamesServicesManager.updateGameTimeInLeaderboard(baseActivity, googleSignInAccount);
    }

    public void showPurchaseSpecialsDialog(final BaseActivity baseActivity, final PurchaseSpecialsDialog.SubscriptionDialogListener subscriptionDialogListener) {
        PurchaseSpecialsDialog purchaseSpecialsDialog = new PurchaseSpecialsDialog(baseActivity);
        purchaseSpecialsDialog.setmListener(new PurchaseSpecialsDialog.SubscriptionDialogListener() {
            @Override
            public void onStartSubscriptionClicked() {
                subscriptionDialogListener.onStartSubscriptionClicked();
            }
        });
        purchaseSpecialsDialog.show();
    }

    public List<ILevel> getDataForLevelsFragment(Realm realm) {
        List<ILevel> dataList = new ArrayList<>();
        dataList.add(new LevelSpecials());

        long numOfLevelsToDisplay = DataManager.getInstance().getNumberOfLevelsToDisplay();
        int totalSolvedQuotesCount = MyDatabase.getInstance().getTotalSolvedQuotesCount(realm);

        int i;
        for (i = 0; i < numOfLevelsToDisplay; i++) {
            int minSolvedQuotesToOpen = MyDatabase.getInstance().getTotalQuotesTilLevelCount(realm, i) * OPEN_LEVEL_COMPLETION_PERCENT / 100;
            if (totalSolvedQuotesCount >= minSolvedQuotesToOpen) { // if level is open!
                LevelOpen levelOpen = new LevelOpen();
                levelOpen.setLevelNum(i + 1);
                levelOpen.setTotalQuotesCount(MyDatabase.getInstance().getTotalQuotesInLevelCount(realm, i + 1));
                levelOpen.setTotalSolvedQuotesCount(MyDatabase.getInstance().getTotalSolvedQuotesInLevelCount(realm, i + 1));
                dataList.add(levelOpen);
            } else { // level is lock
                LevelLock levelLock = new LevelLock();
                levelLock.setLevelNum(i + 1);
                levelLock.setLogosToOpen(minSolvedQuotesToOpen - totalSolvedQuotesCount);
                dataList.add(levelLock);
            }
        }

        try {
            int numOfOpenLevels = 0;
            for (ILevel iLevel : dataList) {
                if (iLevel instanceof LevelOpen) {
                    numOfOpenLevels++;
                }
            }

            int posOfAd = new Random().nextInt(numOfOpenLevels + 1) + 1;
            dataList.add(posOfAd, new LevelAd());
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }

        dataList.add(new LevelSoon(i + 1));
        dataList.add(new LevelSoon(i + 2));

        return dataList;
    }

    public List<SpecialLevel> getDataForSpecialLevelsFragment(Realm realm) {
        List<SpecialLevel> specialLevelList = MyDatabase.getInstance().getAllSpecialLevels(realm);
        for (SpecialLevel specialLevel : specialLevelList) {
            specialLevel.setTotalQuotes(MyDatabase.getInstance().getTotalQuotesInLevelCount(realm, specialLevel.getLevelId()));
            specialLevel.setTotalSolvedQuotes(MyDatabase.getInstance().getTotalSolvedQuotesInLevelCount(realm, specialLevel.getLevelId()));
        }
        return specialLevelList;
    }

    public List<IGetCoins> getDataForCoinsFragment(BaseActivity baseActivity) {
        List<IGetCoins> getCoinsList = new ArrayList<>();

        List<GetCoinsInAppBilling> inAppBillingList = PurchasesManager.getInstance().getPurchasesListingsDetails();

        getCoinsList.add(new GetCoinsHeader(baseActivity.getString(R.string.coins_dialog_support_us)));
        GetCoinsNormal downloadLogotest = new GetCoinsNormal(R.drawable.ic_logotest, baseActivity.getString(R.string.download_logotest_txt),
                baseActivity.getString(R.string.download_logotest_reward),
                ResourcesCompat.getColor(baseActivity.getResources(), R.color.fragment_logo_background_color, null),
                GetCoinsNormal.rowType.DOWNLOAD_LOGOTEST);
        boolean isDownloadLogotestRewarded = false;
        if (DataManager.getInstance().isDownloadLogotestApp() ||
                MyUtils.isPackageInstalled(LOGOTEST_APP_PACKAGE_NAME, baseActivity.getPackageManager()))
            isDownloadLogotestRewarded = true;
        downloadLogotest.setRewarded(isDownloadLogotestRewarded);
        getCoinsList.add(downloadLogotest);
        GetCoinsNormal facebookLike = new GetCoinsNormal(R.drawable.ic_facebook_like,
                baseActivity.getString(R.string.like_on_facebook),
                baseActivity.getString(R.string.like_on_facebook_reward),
                ResourcesCompat.getColor(baseActivity.getResources(), R.color.facebook_color, null),
                GetCoinsNormal.rowType.FACEBOOK);
        facebookLike.setRewarded(DataManager.getInstance().isLikedFacebookPage());
        getCoinsList.add(facebookLike);
        GetCoinsNormal instagramFollow = new GetCoinsNormal(R.drawable.ic_instagram_like,
                baseActivity.getString(R.string.follow_on_instagram),
                baseActivity.getString(R.string.follow_on_instagram_reward),
                ResourcesCompat.getColor(baseActivity.getResources(), R.color.instagram_color, null),
                GetCoinsNormal.rowType.INSTAGRAM);
        instagramFollow.setRewarded(DataManager.getInstance().isFollowedInstagramPage());
        getCoinsList.add(instagramFollow);
        getCoinsList.add(new GetCoinsNormal(R.drawable.ic_gift,
                baseActivity.getString(R.string.invite_friend_to_application),
                baseActivity.getString(R.string.invite_friend_reward),
                ResourcesCompat.getColor(baseActivity.getResources(), R.color.fragment_logo_background_color, null)
                , GetCoinsNormal.rowType.INVITE_FRIEND));
        getCoinsList.add(new GetCoinsNormal(R.drawable.ic_reward_video,
                baseActivity.getString(R.string.video_reward_text),
                baseActivity.getString(R.string.video_reward),
                ResourcesCompat.getColor(baseActivity.getResources(), R.color.fragment_logo_background_color, null), GetCoinsNormal.rowType.VIDEO));
        getCoinsList.add(new GetCoinsRankUs());

        getCoinsList.add(new GetCoinsHeader(baseActivity.getString(R.string.coins_dialog_purchases)));
        getCoinsList.add(PurchasesManager.getInstance().getGetCoinsSpecialDeal());
        if (!inAppBillingList.isEmpty()) {
            getCoinsList.addAll(inAppBillingList);
        }

        return getCoinsList;
    }

    public List<ISettings> getDataForSettingsFragment(Context context) {
        List<ISettings> settingsList = new ArrayList<>();

        SettingsNormal soundSettings = new SettingsNormal(context.getString(R.string.sounds), SettingsNormal.RowType.SOUNDS);
        soundSettings.setShouldShowIcon(true);
        soundSettings.setIconRes(DataManager.getInstance().isSoundWork() ? R.drawable.ic_sound_on : R.drawable.ic_sound_off);
        settingsList.add(soundSettings);
        settingsList.add(new SettingsNormal(context.getString(R.string.reset_game), SettingsNormal.RowType.RESET_GAME));
        settingsList.add(new SettingsNormal(context.getString(R.string.statistics), SettingsNormal.RowType.STATISTICS));
        settingsList.add(new SettingsNormal(context.getString(R.string.privacy_policy), SettingsNormal.RowType.PRIVACY_POLICY));
        settingsList.add(new SettingsNormal(context.getString(R.string.token), SettingsNormal.RowType.TOKEN));

        return settingsList;
    }

    public void showInviteFriendSuggestionIfNeed(final BaseActivity baseActivity) {
        if (DataManager.getInstance().shouldShowInviteFriendSuggestion()) {
            DialogUtils.showInviteFriendSuggestionDialog(baseActivity, new IOnInviteFriendSuggestionListener() {
                @Override
                public void onGoToInviteFriendClick() {
                    goToInviteFriendView(baseActivity, false);
                }
            });
        }
        DataManager.getInstance().addToInviteFriendSuggestion();
    }
}