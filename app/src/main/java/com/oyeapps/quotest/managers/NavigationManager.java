package com.oyeapps.quotest.managers;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.activities.BaseActivity;
import com.oyeapps.quotest.activities.CoinsActivity;
import com.oyeapps.quotest.activities.GameActivity;
import com.oyeapps.quotest.activities.InviteFriendActivity;
import com.oyeapps.quotest.activities.MainActivity;
import com.oyeapps.quotest.activities.SettingActivity;
import com.oyeapps.quotest.activities.SplashActivity;
import com.oyeapps.quotest.extraData.ExtraDataItem;
import com.oyeapps.quotest.fragments.AllQuotesFragment;
import com.oyeapps.quotest.fragments.BaseFragment;
import com.oyeapps.quotest.fragments.CoinsFragment;
import com.oyeapps.quotest.fragments.InviteFriendFragment;
import com.oyeapps.quotest.fragments.LevelsFragment;
import com.oyeapps.quotest.fragments.MainFragment;
import com.oyeapps.quotest.fragments.QuoteFragment;
import com.oyeapps.quotest.fragments.SettingsFragment;
import com.oyeapps.quotest.fragments.SpecialLevelsFragment;
import com.oyeapps.quotest.fragments.StatisticsFragment;
import com.oyeapps.quotest.utils.Logger;
import com.oyeapps.quotest.utils.MyUtils;

import static com.oyeapps.quotest.utils.Consts.TO_SCREEN_GET_COINS_CODE;
import static com.oyeapps.quotest.utils.Consts.TO_SCREEN_INVITE_FRIEND_CODE;
import static com.oyeapps.quotest.utils.Consts.TO_SCREEN_LEVELS_CODE;

public class NavigationManager {

    private static final String TAG = NavigationManager.class.getSimpleName();

    private static final String FROM_ACTIVITY_EXTRA = "fromActivity";

    private static NavigationManager instance = null;

    public synchronized static NavigationManager getInstance() {
        if (instance == null) {
            instance = new NavigationManager();
        }
        return instance;
    }

    private boolean mShouldHandleNavToScreen = false;
    private String mScreenCode = "0";
    private String mPushUrl = "";

    private void clearData() {
        instance = null;
    }

    public void setmScreenCode(String mScreenCode, String mPushUrl) {
        if (!mScreenCode.equals("0") || !mPushUrl.isEmpty()) {
            this.mScreenCode = mScreenCode;
            this.mPushUrl = mPushUrl;
            this.mShouldHandleNavToScreen = true;
        }
    }

    public void navigateBack(BaseActivity baseActivity) {
        Bundle bundle = MyUtils.getNonNullBundle(baseActivity.getIntent().getExtras());
        String fromActivity = bundle.getString(FROM_ACTIVITY_EXTRA, "");
        boolean isFromGoToScreenNav = fromActivity.equals(SplashActivity.class.getName());
        boolean isMainFragmentInActivity = baseActivity.getSupportFragmentManager().getBackStackEntryCount() == 0;
        baseActivity.handleBackPress(isFromGoToScreenNav, isMainFragmentInActivity);
    }

    public boolean isCurrentFragmentHandleBackPress(Fragment fragment) {
        if (fragment instanceof BaseFragment) {
            return ((BaseFragment) fragment).isFragmentHandleBackPress();
        }
        return false;
    }

    void handleAfterSplashNavigation(BaseActivity baseActivity) {
        try {
            if (!DataManager.getInstance().isEnteredApp()) {
                DataManager.getInstance().setEnteredApp();
            }
            if (mShouldHandleNavToScreen) {
                handleGoToScreenNavigation(baseActivity);
            } else {
                goToMainActivity(baseActivity);
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void handleGoToScreenNavigation(BaseActivity baseActivity) {
        if (!mPushUrl.isEmpty()) {
            goToMainActivity(baseActivity);
            openUrl(baseActivity, mPushUrl);
        } else {
            switch (mScreenCode) {
                case TO_SCREEN_INVITE_FRIEND_CODE:
                    GeneralManager.getInstance().goToInviteFriendView(baseActivity, true);
                    break;
                case TO_SCREEN_LEVELS_CODE:
                    goToGameActivity(baseActivity);
                    break;
                case TO_SCREEN_GET_COINS_CODE:
                    goToCoinsActivity(baseActivity);
                    break;
                default:
                    goToMainActivity(baseActivity);
                    break;
            }
        }
        clearData();
    }

    public void goToMainActivity(BaseActivity baseActivity) {
        try {
            startActivity(baseActivity, MainActivity.class);
            if (baseActivity instanceof GameActivity ||
                    baseActivity instanceof InviteFriendActivity ||
                    baseActivity instanceof CoinsActivity) {
                baseActivity.finish();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void goToCoinsActivity(BaseActivity baseActivity) {
        try {
            startActivity(baseActivity, CoinsActivity.class);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    void goToInviteFriendActivity(BaseActivity baseActivity) {
        try {
            startActivity(baseActivity, InviteFriendActivity.class);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void goToSettingActivity(BaseActivity baseActivity) {
        try {
            startActivity(baseActivity, SettingActivity.class);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void goToGameActivity(BaseActivity baseActivity) {
        try {
            startActivity(baseActivity, GameActivity.class);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void goToAllQuotesFragment(BaseActivity baseActivity, ExtraDataItem extraData) {
        try {
            baseActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment, AllQuotesFragment.newInstance(extraData), AllQuotesFragment.class.getName())
                    .addToBackStack(null)
                    .commit();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void goToSpecialLevelsFragment(BaseActivity baseActivity) {
        try {
            baseActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment, SpecialLevelsFragment.newInstance(), SpecialLevelsFragment.class.getName())
                    .addToBackStack(null)
                    .commit();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void goToStatisticsFragment(BaseActivity baseActivity) {
        try {
            baseActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment, StatisticsFragment.newInstance(), StatisticsFragment.class.getName())
                    .addToBackStack(null)
                    .commit();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void goToInviteFriendFragment(BaseActivity baseActivity) {
        try {
            baseActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment, InviteFriendFragment.newInstance(), InviteFriendFragment.class.getName())
                    .commit();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void goToSettingFragment(BaseActivity baseActivity) {
        try {
            baseActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment, SettingsFragment.newInstance(), SettingsFragment.class.getName())
                    .commit();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void openPrivacyPolicy(BaseActivity baseActivity) {
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(baseActivity.getString(R.string.privacy_policy_url)));
            baseActivity.startActivity(browserIntent);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void openFacebookPage(BaseActivity baseActivity) {
        try {
            baseActivity.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            baseActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/" + baseActivity.getString(R.string.facebook_page_id))));
        } catch (Exception e) {
            baseActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(baseActivity.getString(R.string.facebook_page_url))));
        }
    }

    public void openInstagramPage(BaseActivity baseActivity) {
        Uri uri = Uri.parse("http://www.instagram.com/_u/" + DataManager.getInstance().getInstagramPageUsername());
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        try {
            baseActivity.startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            baseActivity.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://www.instagram.com/" + DataManager.getInstance().getInstagramPageUsername())));
        }
    }

    public void openAppInPlayStore(BaseActivity baseActivity, String packageName) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(
                "https://play.google.com/store/apps/details?id=" + packageName));
        intent.setPackage("com.android.vending");
        baseActivity.startActivity(intent);
    }

    public void navigateToApp(BaseActivity baseActivity, String packageName) {
        try {
            Intent launchIntent = baseActivity.getPackageManager().getLaunchIntentForPackage(packageName);
            baseActivity.startActivity(launchIntent);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void openUrl(BaseActivity baseActivity, String url) {
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            baseActivity.startActivity(browserIntent);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void goToMainFragment(BaseActivity baseActivity) {
        try {
            baseActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment, MainFragment.newInstance(), MainFragment.class.getName())
                    .commit();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void goToLevelsFragment(BaseActivity baseActivity) {
        try {
            baseActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment, LevelsFragment.newInstance(), LevelsFragment.class.getName())
                    .commit();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void goToQuoteFragment(FragmentActivity activity, ExtraDataItem extraData) {
        try {
            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment, QuoteFragment.newInstance(extraData), QuoteFragment.class.getName())
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void goToCoinsFragment(FragmentActivity activity) {
        try {
            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment, CoinsFragment.newInstance(), CoinsFragment.class.getName())
                    .commitAllowingStateLoss();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void startActivity(BaseActivity from, Class to) {
        Intent intent = new Intent(from, to);
        intent.putExtra(FROM_ACTIVITY_EXTRA, from.name);
//        if (Build.VERSION.SDK_INT > 20) {
//            ActivityOptions options =
//                    ActivityOptions.makeSceneTransitionAnimation(from);
//            from.startActivity(intent, options.toBundle());
//        } else {
//            from.startActivity(intent);
//        }
        from.startActivity(intent);
        if (from instanceof SplashActivity) {
            from.finish();
        }
    }

    public void closeApp(BaseActivity baseActivity) {
        baseActivity.finishAffinity();
        System.exit(0);
    }
}
