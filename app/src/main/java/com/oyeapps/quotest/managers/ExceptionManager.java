package com.oyeapps.quotest.managers;

import androidx.fragment.app.FragmentActivity;

import com.oyeapps.quotest.interfaces.IOnAppTerminationErrorOccurredDialogListener;
import com.oyeapps.quotest.interfaces.IOnNavigateOutErrorOccurredDialogListener;
import com.oyeapps.quotest.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitsan on 10/05/2017.
 */

public class ExceptionManager implements java.lang.Thread.UncaughtExceptionHandler {

    @Override
    public void uncaughtException(Thread t, Throwable e) {

    }

    public enum viewErrorType {
        NAVIGATION,
        TERMINAL
    }

    public interface ExceptionManagerHolder {
        ExceptionManager getExceptionManager();
    }

    public enum errorTypes {
        TERMINAL,
        NETWORK,
        NAVIGATION,
        TERMINATION
    }

    private List<String> appTerminatingFragments = new ArrayList<>();
    private List<String> navigationFragments = new ArrayList<>();

    private List<String> appTerminatingActivities = new ArrayList<>();
    private List<String> navigationActivities = new ArrayList<>();

    private static ExceptionManager instance = null;

    public static ExceptionManager getInstance() {
        if (instance == null) {
            instance = new ExceptionManager();
        }
        return instance;
    }

    private ExceptionManager() {
//        appTerminatingFragments = Collections.emptyList();
//        navigationFragments = Arrays.asList(AllQuotesFragment.class.getname(), LevelsFragment.class.getname(), QuoteFragmentExtraData.class.getname(),
//                SettingFragment.class.getname(), StatisticsFragment.class.getname(), ChangeUsernameFragment.class.getname(),
//                GameFragment.class.getname(), GameFinishedFragment.class.getname());
//        appTerminatingActivities = Arrays.asList(SplashActivity.class.getname(), MainActivity.class.getname());
//        navigationActivities = Arrays.asList(GameActivity.class.getname(), SettingActivity.class.getname(), RecordsTableActivity.class.getname(),
//                FastGameActivity.class.getname(), InviteFriendActivity.class.getname());
    }

    public void handleError(errorTypes errType, String className, FragmentActivity activity) {
        if (errType.equals(errorTypes.TERMINAL) && isApplicationTermination(className)) {
            alertAndTerminate(activity);
        }
        if (errType.equals(errorTypes.TERMINAL) && isNavigateTermination(className)) {
            alertAndNavigateOut(activity);
        }
        if (errType.equals(errorTypes.NETWORK)) {
            DialogUtils.showNetworkErrorOccurredDialog(activity);
        }
        if (errType.equals(errorTypes.NAVIGATION)) {
            alertAndNavigateOut(activity);
        }
        if (errType.equals(errorTypes.TERMINATION)) {
            terminateApp();
        }
    }

    private void alertAndTerminate(FragmentActivity activity) {
        DialogUtils.showAppTerminationDialog(activity, new IOnAppTerminationErrorOccurredDialogListener() {
            @Override
            public void onTermination() {
                terminateApp();
            }
        });
    }

    private void alertAndNavigateOut(final FragmentActivity activity) {
        DialogUtils.showNavigateOutDialog(activity, new IOnNavigateOutErrorOccurredDialogListener() {
            @Override
            public void onNavigateOut() {
                activity.getOnBackPressedDispatcher().onBackPressed();
            }
        });
    }

    private void terminateApp() {
        System.exit(0);
    }

    private boolean isNavigateTermination(String className) {
        return navigationFragments.contains(className) || navigationActivities.contains(className);
    }

    private boolean isApplicationTermination(String className) {
        return appTerminatingFragments.contains(className) || appTerminatingActivities.contains(className);
    }

    public void addActivityToList(String activityName, viewErrorType viewErrorType) {
        if (viewErrorType == ExceptionManager.viewErrorType.NAVIGATION) {
            this.navigationActivities.add(activityName);
        } else {
            this.appTerminatingActivities.add(activityName);
        }
    }

    public void removeActivityFromList(String activityName, viewErrorType viewErrorType) {
        if (viewErrorType == ExceptionManager.viewErrorType.NAVIGATION) {
            this.navigationActivities.remove(activityName);
        } else {
            this.appTerminatingActivities.remove(activityName);
        }
    }

    public void addFragmentToList(String fragmentName, viewErrorType viewErrorType) {
        if (viewErrorType == ExceptionManager.viewErrorType.NAVIGATION) {
            this.navigationFragments.add(fragmentName);
        } else {
            this.appTerminatingFragments.add(fragmentName);
        }
    }

    public void removeFragmentFromList(String fragmentName, viewErrorType viewErrorType) {
        if (viewErrorType == ExceptionManager.viewErrorType.NAVIGATION) {
            this.navigationFragments.remove(fragmentName);
        } else {
            this.appTerminatingFragments.remove(fragmentName);
        }
    }
}
