package com.oyeapps.quotest.managers;

import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.oyeapps.quotest.BuildConfig;
import com.oyeapps.quotest.activities.BaseActivity;
import com.oyeapps.quotest.app.MyApplication;
import com.oyeapps.quotest.data_obj.GeneralData;
import com.oyeapps.quotest.database.MyDatabase;
import com.oyeapps.quotest.interfaces.IOnCreateDynamicLinkListener;
import com.oyeapps.quotest.interfaces.IOnCreateNewUserListener;
import com.oyeapps.quotest.interfaces.IOnGetDynamicLinkListener;
import com.oyeapps.quotest.interfaces.IOnGetGeneralDataListener;
import com.oyeapps.quotest.interfaces.IOnGetQuotesDataListener;
import com.oyeapps.quotest.interfaces.IOnGetSpecialQuotesDataListener;
import com.oyeapps.quotest.interfaces.IOnGetUserInviteesListener;
import com.oyeapps.quotest.interfaces.IOnGetUserListener;
import com.oyeapps.quotest.services.FSGeneralData;
import com.oyeapps.quotest.services.FSQuote;
import com.oyeapps.quotest.services.FSQuotesData;
import com.oyeapps.quotest.services.FSSpecialLevel;
import com.oyeapps.quotest.services.FSSpecialQuotesData;
import com.oyeapps.quotest.services.Invitee;
import com.oyeapps.quotest.services.UserInvite;
import com.oyeapps.quotest.utils.Logger;
import com.oyeapps.quotest.utils.MyUtils;
import com.oyeapps.quotest.widgets.ComplexPreferences;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

import static android.content.Context.MODE_PRIVATE;
import static com.oyeapps.quotest.utils.Consts.GENERAL_DATA_KEY;
import static com.oyeapps.quotest.utils.Consts.MY_PREFERENCES;
import static com.oyeapps.quotest.utils.FlavorConsts.ANDROID_APP_MINIMUM_VERSION;
import static com.oyeapps.quotest.utils.FlavorConsts.COLLECTION_GENERAL;
import static com.oyeapps.quotest.utils.FlavorConsts.COLLECTION_USERS;
import static com.oyeapps.quotest.utils.FlavorConsts.DOCUMENT_GENERAL_DATA;
import static com.oyeapps.quotest.utils.FlavorConsts.DOCUMENT_QUOTES;
import static com.oyeapps.quotest.utils.FlavorConsts.DOCUMENT_SPECIAL_QUOTES;
import static com.oyeapps.quotest.utils.FlavorConsts.DYNAMIC_LINK_BASE_LINK;
import static com.oyeapps.quotest.utils.FlavorConsts.DYNAMIC_LINK_DOMAIN_URL_PREFIX;
import static com.oyeapps.quotest.utils.FlavorConsts.IOS_APP_MINIMUM_VERSION;
import static com.oyeapps.quotest.utils.FlavorConsts.IOS_APP_PACKAGE_NAME;
import static com.oyeapps.quotest.utils.FlavorConsts.IOS_APP_STORE_ID;
import static com.oyeapps.quotest.utils.FlavorConsts.SUB_COLLECTION_INVITEES;

public class DataManager {

    private static final String TAG = DataManager.class.getSimpleName();

    private static DataManager instance = null;

    public synchronized static DataManager getInstance() {
        if (instance == null) {
            instance = new DataManager();
        }
        return instance;
    }

    private GeneralData getGeneralData() {
        ComplexPreferences complexPreferences =
                ComplexPreferences.getComplexPreferences(MyApplication.getInstance().getContext(), MY_PREFERENCES, MODE_PRIVATE);
        GeneralData generalData = complexPreferences.getObject(GENERAL_DATA_KEY, GeneralData.class);
        if (generalData == null) {
            return new GeneralData();
        }
        return generalData;
    }

    private void updateGeneralData(GeneralData generalData) {
        ComplexPreferences complexPreferences =
                ComplexPreferences.getComplexPreferences(MyApplication.getInstance().getContext(), MY_PREFERENCES, MODE_PRIVATE);
        complexPreferences.putObject(GENERAL_DATA_KEY, generalData);
        complexPreferences.commit();
    }

    private GeneralData getGeneralDataForEdit() {
        return getGeneralData();
    }

    void getGeneralData(final IOnGetGeneralDataListener iOnGetGeneralDataListener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Logger.d(Logger.TEST, TAG, "Attempt to connect: " + COLLECTION_GENERAL + " -> " + DOCUMENT_GENERAL_DATA);
        db.collection(COLLECTION_GENERAL).document(DOCUMENT_GENERAL_DATA)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot documentSnapshot = task.getResult();
                            if (documentSnapshot != null && documentSnapshot.exists()) {
                                FSGeneralData fsGeneralData = documentSnapshot.toObject(FSGeneralData.class);
                                if (fsGeneralData != null) {
                                    Logger.d(Logger.TEST, TAG, "GeneralData downloaded successfully");
                                    if (MyUtils.isAppMustUpdate(fsGeneralData.getMinAppVersion())) {
                                        iOnGetGeneralDataListener.onAppMustUpdate(fsGeneralData.getUpdateUrl());
                                        return;
                                    }
                                    initGeneralData(fsGeneralData);
                                    numOfLevelsToDisplay = fsGeneralData.getNumOfLevelsToDisplay();
                                    dbVersion = fsGeneralData.getDbVersion();
                                    specialDbVersion = fsGeneralData.getSpecialsDbVersion();
                                    String currentDbVersion = getDbVersion();
                                    String currentSpecialsDbVersion = getSpecialDbVersioin();
                                    iOnGetGeneralDataListener.
                                            onDatabaseUpdate(MyUtils.isDatabaseNeedToBeUpdate(currentDbVersion, dbVersion),
                                                    MyUtils.isDatabaseNeedToBeUpdate(currentSpecialsDbVersion, specialDbVersion));
                                } else {
                                    iOnGetGeneralDataListener.onFailure();
                                }
                            } else {
                                Logger.d(Logger.TEST, TAG, "GeneralData download failure");
                                iOnGetGeneralDataListener.onFailure();
                            }
                        } else {
                            Logger.d(Logger.TEST, TAG, "Internet Connection Error");
                            iOnGetGeneralDataListener.onFailure();
                        }
                    }
                });
    }

    private void initGeneralData(FSGeneralData fsGeneralData) {
        GeneralData generalData = getGeneralDataForEdit();
        generalData.setmLastAppVersion(fsGeneralData.getLastAppVersion());
        generalData.setNumOfQuotesBetweenAds(fsGeneralData.getNumOfQuotesToShowAd());
        generalData.setmAndroidUrl(fsGeneralData.getUpdateUrl());
        generalData.setInstagramPageUsername(fsGeneralData.getInstagramPageUsername());
        updateGeneralData(generalData);
    }

    void getQuotesData(final IOnGetQuotesDataListener iOnGetQuotesDataListener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Logger.d(Logger.TEST, TAG, "Attempt to connect: " + COLLECTION_GENERAL + " -> " + DOCUMENT_QUOTES);
        db.collection(COLLECTION_GENERAL).document(DOCUMENT_QUOTES)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot documentSnapshot = task.getResult();
                            if (documentSnapshot != null && documentSnapshot.exists()) {
                                FSQuotesData fsQuotesData = documentSnapshot.toObject(FSQuotesData.class);
                                if (fsQuotesData != null) {
                                    Logger.d(Logger.TEST, TAG, "Quotesdata downloaded successfully");
                                    initQuotesData(fsQuotesData);
                                    iOnGetQuotesDataListener.onSuccess();
                                } else {
                                    iOnGetQuotesDataListener.onFailure();
                                }
                            } else {
                                Logger.d(Logger.TEST, TAG, "Quotesdata download failure");
                                iOnGetQuotesDataListener.onFailure();
                            }
                        } else {
                            Logger.d(Logger.TEST, TAG, "Internet Connection Error");
                            iOnGetQuotesDataListener.onFailure();
                        }
                    }
                });
    }

    private String dbVersion = "";
    private String specialDbVersion = "";
    private long numOfLevelsToDisplay = 0;

    private void initQuotesData(FSQuotesData fsQuotesData) {
        Realm realm = Realm.getDefaultInstance();
        int currentQuotesCount = MyDatabase.getInstance().getTotalQuotesCount(realm);
        for (FSQuote fsQuote : fsQuotesData.getQuoteList()) {
            if (fsQuote.getLevel() <= numOfLevelsToDisplay) {
                MyDatabase.getInstance().addOrUpdateQuote(realm, fsQuote);
            }
        }
        int newQuotesCount = MyDatabase.getInstance().getTotalQuotesCount(realm);
        if (newQuotesCount > currentQuotesCount) {
            setGameFinished(false);
        }
        setDbVersion(dbVersion);
        setNumOfLevelsToDisplay(numOfLevelsToDisplay);
    }

    void getSpecialQuotesData(final IOnGetSpecialQuotesDataListener iOnGetSpecialQuotesDataListener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Logger.d(Logger.TEST, TAG, "Attempt to connect: " + COLLECTION_GENERAL + " -> " + DOCUMENT_SPECIAL_QUOTES);
        db.collection(COLLECTION_GENERAL).document(DOCUMENT_SPECIAL_QUOTES)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot documentSnapshot = task.getResult();
                            if (documentSnapshot != null && documentSnapshot.exists()) {
                                FSSpecialQuotesData fsSpecialQuotesData = documentSnapshot.toObject(FSSpecialQuotesData.class);
                                if (fsSpecialQuotesData != null) {
                                    Logger.d(Logger.TEST, TAG, "SpecialQuotesData downloaded successfully");
                                    initSpecialQuotesData(fsSpecialQuotesData);
                                    iOnGetSpecialQuotesDataListener.onSuccess();
                                } else {
                                    iOnGetSpecialQuotesDataListener.onFailure();
                                }
                            } else {
                                Logger.d(Logger.TEST, TAG, "SpecialQuotesData download failure");
                                iOnGetSpecialQuotesDataListener.onFailure();
                            }
                        } else {
                            Logger.d(Logger.TEST, TAG, "Internet Connection Error");
                            iOnGetSpecialQuotesDataListener.onFailure();
                        }
                    }
                });

    }

    private void initSpecialQuotesData(FSSpecialQuotesData fsSpecialQuotesData) {
        Realm realm = Realm.getDefaultInstance();
        List<FSSpecialLevel> specialLevelList = fsSpecialQuotesData.specialLevelList;
        for (FSSpecialLevel fsSpecialLevel : specialLevelList) {
            MyDatabase.getInstance().addOrUpdateSpecialLevel(realm, fsSpecialLevel);
        }
        List<FSQuote> fsQuoteList = fsSpecialQuotesData.getQuoteList();
        for (FSQuote fsQuote : fsQuoteList) {
            MyDatabase.getInstance().addOrUpdateQuote(realm, fsQuote);
        }
        setSpecialDbVersion(specialDbVersion);
    }

    void createDynamicLink(final BaseActivity baseActivity,
                           final IOnCreateDynamicLinkListener iOnCreateDynamicLinkListener) {
        try {
            String udid = MyUtils.getDeviceId(baseActivity);
            String link = DYNAMIC_LINK_BASE_LINK + udid;
            FirebaseDynamicLinks.getInstance().createDynamicLink()
                    .setLink(Uri.parse(link))
                    .setDomainUriPrefix(DYNAMIC_LINK_DOMAIN_URL_PREFIX)
                    .setAndroidParameters(
                            new DynamicLink.AndroidParameters.Builder(BuildConfig.APPLICATION_ID)
                                    .setMinimumVersion(ANDROID_APP_MINIMUM_VERSION)
                                    .build())
                    .setIosParameters(
                            new DynamicLink.IosParameters.Builder(IOS_APP_PACKAGE_NAME)
                                    .setAppStoreId(IOS_APP_STORE_ID)
                                    .setMinimumVersion(IOS_APP_MINIMUM_VERSION)
                                    .build())
                    .buildShortDynamicLink()
                    .addOnSuccessListener(new OnSuccessListener<ShortDynamicLink>() {
                        @Override
                        public void onSuccess(ShortDynamicLink shortDynamicLink) {
                            try {
                                Uri invitationUrl = shortDynamicLink.getShortLink();
                                if (invitationUrl != null) {
                                    Logger.d(Logger.TEST, TAG, invitationUrl.toString());
                                    DataManager.getInstance().setInvitationLink(invitationUrl.toString());
                                    Logger.d(Logger.TEST, TAG, "Short dynamic link created successfully : " + invitationUrl.toString());
                                    iOnCreateDynamicLinkListener.onSuccess(invitationUrl.toString());
                                } else {
                                    Logger.d(Logger.TEST, TAG, "Error on creating dynamic link");
                                    iOnCreateDynamicLinkListener.onFailure();
                                }
                            } catch (Exception e) {
                                Logger.e(Logger.TEST, e.getMessage(), e);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            try {
                                Logger.d(Logger.TEST, e.getMessage(), getClass().getSimpleName());
                                iOnCreateDynamicLinkListener.onFailure();
                            } catch (Exception e1) {
                                Logger.e(Logger.TEST, e1.getMessage(), e1);
                            }
                        }
                    });
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    void getDynamicLink(final BaseActivity baseActivity, final IOnGetDynamicLinkListener iOnGetDynamicLinkListener) {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(baseActivity.getIntent())
                .addOnSuccessListener(new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        if (pendingDynamicLinkData != null && pendingDynamicLinkData.getLink() != null) {
                            Uri deepLink = pendingDynamicLinkData.getLink();
                            iOnGetDynamicLinkListener.onSuccess(deepLink);
                        } else {
                            iOnGetDynamicLinkListener.onFailure();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Logger.d(Logger.TEST, TAG, "getDynamicLink:onFailure : " + e);
                        iOnGetDynamicLinkListener.onFailure();
                    }
                });
    }

    void createUser(final BaseActivity baseActivity, UserInvite userInvite,
                    final IOnCreateNewUserListener iOnCreateNewUserListener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        final String udid = MyUtils.getDeviceId(baseActivity);
        db.collection(COLLECTION_USERS).document(userInvite.getUdid())
                .set(userInvite)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Logger.d(Logger.TEST, getClass().getSimpleName(), "User " + udid + " added successfully!");
                            iOnCreateNewUserListener.onSuccess();
                        } else {
                            Logger.d(Logger.TEST, getClass().getSimpleName(), "Fail to add user (" + udid + ")");
                            iOnCreateNewUserListener.onFailure();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Logger.d(Logger.TEST, getClass().getSimpleName(), "Fail to add user (" + udid + "), Error: " + e.getMessage());
                        iOnCreateNewUserListener.onFailure();
                    }
                });
    }

    void getUser(final String udid,
                 final IOnGetUserListener iOnGetUserListener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        final DocumentReference userRef = db.collection(COLLECTION_USERS).document(udid);
        userRef.get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot documentSnapshot = task.getResult();
                            if (documentSnapshot != null && documentSnapshot.exists()) {
                                UserInvite userInvite = documentSnapshot.toObject(UserInvite.class);
                                if (userInvite != null) {
                                    Logger.d(Logger.TEST, getClass().getSimpleName(), "User " + userInvite.getUdid() + " exists!");
                                    iOnGetUserListener.onUserExists(userInvite);
                                } else {
                                    iOnGetUserListener.onUserDoesntExist();
                                }
                            } else {
                                Logger.d(Logger.TEST, getClass().getSimpleName(), "User " + udid + " doesn't exists!");
                                iOnGetUserListener.onUserDoesntExist();
                            }
                        } else {
                            Logger.d(Logger.TEST, getClass().getSimpleName(), "Internet Connection Error");
                            iOnGetUserListener.onFailure();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Logger.d(Logger.TEST, getClass().getSimpleName(), e.getMessage());
                        iOnGetUserListener.onFailure();
                    }
                });
    }

    void getUserInvitees(final String udid, final IOnGetUserInviteesListener iOnGetUserInviteesListener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(COLLECTION_USERS).document(udid).collection(SUB_COLLECTION_INVITEES)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            QuerySnapshot queryDocumentSnapshots = task.getResult();
                            if (queryDocumentSnapshots != null) {
                                List<Invitee> list = new ArrayList<>();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    Invitee invitee = document.toObject(Invitee.class);
                                    list.add(invitee);
                                }
                                iOnGetUserInviteesListener.onSuccess(list);
                            } else {
                                iOnGetUserInviteesListener.onFailure();
                            }
                        } else {
                            iOnGetUserInviteesListener.onFailure();
                        }
                    }
                });
    }

    void setUserRewardedForInvitee(String udid, String inviteeUdid) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(COLLECTION_USERS)
                .document(udid)
                .collection(SUB_COLLECTION_INVITEES)
                .document(inviteeUdid)
                .update("is_rewarded", true);
    }

    boolean isEnteredApp() {
        return getGeneralData().isEnteredApp();
    }

    void setEnteredApp() {
        GeneralData generalData = getGeneralData();
        generalData.setEnteredApp(true);
        updateGeneralData(generalData);
    }

    boolean isLikedFacebookPage() {
        return getGeneralData().ismIsLikedFacebookPage();
    }

    void setFacebookPageLiked() {
        GeneralData generalData = getGeneralData();
        generalData.setmIsLikedFacebookPage(true);
        updateGeneralData(generalData);
    }

    boolean isFollowedInstagramPage() {
        return getGeneralData().ismIsFollowedOnInstagramPage();
    }

    void setInstagramPageFollowed() {
        GeneralData generalData = getGeneralData();
        generalData.setmIsFollowedOnInstagramPage(true);
        updateGeneralData(generalData);
    }

    boolean isDownloadLogotestApp() {
        return getGeneralData().ismIsDownloadLogotest();
    }

    void setDownloadedLogotestApp() {
        GeneralData generalData = getGeneralData();
        generalData.setmIsDownloadLogotest(true);
        updateGeneralData(generalData);
    }

    public void increaseCoinsByAmount(int amount) {
        GeneralData generalData = getGeneralData();
        int money = generalData.getmCoins();
        money += amount;
        generalData.setmCoins(money);
        updateGeneralData(generalData);
    }

    public void decreaseCoinsByAmount(int amount) {
        GeneralData generalData = getGeneralData();
        int money = generalData.getmCoins();
        money -= amount;
        generalData.setmCoins(money);
        updateGeneralData(generalData);
    }

    public int getCoinsAmount() {
        return getGeneralData().getmCoins();
    }

    public String getCoinsAmountAsStr() {
        return String.valueOf(getGeneralData().getmCoins());
    }

    String getLastAppVersion() {
        return getGeneralData().getmLastAppVersion();
    }

    String getAndroidUpdateUrl() {
        return getGeneralData().getmAndroidUrl();
    }

    public long getGameTime() {
        return getGeneralData().getmGameTime();
    }

    public void addToGameTime(long secs) {
        GeneralData generalData = getGeneralData();
        generalData.addGameTime(secs);
        updateGeneralData(generalData);
    }

    public void resetGame() {
        GeneralData generalData = getGeneralData();
        generalData.setmGameTime(0);
        updateGeneralData(generalData);
    }

    boolean isSoundWork() {
        return getGeneralData().isSoundWork();
    }

    void setSound(boolean sound) {
        GeneralData generalData = getGeneralData();
        generalData.setSoundWork(sound);
        updateGeneralData(generalData);
    }

    String getInvitationLink() {
        return getGeneralData().getInvitationLink();
    }

    void setInvitationLink(String link) {
        GeneralData generalData = getGeneralData();
        generalData.setInvitationLink(link);
        updateGeneralData(generalData);
    }

    void setAdsRemoved() {
        GeneralData generalData = getGeneralData();
        generalData.setmIsAdsRemoved(true);
        updateGeneralData(generalData);
    }

    public boolean isAdsRemoved() {
        return getGeneralData().ismIsAdsRemoved();
    }

    void setPurchaseSpecials(boolean isPurchaseSpecials) {
        GeneralData generalData = getGeneralData();
        generalData.setPurchaseSpecials(isPurchaseSpecials);
        updateGeneralData(generalData);
    }

    public boolean isPurchaseSpecials() {
        return getGeneralData().isPurchaseSpecials();
    }

    private String getDbVersion() {
        return getGeneralData().getmDbVersion();
    }

    private void setDbVersion(String dbVersion) {
        GeneralData generalData = getGeneralData();
        generalData.setmDbVersion(dbVersion);
        updateGeneralData(generalData);
    }

    private String getSpecialDbVersioin() {
        return getGeneralData().getSpecialDbVersion();
    }

    private void setSpecialDbVersion(String specialDbVersion) {
        GeneralData generalData = getGeneralData();
        generalData.setSpecialDbVersion(specialDbVersion);
        updateGeneralData(generalData);
    }

    public long getNumberOfLevelsToDisplay() {
        return getGeneralData().getmNumberOfLevelsToDisplay();
    }

    private void setNumOfLevelsToDisplay(long numOfLevelsToDisplay) {
        GeneralData generalData = getGeneralData();
        generalData.setmNumberOfLevelsToDisplay(numOfLevelsToDisplay);
        updateGeneralData(generalData);
    }

    void addInviteesToCounter(int count) {
        GeneralData generalData = getGeneralData();
        generalData.addInviteesToCounter(count);
        updateGeneralData(generalData);
    }

    int getInviteesCounter() {
        return getGeneralData().getInviteesCount();
    }

    void resetInviteesCounter() {
        GeneralData generalData = getGeneralData();
        generalData.resetInviteesCount();
        updateGeneralData(generalData);
    }

    void setGameFinished(boolean isGameFinished) {
        GeneralData generalData = getGeneralData();
        generalData.setGameFinished(isGameFinished);
        updateGeneralData(generalData);
    }

    boolean isGameFinished() {
        return getGeneralData().isGameFinished();
    }

    long getNumOfQuotesBetweenAds() {
        return getGeneralData().getNumOfQuotesBetweenAds();
    }

    void addToInviteFriendSuggestion() {
        GeneralData generalData = getGeneralData();
        int newCnt = generalData.getInviteFriendSuggestionCounter() + 1;
        generalData.setInviteFriendSuggestionCounter(newCnt);
        updateGeneralData(generalData);
    }

    boolean shouldShowInviteFriendSuggestion() {
        GeneralData generalData = getGeneralData();
        return generalData.getInviteFriendSuggestionCounter() % 10 == 4;
    }

    String getInstagramPageUsername() {
        return getGeneralData().getInstagramPageUsername();
    }
}
