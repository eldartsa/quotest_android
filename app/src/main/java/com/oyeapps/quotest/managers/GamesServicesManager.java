package com.oyeapps.quotest.managers;

import android.content.Intent;

import androidx.appcompat.app.AlertDialog;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.games.Games;
import com.google.android.gms.tasks.OnSuccessListener;
import com.oyeapps.quotest.R;
import com.oyeapps.quotest.activities.BaseActivity;

public class GamesServicesManager {

    private static final int RC_LEADERBOARD_UI = 9004;
    public static final int RC_SIGN_IN = 9000;

    enum SignInAction {
        SHOW_LEADERBOARD,
        UPDATE_GAME_TIME_IN_LEADERBOARD,
    }

    private static SignInAction signInAction;

    private static boolean isSignedIn(GoogleSignInAccount googleSignInAccount) {
        return googleSignInAccount != null && GoogleSignIn.hasPermissions(googleSignInAccount);
    }

    static void showLeaderboard(final BaseActivity baseActivity, GoogleSignInAccount googleSignInAccount) {
        if (isSignedIn(googleSignInAccount)) {
            if (DataManager.getInstance().isGameFinished()) {
                updateGameTimeInLeaderboard(baseActivity, googleSignInAccount);
            }
            Games.getLeaderboardsClient(baseActivity, googleSignInAccount)
                    .getLeaderboardIntent(baseActivity.getString(R.string.leaderboard))
                    .addOnSuccessListener(new OnSuccessListener<Intent>() {
                        @Override
                        public void onSuccess(Intent intent) {
                            baseActivity.startActivityForResult(intent, RC_LEADERBOARD_UI);
                        }
                    });
        } else {
            signInAction = SignInAction.SHOW_LEADERBOARD;
            signIn(baseActivity);
        }
    }

    static void updateGameTimeInLeaderboard(BaseActivity baseActivity, GoogleSignInAccount googleSignInAccount) {
        if (isSignedIn(googleSignInAccount)) {
            Games.getLeaderboardsClient(baseActivity, googleSignInAccount)
                    .submitScore(baseActivity.getString(R.string.leaderboard), 1337);
        } else {
            signInAction = SignInAction.UPDATE_GAME_TIME_IN_LEADERBOARD;
            signIn(baseActivity);
        }
    }

    private static void signIn(BaseActivity baseActivity) {
        GoogleSignInClient signInClient = GoogleSignIn.getClient(baseActivity,
                GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN);
        Intent intent = signInClient.getSignInIntent();
        baseActivity.startActivityForResult(intent, RC_SIGN_IN);
    }

    public static void onSignInActivityResult(BaseActivity baseActivity, Intent data) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        if (result.isSuccess()) {
            // The signed in account is stored in the result.
            GoogleSignInAccount signedInAccount = result.getSignInAccount();
            handleActionAfterSignIn(baseActivity, signedInAccount);
        } else {
            String message = result.getStatus().getStatusMessage();
            if (message == null || message.isEmpty()) {
                message = baseActivity.getString(R.string.network_failure_dialog_message);
            }
            new AlertDialog.Builder(baseActivity).setMessage(message)
                    .setNeutralButton(android.R.string.ok, null).show();
        }
    }

    private static void handleActionAfterSignIn(BaseActivity baseActivity, GoogleSignInAccount signedInAccount) {
        switch (signInAction) {
            case SHOW_LEADERBOARD:
                showLeaderboard(baseActivity, signedInAccount);
                break;
            case UPDATE_GAME_TIME_IN_LEADERBOARD:
                updateGameTimeInLeaderboard(baseActivity, signedInAccount);
                break;
        }
    }
}
