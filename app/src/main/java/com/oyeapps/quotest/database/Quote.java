package com.oyeapps.quotest.database;

import android.content.Context;

import com.oyeapps.quotest.BuildConfig;
import com.oyeapps.quotest.R;
import com.oyeapps.quotest.app.MyApplication;
import com.oyeapps.quotest.services.FSQuote;
import com.oyeapps.quotest.utils.Consts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

import static com.oyeapps.quotest.utils.Consts.FLAVOR_ISRAEL;
import static com.oyeapps.quotest.utils.Consts.LANGUAGE_HEBREW;
import static com.oyeapps.quotest.utils.Consts.REVEAL_NORMAL_QUOTE_PERCENT;
import static com.oyeapps.quotest.utils.Consts.REVEAL_SPECIAL_QUOTE_PERCENT;
import static com.oyeapps.quotest.utils.Consts.SOLUTION_LETTER_REVEAL;
import static com.oyeapps.quotest.utils.Consts.STATUS_CLEAN;
import static com.oyeapps.quotest.utils.MyUtils.isSeparator;

/**
 * Created by eldarts on 18/05/2017.
 */

public class Quote extends RealmObject {

    @PrimaryKey
    private int mId = -1;

    private String mCategory = "";
    private int mLanguage = 1;
    private String mOriginalRandomLetters = "";
    private String mEditedRandomLetters = "";
    private String mLastTry = "";
    private boolean resolveQuoteHint = false;
    private boolean removeUnnecessaryLettersHint = false;
    private int revealLetterHint = 0;
    private int mLevel = 1;
    private String mQuote = "";
    private String mQuoteHolder = "";
    private int mStatus = STATUS_CLEAN;
    private long mTimeSolvedInSeconds = 0;

    public Quote(FSQuote fsQuote) {
        this.mId = fsQuote.getId();
        this.mLanguage = fsQuote.getLanguage();
        this.mCategory = fsQuote.getCategory();
        this.mQuote = fsQuote.getQuote();
        this.mQuoteHolder = fsQuote.getQuoteholder();
        this.mLevel = fsQuote.getLevel();
        initQuoteData();
    }

    void initQuoteData() {
        int revealPercent = mLevel < 1000 ? REVEAL_NORMAL_QUOTE_PERCENT : REVEAL_SPECIAL_QUOTE_PERCENT;
        initLastTry(revealPercent);
        initRandomLetters();
    }

    public Quote() {
    }

    private int getLettersCount() {
        int count = 0;
        for (int i = 0; i < mQuoteHolder.length(); i++) {
            if (!isSeparator(mQuoteHolder.charAt(i)))
                count++;
        }
        return count;
    }

    public int getLettersCountWithoutRevealedLetters() {
        int count = 0;
        for (int i = 0; i < mQuoteHolder.length(); i++) {
            if (!isSeparator(mQuoteHolder.charAt(i)) && mLastTry.charAt(i) != SOLUTION_LETTER_REVEAL) {
                count++;
            }
        }
        return count;
    }

    private void initLastTry(int revealPercent) {
        mLastTry = "";
        for (int i = 0; i < mQuoteHolder.length(); i++) {
            char letter = mQuoteHolder.charAt(i);
            if (isSeparator(letter))
                mLastTry += letter;
            else mLastTry += Consts.SOLUTION_LETTER_WITHOUT_VALUE;
        }
        if (getLettersCount() > 3) {
            int charsToReveal = getLettersCount() * revealPercent / 100;
            int j = 0;
            Random random = new Random();
            while (j < charsToReveal) {
                int pos = random.nextInt(mLastTry.length());
                char letter = mLastTry.charAt(pos);
                if (!isSeparator(letter) && letter != SOLUTION_LETTER_REVEAL) {
                    mLastTry = mLastTry.substring(0, pos) + SOLUTION_LETTER_REVEAL + mLastTry.substring(pos + 1);
                    j++;
                }
            }
        }
    }

    private void initRandomLetters() {
        List<Character> randomLetters = new ArrayList<>();
        for (int i = 0; i < mQuoteHolder.length(); i++) {
            char letter = mQuoteHolder.charAt(i);
            if (!isSeparator(letter) && mLastTry.charAt(i) != SOLUTION_LETTER_REVEAL)
                randomLetters.add(letter);
        }

        int randomLettersCount, logoLettersCount;
        logoLettersCount = getLettersCountWithoutRevealedLetters();

        if (logoLettersCount > 14)
            randomLettersCount = 16;
        else randomLettersCount = 14;

        Context context = MyApplication.getInstance().getContext();

        if (BuildConfig.FLAVOR.equals(FLAVOR_ISRAEL)) {
            String[] mHebrewLetters, mEnglishLetters;
            mHebrewLetters = context.getResources().getStringArray(R.array.hebrew_letters_array);
            mEnglishLetters = context.getResources().getStringArray(R.array.english_letters_array);

            Random rnd = new Random();
            for (int i = 0; i < randomLettersCount - logoLettersCount; i++) {
                int rndLetterPos;
                if (mLanguage == LANGUAGE_HEBREW) {
                    rndLetterPos = rnd.nextInt(mHebrewLetters.length);
                    randomLetters.add(mHebrewLetters[rndLetterPos].charAt(0));
                } else {
                    rndLetterPos = rnd.nextInt(mEnglishLetters.length);
                    randomLetters.add(mEnglishLetters[rndLetterPos].charAt(0));
                }
            }
        }

        Collections.shuffle(randomLetters);

        StringBuilder randomLettersStr = new StringBuilder();
        for (Character letter : randomLetters) {
            randomLettersStr.append(letter);
        }

        mOriginalRandomLetters = randomLettersStr.toString();
        mEditedRandomLetters = mOriginalRandomLetters;
    }

    public boolean isRandomLetterSelected() {
        return mEditedRandomLetters.contains(String.valueOf(Consts.RANDOM_LETTER_HIDDEN));
    }

    public boolean isSolutionLayoutFull() {
        return !mLastTry.contains(String.valueOf(Consts.SOLUTION_LETTER_WITHOUT_VALUE));
    }

    public void addTime(long secs) {
        mTimeSolvedInSeconds += secs;
    }

    public String getmQuoteHolder() {
        return mQuoteHolder;
    }

    void setmQuoteHolder(String mQuoteHolder) {
        this.mQuoteHolder = mQuoteHolder;
    }

    void setmTimeSolvedInSeconds(long mTimeSolvedInSeconds) {
        this.mTimeSolvedInSeconds = mTimeSolvedInSeconds;
    }

    public boolean isRemoveUnnecessaryLettersHint() {
        return removeUnnecessaryLettersHint;
    }

    public void setRemoveUnnecessaryLettersHint(boolean removeUnnecessaryLettersHint) {
        this.removeUnnecessaryLettersHint = removeUnnecessaryLettersHint;
    }

    public void revealLetter() {
        this.revealLetterHint++;
    }

    void setRevealLetterHint(int revealLetterHint) {
        this.revealLetterHint = revealLetterHint;
    }

    public void setResolveQuoteHint(boolean resolveQuoteHint) {
        this.resolveQuoteHint = resolveQuoteHint;
    }

    public String getmOriginalRandomLetters() {
        return mOriginalRandomLetters;
    }

    void setmOriginalRandomLetters(String mOriginalRandomLetters) {
        this.mOriginalRandomLetters = mOriginalRandomLetters;
    }

    public String getmEditedRandomLetters() {
        return mEditedRandomLetters;
    }

    public void setmEditedRandomLetters(String mEditedRandomLetters) {
        this.mEditedRandomLetters = mEditedRandomLetters;
    }

    public int getmId() {
        return mId;
    }

    public String getmCategory() {
        return mCategory;
    }

    void setmCategory(String mCategory) {
        this.mCategory = mCategory;
    }

    public int getmLanguage() {
        return mLanguage;
    }

    void setmLanguage(int mLanguage) {
        this.mLanguage = mLanguage;
    }

    public String getmLastTry() {
        return mLastTry;
    }

    public void setmLastTry(String mLastTry) {
        this.mLastTry = mLastTry;
    }

    public int getmLevel() {
        return mLevel;
    }

    void setmLevel(int mLevel) {
        this.mLevel = mLevel;
    }

    public String getmQuote() {
        return mQuote;
    }

    void setmQuote(String mQuote) {
        this.mQuote = mQuote;
    }

    public int getmStatus() {
        return mStatus;
    }

    public void setmStatus(int mStatus) {
        this.mStatus = mStatus;
    }
}
