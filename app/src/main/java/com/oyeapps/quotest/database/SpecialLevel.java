package com.oyeapps.quotest.database;

import java.util.Calendar;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class SpecialLevel extends RealmObject {

    private static final long ONE_WEEK_IN_MILLIS = 604800000L;

    @PrimaryKey
    private int levelId;

    private String levelName;
    private Date dateAdded;
    private boolean isLevelPurchased = false;
    private String imageUrl = "";

    @Ignore
    private int totalQuotes;
    @Ignore
    private int totalSolvedQuotes;

    public SpecialLevel() {
    }

    public boolean isLevelNew() {
        Date oneWeekAgoDate = new Date(Calendar.getInstance().getTimeInMillis() - ONE_WEEK_IN_MILLIS);
        return getDateAdded().after(oneWeekAgoDate);
    }

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public int getTotalQuotes() {
        return totalQuotes;
    }

    public void setTotalQuotes(int totalQuotes) {
        this.totalQuotes = totalQuotes;
    }

    public int getTotalSolvedQuotes() {
        return totalSolvedQuotes;
    }

    public void setTotalSolvedQuotes(int totalSolvedQuotes) {
        this.totalSolvedQuotes = totalSolvedQuotes;
    }

    public boolean isLevelPurchased() {
        return this.isLevelPurchased;
    }

    public void setLevelPurchased(boolean levelPurchased) {
        this.isLevelPurchased = levelPurchased;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
