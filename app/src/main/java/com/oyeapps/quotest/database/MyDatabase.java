package com.oyeapps.quotest.database;

import com.oyeapps.quotest.managers.DataManager;
import com.oyeapps.quotest.services.FSQuote;
import com.oyeapps.quotest.services.FSSpecialLevel;
import com.oyeapps.quotest.utils.ImageUtils;
import com.oyeapps.quotest.utils.Logger;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

import static com.oyeapps.quotest.utils.Consts.OPEN_LEVEL_COMPLETION_PERCENT;
import static com.oyeapps.quotest.utils.Consts.STATUS_CLEAN;
import static com.oyeapps.quotest.utils.Consts.STATUS_SOLVED;

/**
 * Created by eldarts on 28/06/2017.
 */

public class MyDatabase {

    private static MyDatabase myDatabase;

    public static MyDatabase getInstance() {
        if (myDatabase == null) {
            myDatabase = new MyDatabase();
        }
        return myDatabase;
    }

    public void addOrUpdateQuote(Realm realm, FSQuote fsQuote) {
        try {
            if (realm != null && !realm.isClosed()) {
                realm.beginTransaction();
                Quote quoteFromDb = realm.where(Quote.class).equalTo("mId", fsQuote.getId()).findFirst();
                if (quoteFromDb == null) {
                    Quote newQuote = new Quote(fsQuote);
                    realm.copyToRealm(newQuote);
                } else {
                    quoteFromDb.setmCategory(fsQuote.getCategory());
                    quoteFromDb.setmLevel(fsQuote.getLevel());
                    quoteFromDb.setmQuote(fsQuote.getQuote());
                    if (!fsQuote.getQuoteholder().equals(quoteFromDb.getmQuoteHolder()) || fsQuote.getLanguage() != quoteFromDb.getmLanguage()) {
                        quoteFromDb.setmLanguage(fsQuote.getLanguage());
                        quoteFromDb.setmQuoteHolder(fsQuote.getQuoteholder());
                        quoteFromDb.setResolveQuoteHint(false);
                        quoteFromDb.setRemoveUnnecessaryLettersHint(false);
                        quoteFromDb.setRevealLetterHint(0);
                        quoteFromDb.setmEditedRandomLetters("");
                        quoteFromDb.setmOriginalRandomLetters("");
                        if (quoteFromDb.getmStatus() == STATUS_SOLVED) {
                            quoteFromDb.setmLastTry(quoteFromDb.getmQuoteHolder());
                        } else {
                            quoteFromDb.setmLastTry("");
                            quoteFromDb.initQuoteData();
                        }
                    }
                }

                realm.commitTransaction();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void addOrUpdateSpecialLevel(Realm realm, FSSpecialLevel fsSpecialLevel) {
        try {
            if (realm != null && !realm.isClosed()) {
                realm.beginTransaction();

                SpecialLevel specialLevel = realm.where(SpecialLevel.class).equalTo("levelId", fsSpecialLevel.getid()).findFirst();
                if (specialLevel == null) {
                    SpecialLevel newSpecialLevel = new SpecialLevel();
                    newSpecialLevel.setLevelId(fsSpecialLevel.getid());
                    newSpecialLevel.setLevelName(fsSpecialLevel.getname());
                    newSpecialLevel.setDateAdded(Calendar.getInstance().getTime());
                    newSpecialLevel.setImageUrl(ImageUtils.getImageUrlForSpecialLevel(fsSpecialLevel.getLevelNameImg()));
                    realm.copyToRealm(newSpecialLevel);
                } else {
                    specialLevel.setLevelName(fsSpecialLevel.getname());
                    specialLevel.setImageUrl(ImageUtils.getImageUrlForSpecialLevel(fsSpecialLevel.getLevelNameImg()));
                }
                realm.commitTransaction();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public List<SpecialLevel> getAllSpecialLevels(Realm realm) {
        try {
            if (realm != null && !realm.isClosed()) {
                RealmResults<SpecialLevel> specialLevels = realm.where(SpecialLevel.class).findAll();
                return realm.copyToRealm(specialLevels);
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return new ArrayList<>();
    }

    public int getTotalQuotesCount(Realm realm) {
        try {
            if (realm != null && !realm.isClosed()) {
                return (int) realm.where(Quote.class).between("mLevel", 1, DataManager.getInstance().getNumberOfLevelsToDisplay()).count();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return 0;
    }

    public int getTotalQuotesInGameCount(Realm realm) {
        try {
            if (realm != null && !realm.isClosed()) {
                return (int) realm.where(Quote.class).count();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return 0;
    }

    public int getTotalSolvedQuotesInGameCount(Realm realm) {
        try {
            if (realm != null && !realm.isClosed()) {
                return (int) realm.where(Quote.class).
                        equalTo("mStatus", STATUS_SOLVED).count();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return 0;
    }

    public int getTotalUnSolvedQuotesInGameCount(Realm realm) {
        try {
            if (realm != null && !realm.isClosed()) {
                return (int) realm.where(Quote.class)
                        .equalTo("mStatus", STATUS_CLEAN).count();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return 0;
    }

    public int getTotalSolvedQuotesCount(Realm realm) {
        try {
            if (realm != null && !realm.isClosed()) {
                return (int) realm.where(Quote.class).between("mLevel", 1, DataManager.getInstance().getNumberOfLevelsToDisplay()).
                        equalTo("mStatus", STATUS_SOLVED).count();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return 0;
    }

    public int getTotalUnSolvedQuotesCount(Realm realm) {
        try {
            if (realm != null && !realm.isClosed()) {
                return (int) realm.where(Quote.class).between("mLevel", 1, DataManager.getInstance().getNumberOfLevelsToDisplay())
                        .equalTo("mStatus", STATUS_CLEAN).count();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return 0;
    }

    public int getTotalSolvedQuotesInLevelCount(Realm realm, int level) {
        try {
            if (realm != null && !realm.isClosed()) {
                return (int) realm.where(Quote.class).equalTo("mLevel", level).equalTo("mStatus", STATUS_SOLVED).count();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return 0;
    }

    public int getTotalQuotesInLevelCount(Realm realm, int level) {
        try {
            if (realm != null && !realm.isClosed()) {
                return (int) realm.where(Quote.class).equalTo("mLevel", level).count();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return 0;
    }

    public int getTotalQuotesTilLevelCount(Realm realm, int level) {
        try {
            if (realm != null && !realm.isClosed()) {
                return (int) realm.where(Quote.class).between("mLevel", 1, level).count();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return 0;
    }

    public int getAverageTimeToSolveQuote(Realm realm) {
        try {
            if (realm != null && !realm.isClosed()) {
                int solvedQuotesCount = getTotalSolvedQuotesInGameCount(realm);
                int totalTime = (int) (long) realm.where(Quote.class)
                        .equalTo("mStatus", STATUS_SOLVED)
                        .sum("mTimeSolvedInSeconds");
                if (solvedQuotesCount == 0)
                    return 0;
                return totalTime / solvedQuotesCount;
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return 0;
    }

    public boolean isGameCompleted(Realm realm) {
        int totalQuotesCount = getTotalQuotesCount(realm);
        int totalSolvedQuotesCount = getTotalSolvedQuotesCount(realm) + 1;
        return totalQuotesCount == totalSolvedQuotesCount;
    }

    public boolean isLevelCompleted(Realm realm, int level) {
        int totalSolvedQuotesInLevelCount = getTotalSolvedQuotesInLevelCount(realm, level) + 1;
        int totalQuotesInLevelCount = getTotalQuotesInLevelCount(realm, level);
        return totalSolvedQuotesInLevelCount == totalQuotesInLevelCount;
    }

    public List<Quote> getAllQuotes(Realm realm) {
        try {
            if (realm != null && !realm.isClosed()) {
                RealmResults<Quote> quotes = realm.where(Quote.class).between("mLevel", 1, DataManager.getInstance().getNumberOfLevelsToDisplay()).findAll();
                return realm.copyToRealm(quotes);
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return new ArrayList<>();
    }

    public List<Quote> getAllQuotesByLevel(Realm realm, int level) {
        try {
            if (realm != null && !realm.isClosed()) {
                RealmResults<Quote> quotes = realm.where(Quote.class).equalTo("mLevel", level).findAll();
                return realm.copyFromRealm(quotes);
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return new ArrayList<>();
    }

    public Quote getQuoteById(Realm realm, int id) {
        try {
            if (realm != null && !realm.isClosed()) {
                Quote quote = realm.where(Quote.class).equalTo("mId", id).findFirst();
                if (quote != null)
                    return realm.copyFromRealm(quote);
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return new Quote();
    }

    public int getRemoveUnnecessaryLetterHintCount(Realm realm) {
        try {
            if (realm != null && !realm.isClosed()) {
                return (int) realm.where(Quote.class)
                        .equalTo("removeUnnecessaryLettersHint", true).count();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return 0;
    }

    public int getResolveQuoteHintCount(Realm realm) {
        try {
            if (realm != null && !realm.isClosed()) {
                return (int) realm.where(Quote.class)
                        .equalTo("resolveQuoteHint", true).count();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return 0;
    }

    public int getRevealLetterHintCount(Realm realm) {
        try {
            if (realm != null && !realm.isClosed()) {
                return (int) (long) realm.where(Quote.class)
                        .sum("revealLetterHint");
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return 0;
    }

    public void updateQuote(Realm realm, Quote quote) {
        try {
            if (realm != null && !realm.isClosed()) {
                realm.beginTransaction();
                realm.copyToRealmOrUpdate(quote);
                realm.commitTransaction();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    //Returns the number of the level that opened, otherwise return -1
    public int isNewLevelOpened(Realm realm) {
        try {
            long numOfLevelsToDisplay = DataManager.getInstance().getNumberOfLevelsToDisplay();
            int totalSolvedLogos = getTotalSolvedQuotesCount(realm) + 1;
            for (int i = 1; i < numOfLevelsToDisplay; i++) {
                int minSolvedQuotesToOpen = getTotalQuotesTilLevelCount(realm, i) * OPEN_LEVEL_COMPLETION_PERCENT / 100;
                if (totalSolvedLogos == minSolvedQuotesToOpen) {
                    return i;
                }
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return -1;
    }

    public boolean isSpecialLevelPurchased(Realm realm, int levelId) {
        try {
            if (realm != null && !realm.isClosed()) {
                SpecialLevel specialLevel = realm.where(SpecialLevel.class).equalTo("levelId", levelId).findFirst();
                if (specialLevel != null)
                    return specialLevel.isLevelPurchased();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return false;
    }

    public void resetGame(Realm realm) {
        try {
            if (realm != null && !realm.isClosed()) {
                DataManager.getInstance().resetGame();

                RealmResults<Quote> quotes = realm.where(Quote.class).findAll();

                realm.beginTransaction();
                for (Quote quote : quotes) {
                    quote.setmLastTry("");
                    quote.setmOriginalRandomLetters("");
                    quote.setmEditedRandomLetters("");
                    quote.setmStatus(STATUS_CLEAN);
                    quote.setRemoveUnnecessaryLettersHint(false);
                    quote.setResolveQuoteHint(false);
                    quote.setRevealLetterHint(0);
                    quote.setmTimeSolvedInSeconds(0);
                    quote.initQuoteData();
                }
                realm.commitTransaction();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void setSpecialLevelPurchased(Realm realm, int levelId) {
        try {
            if (realm != null && !realm.isClosed()) {
                realm.beginTransaction();
                SpecialLevel specialLevel = realm.where(SpecialLevel.class).equalTo("levelId", levelId).findFirst();
                if (specialLevel != null) {
                    specialLevel.setLevelPurchased(true);
                }
                realm.commitTransaction();
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }
}
