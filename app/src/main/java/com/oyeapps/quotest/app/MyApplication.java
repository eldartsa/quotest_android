package com.oyeapps.quotest.app;

import android.content.Context;
import android.content.res.Configuration;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.FirebaseApp;
import com.oyeapps.quotest.R;
import com.oyeapps.quotest.managers.PurchasesManager;
import com.oyeapps.quotest.utils.Logger;
import com.oyeapps.quotest.widgets.LocaleUtils;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;

/**
 * Created by eldarts on 18/05/2017.
 */

public class MyApplication extends MultiDexApplication {

    private static final String TAG = MyApplication.class.getSimpleName();

    private static MyApplication mInstance;

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Logger.d(Logger.TEST, TAG, "onCreate App!");

        mInstance = this;

        LocaleUtils.setLocale(new Locale(getString(R.string.locale)));
        LocaleUtils.updateConfig(this, getBaseContext().getResources().getConfiguration());

        Fabric.with(this, new Crashlytics());
        FirebaseApp.initializeApp(this);
        MobileAds.initialize(this, getString(R.string.admob_app_id));

        Realm.init(this);

        PurchasesManager.getInstance().initBillingProcessor(this);
//        final RealmConfiguration configuration = new RealmConfiguration.Builder()
//                .name(RealmConfiguration.DEFAULT_REALM_NAME)
//                .schemaVersion(1)
//                .migration(new Migration())
//                .build();
//        Realm.setDefaultConfiguration(configuration);
//        Realm.getInstance(configuration);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        try {
            PurchasesManager.getInstance().releaseBillingProcessor();
            Realm.getDefaultInstance().close();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //LocaleUtils.updateConfig(this, newConfig);
    }

    public Context getContext() {
        return this;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }
}
