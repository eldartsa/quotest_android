package com.oyeapps.quotest.fragments;


import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.adapters.AllQuotesAdapter;
import com.oyeapps.quotest.data_obj.ActionBarItem;
import com.oyeapps.quotest.database.MyDatabase;
import com.oyeapps.quotest.database.Quote;
import com.oyeapps.quotest.extraData.AllQuotesFragmentExtraData;
import com.oyeapps.quotest.extraData.ExtraDataItem;
import com.oyeapps.quotest.extraData.QuoteFragmentExtraData;
import com.oyeapps.quotest.helpers.SnackBarHelper;
import com.oyeapps.quotest.managers.NavigationManager;
import com.oyeapps.quotest.managers.SoundManager;
import com.oyeapps.quotest.utils.Logger;
import com.oyeapps.quotest.utils.MyUtils;
import com.oyeapps.quotest.widgets.GridRecyclerView;

import java.util.List;

import static com.oyeapps.quotest.utils.Consts.SOUND_CLICK;

public class AllQuotesFragment extends BaseFragment implements AllQuotesAdapter.IOnQuoteClickListener {

    private AllQuotesAdapter mAdapter;
    private GridRecyclerView mGridRecyclerView;
    private RelativeLayout mRootTl;

    private AllQuotesFragmentExtraData.SourceView mSourceView = AllQuotesFragmentExtraData.SourceView.LEVELS_FRAGMENT;
    private int mLevelNum = 0;
    private String mLevelName = "";

    @Override
    ActionBarItem getActionBarItem() {
        return ActionBarItem.createSimpleActionBar(mLevelName);
    }

    @Override
    int getFragmentLayout() {
        return R.layout.fragment_all_quotes;
    }

    @Override
    void initFragmentView(View view) {
        mGridRecyclerView = view.findViewById(R.id.rv_quotes);
        mRootTl = view.findViewById(R.id.root);
    }

    @Override
    void initFragmentContent() {
        List<Quote> quoteList = MyDatabase.getInstance().getAllQuotesByLevel(getRealm(), mLevelNum);
        if (mAdapter == null) {
            mAdapter = new AllQuotesAdapter(quoteList, this);
            int resId = R.anim.grid_layout_animation_from_bottom;
            LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(), resId);
            mGridRecyclerView.setLayoutAnimation(animation);
        } else {
            mAdapter.update(quoteList);
        }

        int badgeSide = (int) (MyUtils.getScreenWidth() * 0.3);
        mGridRecyclerView.setHasFixedSize(true);
        mGridRecyclerView.setItemViewCacheSize(badgeSide);
        mGridRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mGridRecyclerView.setAdapter(null);
        mGridRecyclerView = null;
        mRootTl = null;
    }

    @Override
    protected void initBundleData(Bundle bundle) {
        ExtraDataItem extraData = bundle.getParcelable(EXTRA_DATA);
        if (extraData instanceof AllQuotesFragmentExtraData) {
            AllQuotesFragmentExtraData allQuotesFragmentExtraData = (AllQuotesFragmentExtraData) extraData;
            mLevelNum = allQuotesFragmentExtraData.getLevelNum();
            mSourceView = allQuotesFragmentExtraData.getSourceView();
            mLevelName = allQuotesFragmentExtraData.getLevelName();
        }
    }

    @Override
    public void onQuoteClick(Quote quote, int pos) {
        try {
            QuoteFragmentExtraData.SourceView sourceView;
            if (mSourceView == AllQuotesFragmentExtraData.SourceView.LEVELS_FRAGMENT) {
                sourceView = QuoteFragmentExtraData.SourceView.LEVELS_FRAGMENT;
            } else {
                sourceView = QuoteFragmentExtraData.SourceView.SPECIAL_LEVELS_FRAGMENT;
            }
            QuoteFragmentExtraData extraData = new QuoteFragmentExtraData(sourceView, quote.getmId(), pos + 1, mLevelName);
            NavigationManager.getInstance().goToQuoteFragment(getActivity(), extraData);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @Override
    public void onQuoteLongClick(Quote quote) {
        SoundManager.getInstance().playSound(SOUND_CLICK);
        SnackBarHelper.getInstance().showLongSnackBar(mRootTl, quote.getmQuote());
    }

    public static AllQuotesFragment newInstance(ExtraDataItem extraData) {

        Bundle args = new Bundle();
        args.putParcelable(EXTRA_DATA, extraData);

        AllQuotesFragment fragment = new AllQuotesFragment();
        fragment.setArguments(args);
        return fragment;
    }
}

