package com.oyeapps.quotest.fragments;

import android.content.Intent;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.oyeapps.quotest.R;
import com.oyeapps.quotest.managers.PurchasesManager;
import com.oyeapps.quotest.utils.Logger;
import com.oyeapps.quotest.utils.MyUtils;

import static com.anjlab.android.iab.v3.Constants.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED;
import static com.anjlab.android.iab.v3.Constants.BILLING_RESPONSE_RESULT_USER_CANCELED;

public abstract class BasePurchasesFragment extends BaseFragment implements PurchasesManager.IOnPurchasesListener {

    @Override
    public void onResume() {
        super.onResume();
        PurchasesManager.getInstance().setListener(this);
        PurchasesManager.getInstance().getBp().loadOwnedPurchasesFromGoogle();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            BillingProcessor bp = PurchasesManager.getInstance().getBp();
            if (bp != null) {
                if (!bp.handleActivityResult(requestCode, resultCode, data)) {
                    super.onActivityResult(requestCode, resultCode, data);
                }
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @Override
    public void onBillingError(int errorCode) {
        if (getContext() != null) {
            switch (errorCode) {
                case BILLING_RESPONSE_RESULT_USER_CANCELED:
                    MyUtils.showToast(getContext(), getString(R.string.user_canceled_item_purchase_toast_message));
                    break;
                case BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED:
                    MyUtils.showToast(getContext(), getString(R.string.item_already_purchased_toast_message));
                    break;
                default:
                    MyUtils.showToast(getContext(), getString(R.string.item_purchase_error_toast_message));
                    break;
            }
        }
    }

    @Override
    public void onSpecialsPurchased() {

    }

    @Override
    public void onSpecialLevelPurchased(int levelId, String levelName) {

    }

    @Override
    public void onRemoveAdsPurchased() {
        
    }
}
