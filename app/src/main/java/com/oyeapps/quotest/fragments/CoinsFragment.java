package com.oyeapps.quotest.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.oyeapps.quotest.BuildConfig;
import com.oyeapps.quotest.R;
import com.oyeapps.quotest.adapters.GetCoinsAdapter;
import com.oyeapps.quotest.data_obj.ActionBarItem;
import com.oyeapps.quotest.dialogs.PurchaseSpecialsDialog;
import com.oyeapps.quotest.interfaces.IGetCoins;
import com.oyeapps.quotest.interfaces.IOnWatchAnotherVideoAdDialogListener;
import com.oyeapps.quotest.managers.AdsManager;
import com.oyeapps.quotest.managers.GeneralManager;
import com.oyeapps.quotest.managers.NavigationManager;
import com.oyeapps.quotest.managers.PurchasesManager;
import com.oyeapps.quotest.managers.SoundManager;
import com.oyeapps.quotest.type_obj.GetCoinsInAppBilling;
import com.oyeapps.quotest.type_obj.GetCoinsNormal;
import com.oyeapps.quotest.utils.Consts;
import com.oyeapps.quotest.utils.DialogUtils;
import com.oyeapps.quotest.utils.Logger;
import com.oyeapps.quotest.utils.MyUtils;

import java.util.List;
import java.util.Objects;

import static com.oyeapps.quotest.managers.PurchasesManager.SKU_REMOVE_ADS;
import static com.oyeapps.quotest.utils.Consts.LOGOTEST_APP_PACKAGE_NAME;
import static com.oyeapps.quotest.utils.Consts.SOUND_CLICK;

public class CoinsFragment extends BasePurchasesFragment implements GetCoinsAdapter.IOnInAppBillingClickListener,
        GetCoinsAdapter.IOnNormalClickListener, AdsManager.GetCoinsRewardListener, View.OnClickListener {

    private GetCoinsAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private CardView mCloseCv;

    private boolean mIsReturnFromFacebookPage, mIsReturnFromInstagramPage, mIsReturnFromDownloadLogotest;

    private RewardedVideoAd mGetCoinsRewardVideoAd;

    private Handler mHandler = new Handler();


    @Override
    ActionBarItem getActionBarItem() {
        return null;
    }

    @Override
    int getFragmentLayout() {
        return R.layout.fragment_coins;
    }

    @Override
    void initFragmentView(View view) {
        mCloseCv = view.findViewById(R.id.cv_close);
        mRecyclerView = view.findViewById(R.id.rv_get_coins);
    }

    @Override
    void initFragmentContent() {
        mGetCoinsRewardVideoAd = MobileAds.getRewardedVideoAdInstance(getContext());
        AdsManager.getInstance().setRewardedVideoAd(mGetCoinsRewardVideoAd, getString(R.string.get_coins_reward_ad_unit_id));

        mCloseCv.setOnClickListener(this);

        List<IGetCoins> getCoinsList = GeneralManager.getInstance().getDataForCoinsFragment(getBaseActivity());
        if (mAdapter == null) {
            mAdapter = new GetCoinsAdapter(getCoinsList, this, this);
        } else {
            mAdapter.update(getCoinsList);
        }
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        AdsManager.getInstance().setmListener(this);
        if (mIsReturnFromFacebookPage) {
            GeneralManager.getInstance().returnFromFacebookPage();
            mAdapter.onNormalRowTypeRewarded(GetCoinsNormal.rowType.FACEBOOK);
            mIsReturnFromFacebookPage = false;
        }
        if (mIsReturnFromInstagramPage) {
            GeneralManager.getInstance().returnFromInstagramPage();
            mAdapter.onNormalRowTypeRewarded(GetCoinsNormal.rowType.INSTAGRAM);
            mIsReturnFromInstagramPage = false;
        }
        if (mIsReturnFromDownloadLogotest) {
            GeneralManager.getInstance().returnFromLogotestDownload();
            mAdapter.onNormalRowTypeRewarded(GetCoinsNormal.rowType.DOWNLOAD_LOGOTEST);
            mIsReturnFromDownloadLogotest = false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mRecyclerView.setAdapter(null);
            mRecyclerView = null;
            mCloseCv = null;
            mHandler.removeCallbacksAndMessages(null);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @Override
    public void onInAppBillingClick(GetCoinsInAppBilling getCoinsInAppBilling) {
        SoundManager.getInstance().playSound(SOUND_CLICK);
        if (getCoinsInAppBilling.getProductId().equals(SKU_REMOVE_ADS)) {
            PurchasesManager.getInstance().purchaseRemoveAds(getBaseActivity());
        } else {
            PurchasesManager.getInstance().consumeProduct(getBaseActivity(), getCoinsInAppBilling.getProductId());
        }
    }

    @Override
    public void onSpecialDealClick() {
        GeneralManager.getInstance().showPurchaseSpecialsDialog(getBaseActivity(), new PurchaseSpecialsDialog.SubscriptionDialogListener() {
            @Override
            public void onStartSubscriptionClicked() {
                PurchasesManager.getInstance().purchaseSpecials(getBaseActivity(), Consts.PurchaseSubscriptionFrom.COINS_POPUP);
            }
        });
    }

    @Override
    public void onNormalClick(GetCoinsNormal.rowType rowType) {
        SoundManager.getInstance().playSound(SOUND_CLICK);
        switch (rowType) {
            case FACEBOOK:
                mIsReturnFromFacebookPage = true;
                NavigationManager.getInstance().openFacebookPage(getBaseActivity());
                break;
            case INSTAGRAM:
                mIsReturnFromInstagramPage = true;
                NavigationManager.getInstance().openInstagramPage(getBaseActivity());
                break;
            case VIDEO:
                AdsManager.getInstance().showGetCoinsRewardVideo(mGetCoinsRewardVideoAd, getString(R.string.get_coins_reward_ad_unit_id));
                break;
            case INVITE_FRIEND:
                GeneralManager.getInstance().goToInviteFriendView(getBaseActivity(), false);
                break;
            case DOWNLOAD_LOGOTEST:
                if (MyUtils.isPackageInstalled(LOGOTEST_APP_PACKAGE_NAME, Objects.requireNonNull(getActivity()).getPackageManager())) {
                    NavigationManager.getInstance().navigateToApp(getBaseActivity(), LOGOTEST_APP_PACKAGE_NAME);
                } else {
                    mIsReturnFromDownloadLogotest = true;
                    NavigationManager.getInstance().openAppInPlayStore(getBaseActivity(), LOGOTEST_APP_PACKAGE_NAME);
                }
                break;
        }
    }

    @Override
    public void onRankUsClick() {
        try {
            SoundManager.getInstance().playSound(SOUND_CLICK);
            NavigationManager.getInstance().openAppInPlayStore(getBaseActivity(), BuildConfig.APPLICATION_ID);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @Override
    public void onAdClosed() {
        try {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.showAnotherRewardVideoAdDialog(getBaseActivity(), new IOnWatchAnotherVideoAdDialogListener() {
                        @Override
                        public void onWatchNowClick() {
                            if (isActivityRunning()) {
                                AdsManager.getInstance().showGetCoinsRewardVideo(mGetCoinsRewardVideoAd, getBaseActivity().getString(R.string.get_coins_reward_ad_unit_id));
                            }
                        }
                    });
                }
            }, 200);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @Override
    public void onAdFailedToLoad() {
        if (getBaseActivity() != null) {
            DialogUtils.showVideoAdFailedToLoadDialog(getBaseActivity());
        }
    }

    @Override
    public void onRemoveAdsPurchased() {
        super.onRemoveAdsPurchased();
        mAdapter.onRemoveAds();
    }

    @Override
    public void onSpecialsPurchased() {
        super.onSpecialsPurchased();
        mAdapter.onPurchaseSpecials();
        mAdapter.onRemoveAds();
    }

    public static CoinsFragment newInstance() {

        Bundle args = new Bundle();

        CoinsFragment fragment = new CoinsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(View v) {
        try {
            SoundManager.getInstance().playSound(SOUND_CLICK);
            Objects.requireNonNull(getActivity()).onBackPressed();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }
}
