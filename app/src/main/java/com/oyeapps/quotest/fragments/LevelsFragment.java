package com.oyeapps.quotest.fragments;

import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import androidx.recyclerview.widget.RecyclerView;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.adapters.LevelsAdapter;
import com.oyeapps.quotest.data_obj.ActionBarItem;
import com.oyeapps.quotest.extraData.AllQuotesFragmentExtraData;
import com.oyeapps.quotest.interfaces.ILevel;
import com.oyeapps.quotest.managers.FirebaseManager;
import com.oyeapps.quotest.managers.GeneralManager;
import com.oyeapps.quotest.managers.NavigationManager;
import com.oyeapps.quotest.managers.SoundManager;
import com.oyeapps.quotest.utils.Logger;

import java.util.List;

import static com.oyeapps.quotest.utils.Consts.OPEN_LEVEL_EVENT_NAME;
import static com.oyeapps.quotest.utils.Consts.SOUND_CLICK;

public class LevelsFragment extends BaseFragment implements LevelsAdapter.LevelsListener {

    private LevelsAdapter mAdapter;
    private RecyclerView mRecyclerView;

    @Override
    ActionBarItem getActionBarItem() {
        return ActionBarItem.createSimpleActionBar("");
    }

    @Override
    int getFragmentLayout() {
        return R.layout.fragment_levels;
    }

    @Override
    void initFragmentView(View view) {
        mRecyclerView = view.findViewById(R.id.rv_levels);
    }

    @Override
    void initFragmentContent() {
        List<ILevel> dataList = GeneralManager.getInstance().getDataForLevelsFragment(getRealm());
        if (mAdapter == null) {
            mAdapter = new LevelsAdapter(this, dataList);
            int resId = R.anim.layout_animation_fall_down;
            LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(), resId);
            mRecyclerView.setLayoutAnimation(animation);
        } else {
            mAdapter.updateList(dataList);
        }

        mRecyclerView.setAdapter(mAdapter);

        GeneralManager.getInstance().showInviteFriendSuggestionIfNeed(getBaseActivity());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mRecyclerView.setAdapter(null);
        mRecyclerView = null;
    }

    @Override
    public void onOpenLevelClicked(int levelNum) {
        try {
            SoundManager.getInstance().playSound(SOUND_CLICK);
            FirebaseManager.getInstance().reportEvent(OPEN_LEVEL_EVENT_NAME, getString(R.string.level, levelNum));
            AllQuotesFragmentExtraData allQuotesFragmentExtraData
                    = new AllQuotesFragmentExtraData(AllQuotesFragmentExtraData.SourceView.LEVELS_FRAGMENT, levelNum, getString(R.string.level, levelNum));
            NavigationManager.getInstance().goToAllQuotesFragment(getBaseActivity(), allQuotesFragmentExtraData);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @Override
    public void onLevelSpecialClick() {
        SoundManager.getInstance().playSound(SOUND_CLICK);
        NavigationManager.getInstance().goToSpecialLevelsFragment(getBaseActivity());
    }

    public static LevelsFragment newInstance() {

        Bundle args = new Bundle();

        LevelsFragment fragment = new LevelsFragment();
        fragment.setArguments(args);
        return fragment;
    }
}