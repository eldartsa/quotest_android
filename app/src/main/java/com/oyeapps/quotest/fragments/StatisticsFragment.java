package com.oyeapps.quotest.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.data_obj.ActionBarItem;
import com.oyeapps.quotest.database.MyDatabase;
import com.oyeapps.quotest.managers.DataManager;
import com.oyeapps.quotest.utils.Logger;
import com.oyeapps.quotest.utils.MyUtils;

public class StatisticsFragment extends BaseFragment {

    private TextView mLogosCountTv, mSolvedLogosCountTv, mUnSolvedLogosCountTv, mGameTimeTv, mProgressPercentTv, mTotalHintsTv,
            mResolveQuoteHintsTv, mRemoveUnnecessaryLettersHintsTv, mRevealLetterHintTv, mAverageTimeToSolveQuote;

    @Override
    ActionBarItem getActionBarItem() {
        return ActionBarItem.createSimpleActionBar(getString(R.string.statistics));
    }

    @Override
    int getFragmentLayout() {
        return R.layout.fragment_statistics;
    }

    @Override
    void initFragmentView(View view) {
        mLogosCountTv = view.findViewById(R.id.FragmentStatisticsLogosCountTv);
        mSolvedLogosCountTv = view.findViewById(R.id.FragmentStatisticsSolvedLogosCountTv);
        mUnSolvedLogosCountTv = view.findViewById(R.id.FragmentStatisticsUnSolvedLogosCountTv);
        mGameTimeTv = view.findViewById(R.id.tv_game_time);
        mProgressPercentTv = view.findViewById(R.id.FragmentStatisticsProgressPercentTv);
        mTotalHintsTv = view.findViewById(R.id.tv_total_hints);
        mResolveQuoteHintsTv = view.findViewById(R.id.tv_resolve_quote);
        mRemoveUnnecessaryLettersHintsTv = view.findViewById(R.id.tv_remove_unnecessary_letters);
        mRevealLetterHintTv = view.findViewById(R.id.tv_reveal_letter);
        mAverageTimeToSolveQuote = view.findViewById(R.id.tv_average_time_to_solve_quote);
    }

    @Override
    void initFragmentContent() {
        try {
            int totalQuotesInGameCount = MyDatabase.getInstance().getTotalQuotesInGameCount(getRealm());
            int totalSolvedQuotes = MyDatabase.getInstance().getTotalSolvedQuotesInGameCount(getRealm());
            int totalUnSolvedQuotes = MyDatabase.getInstance().getTotalUnSolvedQuotesInGameCount(getRealm());
            int resolveQuoteHint = MyDatabase.getInstance().getResolveQuoteHintCount(getRealm());
            int removeUnnecessaryLettersHints = MyDatabase.getInstance().getRemoveUnnecessaryLetterHintCount(getRealm());
            int revealLetterHint = MyDatabase.getInstance().getRevealLetterHintCount(getRealm());
            int averageTimeToSolveQuote = MyDatabase.getInstance().getAverageTimeToSolveQuote(getRealm());

            mLogosCountTv.setText(String.valueOf(totalQuotesInGameCount));
            mSolvedLogosCountTv.setText(String.valueOf(totalSolvedQuotes));
            mUnSolvedLogosCountTv.setText(String.valueOf(totalUnSolvedQuotes));
            mGameTimeTv.setText(MyUtils.getGameTimeFormatted(getActivity(), DataManager.getInstance().getGameTime()));
            int totalPercent = (totalSolvedQuotes * 100) / totalQuotesInGameCount;
            String progress = totalPercent + getString(R.string.percent);
            mProgressPercentTv.setText(progress);

            int totalHints = resolveQuoteHint + removeUnnecessaryLettersHints + revealLetterHint;
            mTotalHintsTv.setText(String.valueOf(totalHints));
            mResolveQuoteHintsTv.setText(String.valueOf(resolveQuoteHint));
            mRemoveUnnecessaryLettersHintsTv.setText(String.valueOf(removeUnnecessaryLettersHints));
            mRevealLetterHintTv.setText(String.valueOf(revealLetterHint));

            String txt = averageTimeToSolveQuote + " " + getString(R.string.seconds);
            this.mAverageTimeToSolveQuote.setText(txt);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mLogosCountTv = null;
        mSolvedLogosCountTv = null;
        mUnSolvedLogosCountTv = null;
        mGameTimeTv = null;
        mProgressPercentTv = null;
        mTotalHintsTv = null;
        mResolveQuoteHintsTv = null;
        mRemoveUnnecessaryLettersHintsTv = null;
        mRevealLetterHintTv = null;
        mAverageTimeToSolveQuote = null;
    }

    public static StatisticsFragment newInstance() {

        Bundle args = new Bundle();

        StatisticsFragment fragment = new StatisticsFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
