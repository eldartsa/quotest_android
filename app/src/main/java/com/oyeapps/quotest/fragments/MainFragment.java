package com.oyeapps.quotest.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.data_obj.ActionBarItem;
import com.oyeapps.quotest.managers.DataManager;
import com.oyeapps.quotest.managers.GeneralManager;
import com.oyeapps.quotest.managers.NavigationManager;
import com.oyeapps.quotest.managers.PurchasesManager;
import com.oyeapps.quotest.managers.SoundManager;
import com.oyeapps.quotest.utils.AnimUtils;
import com.oyeapps.quotest.utils.Logger;
import com.oyeapps.quotest.utils.MyUtils;

import static com.oyeapps.quotest.utils.Consts.SOUND_CLICK;

public class MainFragment extends BasePurchasesFragment implements View.OnClickListener {

    private OnBackPressState onBackPressState = OnBackPressState.SHOW_ALERT_TOAST;

    private enum OnBackPressState {
        SHOW_ALERT_TOAST,
        EXIT
    }

    private Handler mHandler = new Handler();

    private ImageView mRemoveAdsIv;

    @Override
    ActionBarItem getActionBarItem() {
        return null;
    }

    @Override
    int getFragmentLayout() {
        return R.layout.fragment_main;
    }

    @Override
    void initFragmentView(View view) {
        TextView startIv = view.findViewById(R.id.tv_start);
        mRemoveAdsIv = view.findViewById(R.id.iv_remove_ads);
        ImageView settingsIv = view.findViewById(R.id.iv_settings);
        ImageView inviteFriendsIv = view.findViewById(R.id.iv_invite_friends);
        LinearLayout getCoinsLL = view.findViewById(R.id.ll_get_coins);
        ImageView trophyIv = view.findViewById(R.id.iv_trophy);

        startIv.setOnClickListener(this);
        mRemoveAdsIv.setOnClickListener(this);
        settingsIv.setOnClickListener(this);
        getCoinsLL.setOnClickListener(this);
        inviteFriendsIv.setOnClickListener(this);
        trophyIv.setOnClickListener(this);

        if (DataManager.getInstance().isAdsRemoved()) {
            mRemoveAdsIv.setVisibility(View.GONE);
        } else {
            AnimUtils.removeAdsAnimation(getContext(), mRemoveAdsIv);
        }
    }

    public static MainFragment newInstance() {

        Bundle args = new Bundle();

        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(View v) {
        SoundManager.getInstance().playSound(SOUND_CLICK);
        switch (v.getId()) {
            case R.id.tv_start:
                NavigationManager.getInstance().goToGameActivity(getBaseActivity());
                break;
            case R.id.iv_remove_ads:
                PurchasesManager.getInstance().purchaseRemoveAds(getBaseActivity());
                break;
            case R.id.iv_settings:
                NavigationManager.getInstance().goToSettingActivity(getBaseActivity());
                break;
            case R.id.ll_get_coins:
                NavigationManager.getInstance().goToCoinsActivity(getBaseActivity());
                break;
            case R.id.iv_invite_friends:
                GeneralManager.getInstance().goToInviteFriendView(getBaseActivity(), false);
                break;
            case R.id.iv_trophy:
                GeneralManager.getInstance().goToLeaderboard(getBaseActivity());
                break;
        }
    }

    @Override
    public void onRemoveAdsPurchased() {
        super.onRemoveAdsPurchased();
        try {
            mRemoveAdsIv.setVisibility(View.GONE);
            mRemoveAdsIv.clearAnimation();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @Override
    public boolean isFragmentHandleBackPress() {
        switch (onBackPressState) {
            case SHOW_ALERT_TOAST:
                onBackPressState = OnBackPressState.EXIT;
                MyUtils.showToast(getContext(), getString(R.string.click_again_to_exit_toast_message));
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressState = OnBackPressState.SHOW_ALERT_TOAST;
                    }
                }, 2000);
                return true;
            case EXIT:
                return false;
        }
        return false;
    }
}
