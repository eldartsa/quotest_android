package com.oyeapps.quotest.fragments;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.adapters.SettingsAdapter;
import com.oyeapps.quotest.data_obj.ActionBarItem;
import com.oyeapps.quotest.interfaces.IOnSoundClickListener;
import com.oyeapps.quotest.interfaces.ISettings;
import com.oyeapps.quotest.managers.GeneralManager;
import com.oyeapps.quotest.managers.NavigationManager;
import com.oyeapps.quotest.managers.SoundManager;
import com.oyeapps.quotest.type_obj.SettingsNormal;
import com.oyeapps.quotest.utils.DialogUtils;
import com.oyeapps.quotest.utils.Logger;
import com.oyeapps.quotest.utils.MyUtils;

import java.util.List;
import java.util.Objects;

import static com.oyeapps.quotest.utils.Consts.SOUND_CLICK;

public class SettingsFragment extends BaseFragment implements SettingsAdapter.IOnNormalClickListener {

    private RecyclerView mRecyclerView;
    private SettingsAdapter mAdapter;

    @Override
    ActionBarItem getActionBarItem() {
        return ActionBarItem.createSimpleActionBar(getString(R.string.setting));
    }

    @Override
    int getFragmentLayout() {
        return R.layout.fragment_settings;
    }

    @Override
    void initFragmentView(View view) {
        mRecyclerView = view.findViewById(R.id.rv_settings);
    }

    @Override
    void initFragmentContent() {
        if (mAdapter == null) {
            List<ISettings> settingsList = GeneralManager.getInstance().getDataForSettingsFragment(Objects.requireNonNull(getContext()));
            mAdapter = new SettingsAdapter(settingsList, this);
        }
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mRecyclerView.setAdapter(null);
        mAdapter = null;
    }

    @Override
    public void onNormalClick(SettingsNormal.RowType rowType) {
        SoundManager.getInstance().playSound(SOUND_CLICK);
        switch (rowType) {
            case SOUNDS:
                GeneralManager.getInstance().onSoundClick(new IOnSoundClickListener() {
                    @Override
                    public void onSoundChange(boolean sound) {
                        try {
                            mAdapter.updateSound(sound);
                        } catch (Exception e) {
                            Logger.e(Logger.TEST, e.getMessage(), e);
                        }
                    }
                });
                break;
            case RESET_GAME:
                DialogUtils.showResetGameDialog(getActivity(), getRealm());
                break;
            case STATISTICS:
                NavigationManager.getInstance().goToStatisticsFragment(getBaseActivity());
                break;
            case PRIVACY_POLICY:
                NavigationManager.getInstance().openPrivacyPolicy(getBaseActivity());
                break;
            case TOKEN:
                MyUtils.copyFirebasePushNotificationTokenToClipboard(getActivity());
                break;
        }
    }

    public static SettingsFragment newInstance() {

        Bundle args = new Bundle();

        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
