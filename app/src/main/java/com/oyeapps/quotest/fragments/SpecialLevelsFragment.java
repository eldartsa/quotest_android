package com.oyeapps.quotest.fragments;


import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import androidx.recyclerview.widget.RecyclerView;

import com.oyeapps.quotest.BuildConfig;
import com.oyeapps.quotest.R;
import com.oyeapps.quotest.adapters.SpecialLevelsAdapter;
import com.oyeapps.quotest.data_obj.ActionBarItem;
import com.oyeapps.quotest.database.MyDatabase;
import com.oyeapps.quotest.database.SpecialLevel;
import com.oyeapps.quotest.dialogs.PurchaseSpecialsDialog;
import com.oyeapps.quotest.extraData.AllQuotesFragmentExtraData;
import com.oyeapps.quotest.interfaces.IOnNeedToPurchaseSpecialsDialog;
import com.oyeapps.quotest.managers.DataManager;
import com.oyeapps.quotest.managers.GeneralManager;
import com.oyeapps.quotest.managers.NavigationManager;
import com.oyeapps.quotest.managers.PurchasesManager;
import com.oyeapps.quotest.managers.SoundManager;
import com.oyeapps.quotest.utils.Consts;
import com.oyeapps.quotest.utils.DialogUtils;
import com.oyeapps.quotest.utils.Logger;

import java.util.List;

import static com.oyeapps.quotest.utils.Consts.SOUND_CLICK;

public class SpecialLevelsFragment extends BasePurchasesFragment implements SpecialLevelsAdapter.IOnSpecialLevelClickListener {

    private SpecialLevelsAdapter mAdapter;
    private RecyclerView mSpecialLevelsRv;

    @Override
    ActionBarItem getActionBarItem() {
        return ActionBarItem.createSimpleActionBar(getString(R.string.special_levels));
    }

    @Override
    int getFragmentLayout() {
        return R.layout.fragment_special_levels;
    }

    @Override
    void initFragmentView(View view) {
        mSpecialLevelsRv = view.findViewById(R.id.rv_levels);
    }

    @Override
    void initFragmentContent() {
        List<SpecialLevel> specialLevelList = GeneralManager.getInstance().getDataForSpecialLevelsFragment(getRealm());
        if (mAdapter == null) {
            mAdapter = new SpecialLevelsAdapter(specialLevelList, this);
            int resId = R.anim.layout_animation_fall_down;
            LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(), resId);
            mSpecialLevelsRv.setLayoutAnimation(animation);
        } else {
            mAdapter.updateList(specialLevelList);
        }
        mSpecialLevelsRv.setAdapter(mAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mSpecialLevelsRv.setAdapter(null);
        mSpecialLevelsRv = null;
    }

    @Override
    public void onLevelClick(final SpecialLevel specialLevel) {
        SoundManager.getInstance().playSound(SOUND_CLICK);
        if (DataManager.getInstance().isPurchaseSpecials() || MyDatabase.getInstance().isSpecialLevelPurchased(getRealm(), specialLevel.getLevelId())) {
            AllQuotesFragmentExtraData allQuotesFragmentExtraData
                    = new AllQuotesFragmentExtraData(AllQuotesFragmentExtraData.SourceView.SPECIAL_LEVELS_FRAGMENT, specialLevel.getLevelId(), specialLevel.getLevelName());
            NavigationManager.getInstance().goToAllQuotesFragment(getBaseActivity(), allQuotesFragmentExtraData);
        } else {
            DialogUtils.showNeedToPurchaseSpecialLevelDialog(getBaseActivity(), new IOnNeedToPurchaseSpecialsDialog() {
                @Override
                public void onPurchaseAllSpecials() {
                    GeneralManager.getInstance().showPurchaseSpecialsDialog(getBaseActivity(), new PurchaseSpecialsDialog.SubscriptionDialogListener() {
                        @Override
                        public void onStartSubscriptionClicked() {
//                            if (BuildConfig.DEBUG) {
//                                goToAllQuotesFragment(specialLevel.getLevelId(), specialLevel.getLevelName());
//                            } else {
//                                PurchasesManager.getInstance().purchaseSpecials(getBaseActivity(), Consts.PurchaseSubscriptionFrom.COINS_POPUP);
//                            }
                            PurchasesManager.getInstance().purchaseSpecials(getBaseActivity(), Consts.PurchaseSubscriptionFrom.COINS_POPUP);
                        }
                    });
                }

                @Override
                public void onPurchaseSpecialLevel() {
                    if (BuildConfig.DEBUG) {
                        goToAllQuotesFragment(specialLevel.getLevelId(), specialLevel.getLevelName());
                    } else {
                        PurchasesManager.getInstance().purchaseSpecialLevel(getBaseActivity(), specialLevel.getLevelId(), specialLevel.getLevelName(), getRealm());
                    }
                }
            });
        }
    }

    @Override
    public void onSpecialLevelPurchased(int levelId, String levelName) {
        try {
            goToAllQuotesFragment(levelId, levelName);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @Override
    public void onSpecialsPurchased() {

    }

    private void goToAllQuotesFragment(int levelId, String levelName) {
        AllQuotesFragmentExtraData allQuotesFragmentExtraData
                = new AllQuotesFragmentExtraData(AllQuotesFragmentExtraData.SourceView.SPECIAL_LEVELS_FRAGMENT,
                levelId, levelName);
        NavigationManager.getInstance().goToAllQuotesFragment(getBaseActivity(), allQuotesFragmentExtraData);
    }

    public static SpecialLevelsFragment newInstance() {

        Bundle args = new Bundle();

        SpecialLevelsFragment fragment = new SpecialLevelsFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
