package com.oyeapps.quotest.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.oyeapps.quotest.activities.BaseActivity;
import com.oyeapps.quotest.data_obj.ActionBarItem;
import com.oyeapps.quotest.interfaces.IOnActionBarChangeListener;
import com.oyeapps.quotest.managers.ExceptionManager;
import com.oyeapps.quotest.managers.FirebaseManager;
import com.oyeapps.quotest.utils.Logger;
import com.oyeapps.quotest.utils.MyUtils;

import org.jetbrains.annotations.NotNull;

import io.realm.Realm;

/**
 * Created by yositsa on 27/12/2017.
 */

public abstract class BaseFragment extends Fragment {

    protected final String name = getClass().getName();
    protected final String simpleName = getClass().getSimpleName();

    static final String EXTRA_DATA = "extraData";

    private IOnActionBarChangeListener iOnActionBarChangeListener;

    abstract ActionBarItem getActionBarItem();

    abstract int getFragmentLayout();

    abstract void initFragmentView(View view);

    void initFragmentContent() {
    }

    protected void initBundleData(Bundle bundle) {
    }

    private ExceptionManager.viewErrorType getViewErrorType() {
        return ExceptionManager.viewErrorType.NAVIGATION;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        getExceptionManager().addFragmentToList(name, getViewErrorType());
        if (context instanceof IOnActionBarChangeListener) {
            iOnActionBarChangeListener = (IOnActionBarChangeListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        iOnActionBarChangeListener = null;
        getExceptionManager().removeFragmentFromList(name, getViewErrorType());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        try {
            view = inflater.inflate(getFragmentLayout(), container, false);

            Logger.d(Logger.TEST, simpleName, "onCreateView");
            FirebaseManager.getInstance().reportViewEntrance(getActivity(), simpleName);

            Bundle bundle = MyUtils.getNonNullBundle(getArguments());
            initBundleData(bundle);
            initFragmentView(view);
            initFragmentContent();

            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread t, Throwable e) {
                    Logger.e(Logger.TEST, e.getMessage(), e);
                    getExceptionManager().handleError(ExceptionManager.errorTypes.TERMINAL, name, getActivity());
                }
            });
        } catch (Exception e) {
            getExceptionManager().handleError(ExceptionManager.errorTypes.TERMINAL, name, getActivity());
            Logger.e(Logger.TEST, e.getMessage(), e);
        }

        return view;
    }

    public Realm getRealm() {
        if (getActivity() instanceof BaseActivity) {
            return ((BaseActivity) getActivity()).getRealm();
        }
        return Realm.getDefaultInstance();
    }

    ExceptionManager getExceptionManager() {
        if (getActivity() instanceof BaseActivity) {
            return ((BaseActivity) getActivity()).getExceptionManager();
        }
        return ExceptionManager.getInstance();
    }

    boolean isActivityRunning() {
        if (getActivity() instanceof BaseActivity) {
            return ((BaseActivity) getActivity()).ismIsActivityRunning();
        }
        return false;
    }

    protected void showLoader() {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showLoader();
        }
    }

    protected void dismissLoader() {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).dismissLoader();
        }
    }

    private void onActionBarStateChange() {
        if (iOnActionBarChangeListener != null) {
            iOnActionBarChangeListener.onActionBarStateChange(getActionBarItem());
        }
    }

    BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        onActionBarStateChange();
    }

    public boolean isFragmentHandleBackPress() {
        return false;
    }
}
