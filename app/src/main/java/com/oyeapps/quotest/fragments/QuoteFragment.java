package com.oyeapps.quotest.fragments;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.widget.TextViewCompat;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.activities.BaseActivity;
import com.oyeapps.quotest.data_obj.ActionBarItem;
import com.oyeapps.quotest.database.MyDatabase;
import com.oyeapps.quotest.database.Quote;
import com.oyeapps.quotest.extraData.ExtraDataItem;
import com.oyeapps.quotest.extraData.QuoteFragmentExtraData;
import com.oyeapps.quotest.interfaces.IInterstitialAdListener;
import com.oyeapps.quotest.interfaces.ILogoSolvedListener;
import com.oyeapps.quotest.managers.DataManager;
import com.oyeapps.quotest.managers.ExceptionManager;
import com.oyeapps.quotest.managers.GeneralManager;
import com.oyeapps.quotest.managers.NavigationManager;
import com.oyeapps.quotest.managers.SoundManager;
import com.oyeapps.quotest.utils.AnimUtils;
import com.oyeapps.quotest.utils.Logger;
import com.oyeapps.quotest.utils.MyUtils;
import com.oyeapps.quotest.views.RandomLettersLayout;
import com.oyeapps.quotest.views.SolutionLettersLayout;
import com.oyeapps.quotest.widgets.TypeWriter;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static com.oyeapps.quotest.utils.Consts.ONE_LETTER_DISCOVERY_COST;
import static com.oyeapps.quotest.utils.Consts.REWARD_SOLVING_LOGO;
import static com.oyeapps.quotest.utils.Consts.SOLVING_LOGO_COST;
import static com.oyeapps.quotest.utils.Consts.SOUND_CLICK;
import static com.oyeapps.quotest.utils.Consts.SOUND_CORRECT;
import static com.oyeapps.quotest.utils.Consts.SOUND_INCORRECT;
import static com.oyeapps.quotest.utils.Consts.STATUS_SOLVED;
import static com.oyeapps.quotest.utils.Consts.UNNECESSARY_LETTERS_REMOVAL_COST;
import static com.oyeapps.quotest.utils.MyUtils.getScreenWidth;

public class QuoteFragment extends BaseFragment implements View.OnClickListener, RandomLettersLayout.RandomLettersListener, SolutionLettersLayout.SolutionLettersListener {

    private int mQuoteId = -1;
    private int mQuoteNum = 0;
    private QuoteFragmentExtraData.SourceView mSourceView = QuoteFragmentExtraData.SourceView.LEVELS_FRAGMENT;
    private String mLevelName = "";

    private Quote mQuote;

    private TextView mHashTagTv;
    private CardView mSolveLogoCv, mSolveLetterCv, mRemoveLettersCv, mClearCv, mReturnCv, mNextQuoteCv, mFacebookCv, mWhatsappCv;
    private TypeWriter mQuoteTv;
    private RelativeLayout mBottomLayout;
    private LinearLayout mAddedCoinsLayout;
    private View mDisplayDarkLayout;
    private RandomLettersLayout mRandomLayout;
    private SolutionLettersLayout mSolutionLayout;

    private long startTime = 0;

    private ILogoSolvedListener mILogoSolvedListener;
    private IInterstitialAdListener iInterstitialAdListener;

    private Handler mHandler = new Handler();

    @Override
    ActionBarItem getActionBarItem() {
        return ActionBarItem.createSimpleActionBar(getString(R.string.quote, mQuoteNum));
    }

    @Override
    int getFragmentLayout() {
        return R.layout.fragment_quote;
    }

    @Override
    void initFragmentView(View view) {
        mQuoteTv = view.findViewById(R.id.tv_quote);
        mClearCv = view.findViewById(R.id.cv_clear_letters);
        mDisplayDarkLayout = view.findViewById(R.id.FragmentLogoDisplayDarkLayout);
        mAddedCoinsLayout = view.findViewById(R.id.ll_added_coins);
        mReturnCv = view.findViewById(R.id.cv_return);
        mNextQuoteCv = view.findViewById(R.id.cv_next_quote);
        mSolutionLayout = view.findViewById(R.id.FragmentLogoSolutionLayout);
        mBottomLayout = view.findViewById(R.id.FragmentLogoBottomLayout);
        mHashTagTv = view.findViewById(R.id.tv_hash_tag);
        mFacebookCv = view.findViewById(R.id.cv_facebook);
        mWhatsappCv = view.findViewById(R.id.cv_whatsapp);
        mSolveLogoCv = view.findViewById(R.id.cv_solve_logo);
        mRemoveLettersCv = view.findViewById(R.id.cv_remove_letters);
        mSolveLetterCv = view.findViewById(R.id.cv_solve_letter);
    }

    @Override
    void initFragmentContent() {
        mQuote = MyDatabase.getInstance().getQuoteById(getRealm(), mQuoteId);

        Typeface typeface;
        if (MyUtils.isRtlText(mQuote.getmQuote())) {
            mQuoteTv.setTextSize(40);
            mQuoteTv.setAutoSizeTextTypeUniformWithConfiguration(12, 40, 1, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            typeface = ResourcesCompat.getFont(Objects.requireNonNull(getContext()), R.font.bokertovblack);
        } else {
            mQuoteTv.setTextSize(30);
            mQuoteTv.setAutoSizeTextTypeUniformWithConfiguration(12, 30, 1, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            typeface = ResourcesCompat.getFont(Objects.requireNonNull(getContext()), R.font.comicsansms_bold);
        }
        mQuoteTv.setTypeface(typeface);

        String hashTag = mSourceView == QuoteFragmentExtraData.SourceView.LEVELS_FRAGMENT ? "#" + mQuote.getmCategory() : "#" + mLevelName;
        mHashTagTv.setText(hashTag);

        mClearCv.setVisibility(View.GONE);

        mQuoteTv.setCharacterDelay(40);
        mQuoteTv.animateText(mQuote.getmQuote());

        mFacebookCv.setOnClickListener(this);
        mWhatsappCv.setOnClickListener(this);
        mSolveLogoCv.setOnClickListener(this);
        if (!mQuote.isRemoveUnnecessaryLettersHint())
            mRemoveLettersCv.setOnClickListener(this);
        mSolveLetterCv.setOnClickListener(this);
        mReturnCv.setOnClickListener(this);
        mNextQuoteCv.setOnClickListener(this);
        mClearCv.setOnClickListener(this);

        if (mQuote.isRandomLetterSelected())
            mClearCv.setVisibility(View.VISIBLE);
        else mClearCv.setVisibility(View.GONE);

        mSolutionLayout.init(mQuote, this);

        setBottomLayoutHeight();

        if (mQuote.getmStatus() == STATUS_SOLVED) {
            setLogoSolvedLayout();
        } else {
            mRandomLayout = new RandomLettersLayout(getActivity(), this, mQuote);
            mBottomLayout.addView(mRandomLayout);
            mSolutionLayout.setRandomLayout(mRandomLayout);
        }

        if (iInterstitialAdListener != null)
            iInterstitialAdListener.showInterstitialAd();
    }

    @Override
    protected void initBundleData(Bundle bundle) {
        ExtraDataItem extraData = bundle.getParcelable(EXTRA_DATA);
        if (extraData instanceof QuoteFragmentExtraData) {
            QuoteFragmentExtraData quoteFragmentExtraData = (QuoteFragmentExtraData) extraData;
            mQuoteId = quoteFragmentExtraData.getQuoteId();
            mQuoteNum = quoteFragmentExtraData.getQuoteNum();
            mSourceView = quoteFragmentExtraData.getSourceView();
            mLevelName = quoteFragmentExtraData.getLevelName();
        }
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        if (context instanceof ILogoSolvedListener) {
            mILogoSolvedListener = (ILogoSolvedListener) context;
        }
        if (context instanceof IInterstitialAdListener) {
            iInterstitialAdListener = (IInterstitialAdListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mILogoSolvedListener = null;
        iInterstitialAdListener = null;
    }

    private void setBottomLayoutHeight() {
        try {
            int height = (int) ((RandomLettersLayout.getLetterTvSize() * 3.5));
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
            lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            mBottomLayout.setLayoutParams(lp);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void setLogoSolvedLayout() {
        try {
            mClearCv.setVisibility(View.GONE);
            mSolveLogoCv.setOnClickListener(null);
            mRemoveLettersCv.setOnClickListener(null);
            mSolveLetterCv.setOnClickListener(null);

            if (mRandomLayout != null)
                mRandomLayout.setVisibility(View.GONE);

            mDisplayDarkLayout.setVisibility(View.VISIBLE);

            mReturnCv.setVisibility(View.VISIBLE);
            mNextQuoteCv.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
            getExceptionManager().handleError(ExceptionManager.errorTypes.NAVIGATION, name, getActivity());
        }
    }

    private void onLogoSolved() {
        try {
//
//            Bundle params = new Bundle();
//            params.putString(FirebaseAnalytics.Param.ITEM_ID, String.valueOf(mLogo.getmId()));
//            params.putString(FirebaseAnalytics.Param.ITEM_NAME, mLogo.getmQuoteHolder());
//            params.putString(FirebaseAnalytics.Param.LEVEL, getString(R.string.stage) + " " + mLogo.getmLevel());
//            params.putLong(FirebaseAnalytics.Param.VALUE, time);
//            mFirebaseAnalytics.logEvent(TIME_TOOK_TO_SOLVE_LOGO_EVENT_NAME, params);

            long endTime = Calendar.getInstance().getTimeInMillis();
            long secs = TimeUnit.MILLISECONDS.toSeconds(endTime - startTime);
            mQuote.addTime(secs);

            SoundManager.getInstance().playSound(SOUND_CORRECT);
            mQuote.setmStatus(STATUS_SOLVED);
            DataManager.getInstance().increaseCoinsByAmount(REWARD_SOLVING_LOGO);

            setLogoSolvedLayout();
            mSolutionLayout.onLogoSolvedChangeLayout();

            AnimUtils.slideUpAnimation(mReturnCv);
            AnimUtils.slideUpAnimation(mNextQuoteCv);
            addCoinsAnimation();

            if (mILogoSolvedListener != null && mQuote.getmLevel() < 1000) {
                mILogoSolvedListener.onLogoSolved(mQuote.getmLevel());
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
            getExceptionManager().handleError(ExceptionManager.errorTypes.NAVIGATION, name, getActivity());
        }
    }

    private void updateCoinsLayout() {
        onResume();
    }

    private void clearSolution() {
        mClearCv.setVisibility(View.GONE);
        mSolutionLayout.clearSolution();
    }

    private void clearUnnecessaryLettersByHelp() {
        if (DataManager.getInstance().getCoinsAmount() >= UNNECESSARY_LETTERS_REMOVAL_COST) {
            clearSolution();
            mQuote.setRemoveUnnecessaryLettersHint(true);
            mRandomLayout.clearUnnecessaryLetters();
            DataManager.getInstance().decreaseCoinsByAmount(UNNECESSARY_LETTERS_REMOVAL_COST);
            mRemoveLettersCv.setOnClickListener(null);
            updateCoinsLayout();
        } else {
            showNotEnoughMoneyDialog();
        }
    }

    private void solveLogoByHelp() {
        if (DataManager.getInstance().getCoinsAmount() >= SOLVING_LOGO_COST) {
            mSolutionLayout.solveLogoByHelp();
            mQuote.setResolveQuoteHint(true);
            DataManager.getInstance().decreaseCoinsByAmount(SOLVING_LOGO_COST);
            updateCoinsLayout();
            onLogoSolved();
        } else {
            showNotEnoughMoneyDialog();
        }
    }

    private void solveLetterByHelp() {
        if (DataManager.getInstance().getCoinsAmount() >= ONE_LETTER_DISCOVERY_COST) {
            mSolutionLayout.solveLetterByHelp();
            mQuote.revealLetter();
            DataManager.getInstance().decreaseCoinsByAmount(ONE_LETTER_DISCOVERY_COST);
            updateCoinsLayout();
            if (mSolutionLayout.isLogoSolvedNow())
                onLogoSolved();
        } else {
            showNotEnoughMoneyDialog();
        }
    }

    @Override
    public void onClick(View v) {
        try {
            SoundManager.getInstance().playSound(SOUND_CLICK);
            switch (v.getId()) {
                case R.id.cv_clear_letters:
                    clearSolution();
                    break;
                case R.id.cv_facebook:
                    GeneralManager.getInstance().shareQuoteOnFacebook
                            (getBaseActivity(), mQuote.getmQuote(), mQuote.getmQuoteHolder(), mQuote.getmStatus() == STATUS_SOLVED);
                    break;
                case R.id.cv_whatsapp:
                    GeneralManager.getInstance().shareQuoteOnWhatsapp
                            (getBaseActivity(), mQuote.getmQuote(), mQuote.getmQuoteHolder(), mQuote.getmStatus() == STATUS_SOLVED);
                    break;
                case R.id.cv_solve_logo:
                    solveLogoByHelp();
                    break;
                case R.id.cv_remove_letters:
                    clearUnnecessaryLettersByHelp();
                    break;
                case R.id.cv_solve_letter:
                    solveLetterByHelp();
                    break;
                case R.id.cv_return:
                    getBaseActivity().getOnBackPressedDispatcher().onBackPressed();
                    break;
                case R.id.cv_next_quote:
                    onNextQuoteClick();
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void onNextQuoteClick() {
        List<Quote> quoteList = MyDatabase.getInstance().getAllQuotesByLevel(getRealm(), mQuote.getmLevel());
        int nextQuotePos;
        if (mQuoteNum >= quoteList.size()) {
            nextQuotePos = 0;
        } else {
            nextQuotePos = mQuoteNum;
        }
        getBaseActivity().getSupportFragmentManager().popBackStack();
        QuoteFragmentExtraData quoteFragmentExtraData = new QuoteFragmentExtraData(QuoteFragmentExtraData.SourceView.LEVELS_FRAGMENT,
                quoteList.get(nextQuotePos).getmId(), nextQuotePos + 1, mLevelName);
        NavigationManager.getInstance().goToQuoteFragment(getBaseActivity(), quoteFragmentExtraData);
    }

    private void showNotEnoughMoneyDialog() {
        try {
            NavigationManager.getInstance().goToCoinsActivity(getBaseActivity());
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void translateSolvingLogoRewardCoins(final View viewToMove, View target) {
        try {
            viewToMove.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            viewToMove.animate()
                    .x(target.getX())
                    .y(target.getY())
                    .alpha(0)
                    .setDuration(1500)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            try {
                                viewToMove.setLayerType(View.LAYER_TYPE_NONE, null);
                                mAddedCoinsLayout.setVisibility(View.GONE);
                                if (getContext() != null) {
                                    updateCoinsLayout();
                                }
                            } catch (Exception e) {
                                Logger.e(Logger.TEST, e.getMessage(), e);
                            }
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }
                    })
                    .start();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void addCoinsAnimation() {
        try {
            mAddedCoinsLayout.setY(mSolutionLayout.getY());
            mAddedCoinsLayout.setX((float) getScreenWidth() / 2);
            mAddedCoinsLayout.bringToFront();
            mAddedCoinsLayout.setVisibility(View.VISIBLE);
            if (getActivity() instanceof BaseActivity) {
                translateSolvingLogoRewardCoins(mAddedCoinsLayout, ((BaseActivity) getActivity()).getActionBarCoinsTv());
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            startTime = Calendar.getInstance().getTimeInMillis();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            mQuoteTv.releaseSound();
            if (mQuote.getmStatus() != STATUS_SOLVED) {
                long endTime = Calendar.getInstance().getTimeInMillis();
                long secs = TimeUnit.MILLISECONDS.toSeconds(endTime - startTime);
                mQuote.addTime(secs);
            }
            MyDatabase.getInstance().updateQuote(getRealm(), mQuote);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public static QuoteFragment newInstance(ExtraDataItem extraData) {

        Bundle args = new Bundle();
        args.putParcelable(EXTRA_DATA, extraData);

        QuoteFragment fragment = new QuoteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mHandler.removeCallbacksAndMessages(null);
            mReturnCv = null;
            mNextQuoteCv = null;
            mClearCv = null;
            mSolveLogoCv = null;
            mRemoveLettersCv = null;
            mSolveLetterCv = null;
            mWhatsappCv = null;
            mFacebookCv = null;
            mHashTagTv = null;
            mQuoteTv = null;
            mBottomLayout = null;
            mAddedCoinsLayout = null;
            mDisplayDarkLayout = null;
            mRandomLayout = null;
            mSolutionLayout = null;
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @Override
    public void onRandomLetterSelected(String letter) {
        mClearCv.setVisibility(View.VISIBLE);
        mSolutionLayout.onRandomLetterSelected(letter);
        if (mSolutionLayout.isLogoSolvedNow())
            onLogoSolved();
        else if (mQuote.isSolutionLayoutFull()) {
            onWrongAnswer();
        }
    }

    @Override
    public void onSolutionLetterClicked() {
        if (mRandomLayout.toShowClearIv())
            mClearCv.setVisibility(View.VISIBLE);
        else mClearCv.setVisibility(View.GONE);
    }

    private void onWrongAnswer() {
        SoundManager.getInstance().playSound(SOUND_INCORRECT);
        for (TextView textView : mSolutionLayout.getmArrayOfLettersTvs()) {
            AnimUtils.letterBlinkInRed(textView, 0);
        }
    }
}