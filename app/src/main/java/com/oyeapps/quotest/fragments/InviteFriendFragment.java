package com.oyeapps.quotest.fragments;


import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.cardview.widget.CardView;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.data_obj.ActionBarItem;
import com.oyeapps.quotest.managers.GeneralManager;
import com.oyeapps.quotest.managers.SoundManager;

import java.util.Objects;

import static com.oyeapps.quotest.utils.Consts.SOUND_CLICK;

public class InviteFriendFragment extends BaseFragment implements View.OnClickListener {

    private CardView mCloseCv, mShareCv;
    private ImageView mShareIv;

    @Override
    ActionBarItem getActionBarItem() {
        return null;
    }

    @Override
    int getFragmentLayout() {
        return R.layout.fragment_invite_friend;
    }

    @Override
    void initFragmentView(View view) {
        mCloseCv = view.findViewById(R.id.cv_close);
        mShareCv = view.findViewById(R.id.cv_share);
        mShareIv = view.findViewById(R.id.iv_share);
    }

    @Override
    void initFragmentContent() {
        mCloseCv.setOnClickListener(this);
        mShareCv.setOnClickListener(this);
        mShareIv.setOnClickListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mCloseCv = null;
        mShareCv = null;
        mShareIv = null;
    }

    public static InviteFriendFragment newInstance() {

        Bundle args = new Bundle();

        InviteFriendFragment fragment = new InviteFriendFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(View v) {
        SoundManager.getInstance().playSound(SOUND_CLICK);
        switch (v.getId()) {
            case R.id.cv_close:
                Objects.requireNonNull(getActivity()).onBackPressed();
                break;
            case R.id.iv_share:
            case R.id.cv_share:
                GeneralManager.getInstance().shareDynamicLink(getBaseActivity());
                break;
        }
    }

}
