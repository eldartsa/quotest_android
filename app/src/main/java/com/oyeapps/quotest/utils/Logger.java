package com.oyeapps.quotest.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.oyeapps.quotest.BuildConfig;

public class Logger {

    public static final String TEST = "AppDebugger";

    public static void i(String TAG, String className, String message) {
        if (BuildConfig.DEBUG) {
            StringBuilder spaces = new StringBuilder();
            for (int i = className.length(); i < 40; i++) {
                spaces.append(" ");
            }
            Log.i(TAG, className + spaces + "------->               " + message);
        }
    }

    public static void d(String TAG, String className, String message) {
        if (BuildConfig.DEBUG) {
            StringBuilder spaces = new StringBuilder();
            for (int i = className.length(); i < 40; i++) {
                spaces.append(" ");
            }
            Log.d(TAG, className + spaces + "------->               " + message);
        }
    }

    public static void e(String TAG, String message, Throwable e) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, message, e);
        }
        Crashlytics.log(message);
        Crashlytics.logException(e);
    }

    public static void w(String TAG, String className, String message) {
        if (BuildConfig.DEBUG) {
            StringBuilder spaces = new StringBuilder();
            for (int i = className.length(); i < 40; i++) {
                spaces.append(" ");
            }
            Log.w(TAG, className + spaces + "------->               " + message);
        }
    }

    public static void showToast(Context context, String message) {
        if (BuildConfig.DEBUG) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }
}
