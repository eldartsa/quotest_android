package com.oyeapps.quotest.utils;

import android.content.Context;

import com.oyeapps.quotest.app.MyApplication;

import static com.oyeapps.quotest.utils.FlavorConsts.BASE_IMAGES_URL;

public class ImageUtils {

    public static String getImageUrlForSpecialLevel(String specialNameImg) {

        return BASE_IMAGES_URL + "/drawable-" + getDensityName() + "/" + specialNameImg;
    }

    private static String getDensityName() {
        Context context = MyApplication.getInstance().getContext();
        float density = context.getResources().getDisplayMetrics().density;
        if (density >= 4.0) {
            return "xxxhdpi";
        }
        if (density >= 3.0) {
            return "xxhdpi";
        }
        if (density >= 2.0) {
            return "xhdpi";
        }
        return "hdpi";
    }
}
