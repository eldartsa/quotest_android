package com.oyeapps.quotest.utils;

import android.content.Context;

import com.oyeapps.quotest.BuildConfig;
import com.oyeapps.quotest.R;
import com.oyeapps.quotest.app.MyApplication;

public class TextUtils {

    public static String getMainHeaderTextWithColor() {
        Context context = MyApplication.getInstance().getContext();
        return "<font color=\"#749778\">\"</font>" + context.getString(R.string.main_header) + "<font color=\"#749778\">\"</font>";
    }

    public static String getTextInColor(String txt, int colorRes) {
        int intColor = ColorUtils.getColorFromResource(colorRes);
        return "<font color=\"" + ColorUtils.getColorAsHexString(intColor) + "\">" + txt + "</font>";
    }

    public static String getAppVersionsText() {
        return BuildConfig.VERSION_NAME + " (" + BuildConfig.VERSION_CODE + ")";
    }
}
