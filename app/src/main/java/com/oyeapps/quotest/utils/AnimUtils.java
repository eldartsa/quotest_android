package com.oyeapps.quotest.utils;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.oyeapps.quotest.R;

public class AnimUtils {

    public static void startAnimation(Context context, View view) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.main_start_anim);
        view.setAnimation(animation);
    }

    public static void removeAdsAnimation(Context context, View view) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.remove_ads_anim);
        view.setAnimation(animation);
    }

    public static void slideDownAnimation(final View view) {
        try {
            Animation animation = AnimationUtils.loadAnimation(view.getContext(),
                    R.anim.slide_down);
            view.startAnimation(animation);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public static void slideUpAnimation(final View view) {
        try {
            Animation animation = AnimationUtils.loadAnimation(view.getContext(),
                    R.anim.slide_up);
            view.startAnimation(animation);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public static void letterBlinkInRed(final TextView textView, final int i) {
        final int timeToBlink = 100;    //in milissegunds
        final int originalColor = textView.getCurrentTextColor();
        int redColor = Color.RED;
        textView.setTextColor(redColor);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                textView.setTextColor(originalColor);
                if (i != 3) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            letterBlinkInRed(textView, i + 1);
                        }
                    }, timeToBlink);
                }
            }
        }, timeToBlink);
    }
}
