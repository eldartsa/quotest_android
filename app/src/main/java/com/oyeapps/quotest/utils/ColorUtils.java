package com.oyeapps.quotest.utils;

import android.content.Context;

import androidx.core.content.res.ResourcesCompat;

import com.oyeapps.quotest.app.MyApplication;

public class ColorUtils {

    public static String getColorAsHexString(int intColor) {
        return String.format("#%06X", (0xFFFFFF & intColor));
    }

    public static int getColorFromResource(int res) {
        Context context = MyApplication.getInstance().getContext();
        return ResourcesCompat.getColor(context.getResources(), res, null);
    }
}
