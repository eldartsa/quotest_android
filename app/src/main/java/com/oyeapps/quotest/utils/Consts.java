package com.oyeapps.quotest.utils;

import android.content.Context;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.managers.SoundManager;

import java.util.HashMap;

import static com.oyeapps.quotest.utils.Consts.PurchaseRemoveAdsFrom.MAIN_VIEW;
import static com.oyeapps.quotest.utils.Consts.PurchaseSubscriptionFrom.CLICK_ON_LOCK;
import static com.oyeapps.quotest.utils.Consts.PurchaseSubscriptionFrom.COINS_POPUP;
import static com.oyeapps.quotest.utils.Consts.PurchaseSubscriptionFrom.OFFER;
import static com.oyeapps.quotest.utils.Consts.ShareLogoOn.FACEBOOK;
import static com.oyeapps.quotest.utils.Consts.ShareLogoOn.WHATSAPP;

/**
 * Created by eldarts on 18/05/2017.
 */

public class Consts {

    public static final String PURCHASE_SUBSCRIPTION_EVENT_NAME = "PurchaseSubscription";
    public static final String PURCHASE_SUBSCRIPTION_EVENT_PARAMETER_NAME = "PurchaseFrom";
    public static final String PURCHASE_REMOVE_ADS_EVENT_NAME = "PurchaseRemoveAds";
    public static final String PURCHASE_REMOVE_ADS_EVENT_PARAMETER_NAME = "PurchaseFrom";
    public static final String SHARE_LOGO_EVENT_NAME = "ShareLogo";
    public static final String SHARE_LOGO_EVENT_PARAMETER_NAME = "ShareLogoOn";
    public static final String OPEN_LEVEL_EVENT_NAME = "EnterLevel";
    public static final String OPEN_LEVEL_EVENT_PARAMETER_NAME = "LevelNum";
    public static final String TIME_TOOK_TO_SOLVE_LOGO_EVENT_NAME = "TimeTookToSolveLogo";
    public static final String FREE_COINS_EVENT_NAME = "FreeCoins";
    public static final String FREE_COINS_REWARD_VIDEO_PARAMETER_VALUE = "reward_video";
    public static final String FREE_COINS_FACEBOOK_PARAMETER_VALUE = "facebook";
    public static final String FREE_COINS_INSTAGRAM_PARAMETER_VALUE = "instagram";
    public static final String FREE_COINS_RANK_US_PARAMETER_VALUE = "rank_us";
    public static final String FREE_COINS_DOWNLOAD_LOGOTEST_PARAMETER_VALUE = "download_logotest";
    public static final String FREE_COINS_INVITE_FRIEND_PARAMETER_VALUE = "invite_friend";

    public enum ShareLogoOn {
        WHATSAPP,
        FACEBOOK
    }

    public final static HashMap<ShareLogoOn, String> SHARE_LOGO_EVENT_SHARE_LOGO_ON_PARAMETER_VALUES = new HashMap<ShareLogoOn, String>() {{
        put(WHATSAPP, "whatsapp");
        put(FACEBOOK, "facebook");
    }};

    public enum PurchaseSubscriptionFrom {
        CLICK_ON_LOCK,
        OFFER,
        COINS_POPUP
    }

    public final static HashMap<PurchaseSubscriptionFrom, String> PURCHASE_SUBSCRIPTION_EVENT_PURCHASE_FROM_PARAMETER_VALUES = new HashMap<PurchaseSubscriptionFrom, String>() {{
        put(CLICK_ON_LOCK, "clickOnLock");
        put(OFFER, "offer");
        put(COINS_POPUP, "coinsPopup");
    }};

    public enum PurchaseRemoveAdsFrom {
        MAIN_VIEW,
        COINS_POPUP
    }

    public final static HashMap<PurchaseRemoveAdsFrom, String> PURCHASE_REMOVE_ADS_EVENT_PURCHASE_FROM_PARAMETER_VALUES = new HashMap<PurchaseRemoveAdsFrom, String>() {{
        put(MAIN_VIEW, "mainView");
        put(Consts.PurchaseRemoveAdsFrom.COINS_POPUP, "coinsPopup");
    }};

    public static final String FLAVOR_ISRAEL = "israel";

    public static final String NOTIFICATION_REWARD_CODE = "1";
    public static final String NOTIFICATION_TO_SCREEN_CODE = "2";
    public static final String NOTIFICATION_URL_CODE = "3";
    public static final String TO_SCREEN_INVITE_FRIEND_CODE = "11";
    public static final String TO_SCREEN_LEVELS_CODE = "12";
    public static final String TO_SCREEN_GET_COINS_CODE = "13";

    public static final String LOGOTEST_APP_PACKAGE_NAME = "com.oyeapps.logotest";

    public static final char RANDOM_LETTER_HIDDEN = '#';
    public static final char REMOVED_RANDOM_LETTER_BY_HELP = '@';

    public static final char SOLUTION_LETTER_WITHOUT_VALUE = '#';
    public static final char SOLVED_SOLUTION_LETTER_BY_HELP = '@';
    public static final char SOLUTION_LETTER_REVEAL = '$';

    public static final String MY_PREFERENCES = "MyPrefs";

    public static final String GENERAL_DATA_KEY = "generalData";

    public static final int STATUS_CLEAN = 1;
    public static final int STATUS_SOLVED = 2;

    public static final int LANGUAGE_HEBREW = 1;
    public static final int LANGUAGE_FRENCH = 1;
    public static final int LANGUAGE_ENGLISH = 2;

    //Solution chars
    public static final char SPACE = ' ';
    public static final char AMPERSAND = '&';
    public static final char HYPHEN_MINUS = '-';
    public static final char COLON = ':';
    public static final char DOT = '.';
    public static final char APOSTROPHE = '\'';

    //Amounts
    public static final int UNNECESSARY_LETTERS_REMOVAL_COST = 1200;
    public static final int ONE_LETTER_DISCOVERY_COST = 400;
    public static final int SOLVING_LOGO_COST = 3000;
    public static final int REWARD_SOLVING_LOGO = 100;
    public static final int REWARD_VIDEO_AMOUNT = 600;
    public static final int REWARD_FACEBOOK_LIKE_AMOUNT = 5000;
    public static final int REWARD_INSTAGRAM_FOLLOW_AMOUNT = 5000;
    public static final int REWARD_DOWNLOAD_LOGOTEST_APP_AMOUNT = 5000;
    public static final int REWARD_INVITE_FRIEND_INVITER_AMOUNT = 20000;
    public static final int REWARD_INVITE_FRIEND_INVITEE_AMOUNT = 5000;

    public static final int OPEN_LEVEL_COMPLETION_PERCENT = 75;
    public static final int REVEAL_NORMAL_QUOTE_PERCENT = 30;
    public static final int REVEAL_SPECIAL_QUOTE_PERCENT = 20;
    public static final int CLEAR_UNNECESSARY_LETTERS_PERCENT = 100;

    public static final int SOUND_CLICK = R.raw.click_sound;
    public static final int SOUND_LOCK = R.raw.lock_sound;
    public static final int SOUND_CORRECT = R.raw.correct_sound;
    public static final int SOUND_REWARD = R.raw.reward_sound;
    public static final int SOUND_CHEERS = R.raw.cheers_sound;
    public static final int SOUND_SKIP = R.raw.skip_sound;
    public static final int SOUND_INCORRECT = R.raw.incorrect_sound;
    public static final int SOUND_TYPE = R.raw.type_sound;

    public static void initSoundManager(Context context, SoundManager soundManager) {
        soundManager.addSound(context, SOUND_CLICK);
        soundManager.addSound(context, SOUND_LOCK);
        soundManager.addSound(context, SOUND_CORRECT);
        soundManager.addSound(context, SOUND_REWARD);
        soundManager.addSound(context, SOUND_CHEERS);
        soundManager.addSound(context, SOUND_SKIP);
        soundManager.addSound(context, SOUND_INCORRECT);
    }
}
