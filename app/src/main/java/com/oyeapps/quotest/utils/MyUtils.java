package com.oyeapps.quotest.utils;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.google.firebase.iid.FirebaseInstanceId;
import com.oyeapps.quotest.BuildConfig;
import com.oyeapps.quotest.R;
import com.oyeapps.quotest.activities.BaseActivity;
import com.oyeapps.quotest.app.MyApplication;
import com.oyeapps.quotest.data_obj.GeneralData;
import com.oyeapps.quotest.database.MyDatabase;
import com.oyeapps.quotest.managers.UserAccountManager;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.realm.Realm;

import static com.oyeapps.quotest.utils.Consts.AMPERSAND;
import static com.oyeapps.quotest.utils.Consts.APOSTROPHE;
import static com.oyeapps.quotest.utils.Consts.COLON;
import static com.oyeapps.quotest.utils.Consts.DOT;
import static com.oyeapps.quotest.utils.Consts.HYPHEN_MINUS;
import static com.oyeapps.quotest.utils.Consts.SPACE;

/**
 * Created by eldarts on 25/05/2017.
 */

public class MyUtils {

    public static void slideUpFromScreen(final View view, Context context) {
        try {
            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.slide_up_from_screen);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    try {
                        view.setVisibility(View.GONE);
                    } catch (Exception e) {
                        Logger.e(Logger.TEST, e.getMessage(), e);
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            view.startAnimation(animation);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public static void SlideLeftToScreen(final View view, final Context context) {
        try {
            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.slide_left_to_screen);
            animation.setStartOffset(new Random().nextInt(300));
            view.startAnimation(animation);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public static void SlideLeftFromScreen(final View view, Context context) {
        Animation animation = AnimationUtils.loadAnimation(context,
                R.anim.slide_left_from_screen);
        view.startAnimation(animation);
    }

    public static void SlideRightToScreen(final View view, Context context) {
        try {
            Animation animation = AnimationUtils.loadAnimation(context,
                    R.anim.slide_right_to_screen);
            animation.setStartOffset(new Random().nextInt(300));
            view.startAnimation(animation);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public static void popLetter(final View view, Context context) {
        try {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.show_random_letter);
            animation.setFillEnabled(true);
            view.startAnimation(animation);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static int dpToPx(int dp) {
        Context context = MyApplication.getInstance().getContext();
        return (int) (dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static Bundle getNonNullBundle(Bundle bundle) {
        return bundle == null ? new Bundle() : bundle;
    }

    public static boolean isSeparator(char c) {
        try {
            return c == SPACE || c == AMPERSAND || c == HYPHEN_MINUS || c == COLON || c == DOT || c == APOSTROPHE;
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
            return false;
        }
    }

    public static String getGameTimeFormatted(Context context, long seconds) {
        long minutes = (seconds % 3600) / 60;
        long hours = seconds / 3600;
        if (hours == 0) {
            return minutes + " " + context.getString(R.string.minutes);
        } else {
            return hours + " " + context.getString(R.string.hours) + " " + context.getString(R.string.and) + minutes + " " + context.getString(R.string.minutes);
        }
    }

    @SuppressLint("HardwareIds")
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    /**
     * Compares two version strings.
     * <p>
     * Use this instead of String.compareTo() for a non-lexicographical
     * comparison that works for version strings. e.g. "1.10".compareTo("1.6").
     *
     * @param str1 a string of ordinal numbers separated by decimal points.
     * @param str2 a string of ordinal numbers separated by decimal points.
     * @return The result is a negative integer if str1 is _numerically_ less than str2.
     * The result is a positive integer if str1 is _numerically_ greater than str2.
     * The result is zero if the strings are _numerically_ equal.
     * @note It does not work if "1.10" is supposed to be equal to "1.10.0".
     */
    public static int versionCompare(String str1, String str2) {
        try {
            String[] vals1 = str1.split("\\.");
            String[] vals2 = str2.split("\\.");
            int i = 0;
            // set index to first non-equal ordinal or length of shortest version string
            while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
                i++;
            }
            // compare first non-equal ordinal number
            if (i < vals1.length && i < vals2.length) {
                int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
                return Integer.signum(diff);
            }
            // the strings are equal or one string is a substring of the other
            // e.g. "1.2.3" = "1.2.3" or "1.2.3" < "1.2.3.4"
            return Integer.signum(vals1.length - vals2.length);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
            return -1;
        }
    }

    public static boolean checkPromotion(Realm realm) {
        try {
            int totalSolvedLogos = MyDatabase.getInstance().getTotalSolvedQuotesCount(realm);
            Logger.i(Logger.TEST, "", "Total solved logos: " + totalSolvedLogos);

            GeneralData mGeneral = UserAccountManager.getInstance().getGeneral();
            boolean toShowPromotion = false;
            HashMap<String, Boolean> showPromotion = UserAccountManager.getInstance().getGeneral().getmShowPromotion();
            Iterator it = showPromotion.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                int logos = Integer.parseInt((String) pair.getKey());
                if (totalSolvedLogos >= logos && !(boolean) pair.getValue()) {
                    showPromotion.put((String) pair.getKey(), true);
                    toShowPromotion = true;
                }
            }
            mGeneral.setmShowPromotion(showPromotion);
            UserAccountManager.getInstance().updateGeneral(mGeneral);
            return toShowPromotion;
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
            return false;
        }
    }

    public static void showToast(final Context context, final String txt) {
        Toast.makeText(context, txt, Toast.LENGTH_SHORT).show();
    }

    public static boolean isDatabaseNeedToBeUpdate(String currentDbVersion, String dbVersion) {
        try {
            if (MyUtils.versionCompare(currentDbVersion, dbVersion) < 0) {
                return true;
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return false;
    }

    public static boolean isAppMustUpdate(String mMinAppVersion) {
        try {
            String currentVersion = BuildConfig.VERSION_NAME;
            if (MyUtils.versionCompare(currentVersion, mMinAppVersion) < 0) {
                return true;
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
        return false;
    }

    public static void copyFirebasePushNotificationTokenToClipboard(FragmentActivity activity) {
        String fireBaseToken = FirebaseInstanceId.getInstance().getToken();
        if (fireBaseToken != null && activity != null) {
            ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("copied text", fireBaseToken);
            if (clipboard != null) {
                clipboard.setPrimaryClip(clip);
                Toast.makeText(activity, activity.getResources().getString(R.string.token_copied_toast_message), Toast.LENGTH_LONG).show();
            }
        }
    }

    public static void copyAddFriendLinkToClipboard(BaseActivity activity, String code) {
        try {
            ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("Code Label", code);
            assert clipboard != null;
            clipboard.setPrimaryClip(clip);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public static boolean isAppSentToBackground(BaseActivity activity) {
        ActivityManager activityManager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        if (activityManager == null)
            return true;
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return true;
        }

        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND &&
                    appProcess.processName.equals(activity.getPackageName())) {
                return false;
            }
        }
        return true;
    }

    public static boolean isPackageInstalled(String packageName, PackageManager packageManager) {
        boolean found = true;
        try {
            packageManager.getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {

            found = false;
        }
        return found;
    }

    public static final Pattern RTL_CHARACTERS =
            Pattern.compile("[\u0600-\u06FF\u0750-\u077F\u0590-\u05FF\uFE70-\uFEFF]");

    public static boolean isRtlText(String txt) {
        Matcher matcher = RTL_CHARACTERS.matcher(txt);
        // it's RTL
        return matcher.find();
    }
}
