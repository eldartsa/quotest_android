package com.oyeapps.quotest.utils;

import android.app.Activity;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.activities.BaseActivity;
import com.oyeapps.quotest.database.MyDatabase;
import com.oyeapps.quotest.dialogs.ApplicationDialog;
import com.oyeapps.quotest.interfaces.IOnAppNeedInternetDialogListener;
import com.oyeapps.quotest.interfaces.IOnAppTerminationErrorOccurredDialogListener;
import com.oyeapps.quotest.interfaces.IOnInviteFriendSuggestionListener;
import com.oyeapps.quotest.interfaces.IOnNavigateOutErrorOccurredDialogListener;
import com.oyeapps.quotest.interfaces.IOnNeedToPurchaseSpecialsDialog;
import com.oyeapps.quotest.interfaces.IOnWatchAnotherVideoAdDialogListener;
import com.oyeapps.quotest.managers.NavigationManager;

import java.util.Locale;

import io.realm.Realm;

public class DialogUtils {

    public static void showAppMustUpdateDialog(final BaseActivity baseActivity, final String updateUrl) {
        try {
            ApplicationDialog appDialog = new ApplicationDialog(baseActivity,
                    "",
                    baseActivity.getString(R.string.app_must_update_dialog_message),
                    baseActivity.getString(R.string.app_must_update_dialog_neutral),
                    "");
            appDialog.setOnClickListener(new ApplicationDialog.ApplicationDialogListener() {
                @Override
                public void onPositiveClicked(ApplicationDialog dialog) {
                    dialog.dismiss();
                    NavigationManager.getInstance().openUrl(baseActivity, updateUrl);
                    NavigationManager.getInstance().closeApp(baseActivity);
                }

                @Override
                public void onNegativeClicked(ApplicationDialog dialog) {
                }
            });
            appDialog.setCancelable(false);
            appDialog.show();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public static void showAppHasNewUpdateAvailableDialog(final BaseActivity baseActivity, final String updateUrl) {
        try {
            ApplicationDialog dialog = new ApplicationDialog(baseActivity,
                    "",
                    baseActivity.getString(R.string.app_has_new_update_dialog_message),
                    baseActivity.getString(R.string.app_has_new_update_dialog_positive),
                    baseActivity.getString(R.string.app_has_new_update_dialog_negative));
            dialog.setOnClickListener(new ApplicationDialog.ApplicationDialogListener() {
                @Override
                public void onPositiveClicked(ApplicationDialog dialog) {
                    NavigationManager.getInstance().openUrl(baseActivity, updateUrl);
                    dialog.dismiss();
                }

                @Override
                public void onNegativeClicked(ApplicationDialog dialog) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public static void showResetGameDialog(Activity activity, final Realm realm) {
        try {
            ApplicationDialog appDialog = new ApplicationDialog(activity,
                    "",
                    activity.getString(R.string.reset_game_dialog_message),
                    activity.getString(R.string.reset_game_dialog_positive),
                    activity.getString(R.string.reset_game_dialog_negative));
            appDialog.setOnClickListener(new ApplicationDialog.ApplicationDialogListener() {
                @Override
                public void onPositiveClicked(ApplicationDialog dialog) {
                    MyDatabase.getInstance().resetGame(realm);
                    dialog.dismiss();
                }

                @Override
                public void onNegativeClicked(ApplicationDialog dialog) {
                    dialog.dismiss();
                }
            });
            appDialog.show();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public static void showNetworkErrorOccurredDialog(Activity activity) {
        showDialog(activity, activity.getString(R.string.network_failure_dialog_message));
    }

    public static void showAnotherRewardVideoAdDialog(Activity activity, final IOnWatchAnotherVideoAdDialogListener iOnWatchAnotherVideoAdDialogListener) {
        ApplicationDialog appDialog = new ApplicationDialog(activity,
                activity.getString(R.string.ask_for_new_video_ad_dialog_title),
                activity.getString(R.string.ask_for_new_video_ad_dialog_message),
                activity.getString(R.string.ask_for_new_video_ad_dialog_positive),
                activity.getString(R.string.ask_for_new_video_ad_dialog_negative));
        appDialog.setOnClickListener(new ApplicationDialog.ApplicationDialogListener() {
            @Override
            public void onPositiveClicked(ApplicationDialog dialog) {
                try {
                    dialog.dismiss();
                    iOnWatchAnotherVideoAdDialogListener.onWatchNowClick();
                } catch (Exception e) {
                    Logger.e(Logger.TEST, e.getMessage(), e);
                }
            }

            @Override
            public void onNegativeClicked(ApplicationDialog dialog) {
                dialog.dismiss();
            }
        });
        appDialog.show();
    }

    public static void showVideoAdFailedToLoadDialog(BaseActivity baseActivity) {
        ApplicationDialog appDialog = new ApplicationDialog(baseActivity,
                baseActivity.getString(R.string.fail_to_reload_video_ad_dialog_title),
                baseActivity.getString(R.string.fail_to_reload_video_ad_dialog_message),
                baseActivity.getString(R.string.fail_to_reload_video_ad_dialog_neutral), "");
        appDialog.show();
    }

    public static void showGameCompletedDialog(BaseActivity baseActivity) {
        ApplicationDialog appDialog = new ApplicationDialog(baseActivity,
                baseActivity.getString(R.string.game_finished_dialog_title),
                baseActivity.getString(R.string.game_finished_dialog_message),
                baseActivity.getString(R.string.game_finished_dialog_neutral),
                "");
        appDialog.show();
    }

    public static void showLevelCompletedDialog(BaseActivity baseActivity, int level) {
        String message = baseActivity.getString(R.string.level_finished_dialog_message, String.valueOf(level));
        ApplicationDialog appDialog = new ApplicationDialog(baseActivity,
                baseActivity.getString(R.string.level_finished_dialog_title),
                message,
                baseActivity.getString(R.string.level_finished_dialog_neutral),
                "");
        appDialog.show();
    }

    public static void showAppTerminationDialog(Activity activity, final IOnAppTerminationErrorOccurredDialogListener iOnAppTerminationErrorOccurredDialogListener) {
        ApplicationDialog dialog = new ApplicationDialog(activity,
                "",
                activity.getString(R.string.app_termination_dialog_message),
                activity.getString(R.string.app_termination_dialog_neutral),
                "");
        dialog.setCancelable(false);
        dialog.setOnClickListener(new ApplicationDialog.ApplicationDialogListener() {
            @Override
            public void onPositiveClicked(ApplicationDialog dialog) {
                dialog.dismiss();
                iOnAppTerminationErrorOccurredDialogListener.onTermination();
            }

            @Override
            public void onNegativeClicked(ApplicationDialog dialog) {
            }
        });
        dialog.show();
    }

    public static void showNavigateOutDialog(Activity activity, final IOnNavigateOutErrorOccurredDialogListener iOnNavigateOutErrorOccurredDialogListener) {
        ApplicationDialog dialog = new ApplicationDialog(activity,
                "",
                activity.getString(R.string.navigate_out_dialog_message),
                activity.getString(R.string.navigate_out_dialog_neutral),
                "");
        dialog.setCancelable(false);
        dialog.setOnClickListener(new ApplicationDialog.ApplicationDialogListener() {
            @Override
            public void onPositiveClicked(ApplicationDialog dialog) {
                dialog.dismiss();
                iOnNavigateOutErrorOccurredDialogListener.onNavigateOut();
            }

            @Override
            public void onNegativeClicked(ApplicationDialog dialog) {
            }
        });
        dialog.show();
    }

    private static void showDialog(Activity activity, String message) {
        try {
            ApplicationDialog appDialog = new ApplicationDialog(activity,
                    "",
                    message,
                    activity.getString(R.string.default_dialog_neutral),
                    "");
            appDialog.setOnClickListener(new ApplicationDialog.ApplicationDialogListener() {
                @Override
                public void onPositiveClicked(ApplicationDialog dialog) {
                    try {
                        dialog.dismiss();
                    } catch (Exception e) {
                        Logger.e(Logger.TEST, e.getMessage(), e);
                    }
                }

                @Override
                public void onNegativeClicked(ApplicationDialog dialog) {
                }
            });
            appDialog.show();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public static void showRewardedForReferringUsersDialog(BaseActivity baseActivity, int numOfUsersReferring) {
        String rewardAmountStr = String.format(Locale.US, "%,d", numOfUsersReferring * Consts.REWARD_INVITE_FRIEND_INVITER_AMOUNT);
        String message = baseActivity.getString(R.string.reward_for_referring_users_dialog_message,
                numOfUsersReferring, rewardAmountStr);
        showDialog(baseActivity, message);
    }

    public static void showAppNeedInternetAtFirstLaunch(BaseActivity baseActivity, final IOnAppNeedInternetDialogListener iOnAppNeedInternetDialogListener) {
        ApplicationDialog appDialog = new ApplicationDialog(baseActivity,
                "",
                baseActivity.getString(R.string.app_need_internet_connection_on_first_launch_dialog_message),
                baseActivity.getString(R.string.app_need_internet_connection_on_first_launch_dialog_neutral),
                "");
        appDialog.setOnClickListener(new ApplicationDialog.ApplicationDialogListener() {
            @Override
            public void onPositiveClicked(ApplicationDialog dialog) {
                dialog.dismiss();
                iOnAppNeedInternetDialogListener.onTryAgain();
            }

            @Override
            public void onNegativeClicked(ApplicationDialog dialog) {

            }
        });
        appDialog.setCancelable(false);
        appDialog.show();
    }

    public static void showNeedToPurchaseSpecialLevelDialog(BaseActivity baseActivity, final IOnNeedToPurchaseSpecialsDialog iOnNeedToPurchaseSpecialsDialog) {
        ApplicationDialog appDialog = new ApplicationDialog(baseActivity,
                baseActivity.getString(R.string.need_to_purchase_special_level_title),
                baseActivity.getString(R.string.need_to_purchase_special_level_message),
                baseActivity.getString(R.string.need_to_purchase_special_level_positive),
                baseActivity.getString(R.string.need_to_purchase_special_level_negative));
        appDialog.setOnClickListener(new ApplicationDialog.ApplicationDialogListener() {
            @Override
            public void onPositiveClicked(ApplicationDialog dialog) {
                dialog.dismiss();
                iOnNeedToPurchaseSpecialsDialog.onPurchaseSpecialLevel();
            }

            @Override
            public void onNegativeClicked(ApplicationDialog dialog) {
                dialog.dismiss();
                iOnNeedToPurchaseSpecialsDialog.onPurchaseAllSpecials();
            }
        });
        appDialog.show();
    }

    public static void showInviteFriendSuggestionDialog(BaseActivity baseActivity, final IOnInviteFriendSuggestionListener iOnInviteFriendSuggestionListener) {
        ApplicationDialog applicationDialog = new ApplicationDialog(baseActivity,
                baseActivity.getString(R.string.invite_friend_suggestion_dialog_title),
                baseActivity.getString(R.string.invite_friend_suggestion_dialog_message),
                baseActivity.getString(R.string.invite_friend_suggestion_dialog_positive),
                baseActivity.getString(R.string.invite_friend_suggestion_dialog_negative));
        applicationDialog.setOnClickListener(new ApplicationDialog.ApplicationDialogListener() {
            @Override
            public void onPositiveClicked(ApplicationDialog dialog) {
                dialog.dismiss();
                iOnInviteFriendSuggestionListener.onGoToInviteFriendClick();
            }

            @Override
            public void onNegativeClicked(ApplicationDialog dialog) {
                dialog.dismiss();
            }
        });
        applicationDialog.show();
    }
}
