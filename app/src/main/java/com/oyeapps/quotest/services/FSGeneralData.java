package com.oyeapps.quotest.services;

import com.google.firebase.firestore.PropertyName;

public class FSGeneralData {

    @PropertyName("db_version")
    public String dbVersion = "0";

    @PropertyName("specials_db_version")
    public String specialsDbVersion = "0";

    @PropertyName("last_app_version_android")
    public String lastAppVersion = "0";

    @PropertyName("min_app_version_android")
    public String minAppVersion = "0";

    @PropertyName("num_of_levels_to_display")
    public long numOfLevelsToDisplay = 0;

    @PropertyName("num_of_quotes_to_show_ad")
    public long numOfQuotesToShowAd = 5;

    @PropertyName("update_url_android")
    public String updateUrl = "";

    @PropertyName("instagram_page_username")
    public String instagramPageUsername = "";

    public FSGeneralData() {
    }

    public String getDbVersion() {
        return dbVersion;
    }

    public String getSpecialsDbVersion() {
        return specialsDbVersion;
    }

    public String getLastAppVersion() {
        return lastAppVersion;
    }

    public String getMinAppVersion() {
        return minAppVersion;
    }

    public long getNumOfLevelsToDisplay() {
        return numOfLevelsToDisplay;
    }

    public long getNumOfQuotesToShowAd() {
        return numOfQuotesToShowAd;
    }

    public String getUpdateUrl() {
        return updateUrl;
    }

    public String getInstagramPageUsername() {
        return instagramPageUsername;
    }
}
