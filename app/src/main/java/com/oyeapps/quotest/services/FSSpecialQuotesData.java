package com.oyeapps.quotest.services;

import com.google.firebase.firestore.PropertyName;

import java.util.List;

public class FSSpecialQuotesData {

    @PropertyName("levels")
    public List<FSSpecialLevel> specialLevelList;

    @PropertyName("quote_list")
    public List<FSQuote> quoteList;

    public FSSpecialQuotesData() {
    }

    public List<FSSpecialLevel> getSpecialLevelList() {
        return specialLevelList;
    }

    public List<FSQuote> getQuoteList() {
        return quoteList;
    }
}
