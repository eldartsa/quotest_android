package com.oyeapps.quotest.services;

import com.google.firebase.firestore.PropertyName;

import java.util.ArrayList;
import java.util.List;

public class UserInvite {

    @PropertyName("udid")
    private String udid = "";

    @PropertyName("os")
    private String os = "android";

    @PropertyName("referred_by")
    private String referred_by = "";

    @PropertyName("deep_link")
    private String deep_link = "";

    public UserInvite() {
    }

    public String getUdid() {
        return udid;
    }

    public void setUdid(String udid) {
        this.udid = udid;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getReferred_by() {
        return referred_by;
    }

    public void setReferred_by(String referred_by) {
        this.referred_by = referred_by;
    }

    public String getDeep_link() {
        return deep_link;
    }

    public void setDeep_link(String deep_link) {
        this.deep_link = deep_link;
    }
}
