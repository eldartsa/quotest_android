package com.oyeapps.quotest.services;

import com.google.firebase.firestore.PropertyName;

public class FSSpecialLevel {

    @PropertyName("level_id")
    public int id;

    @PropertyName("level_name")
    public String name;

    @PropertyName("level_name_img")
    public String levelNameImg = "";

    @PropertyName("reveal_percent")
    public int revealPercent = 0;

    public FSSpecialLevel() {
    }

    public int getid() {
        return id;
    }

    public String getname() {
        return name;
    }

    public String getLevelNameImg() {
        return levelNameImg;
    }

    public int getRevealPercent() {
        return revealPercent;
    }
}
