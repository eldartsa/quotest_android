package com.oyeapps.quotest.services;

import com.google.firebase.firestore.PropertyName;

import java.util.List;

public class FSQuotesData {

    @PropertyName("quotes_list")
    public List<FSQuote> quoteList;

    public FSQuotesData() {
    }

    public List<FSQuote> getQuoteList() {
        return quoteList;
    }
}
