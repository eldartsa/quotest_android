package com.oyeapps.quotest.services;

import com.google.firebase.firestore.PropertyName;

public class Invitee {

    @PropertyName("udid")
    private String udid;

    @PropertyName("is_rewarded")
    private boolean is_rewarded = false;

    public Invitee(String udid) {
        this.udid = udid;
    }

    public Invitee() {
    }

    public String getUdid() {
        return udid;
    }

    public void setUdid(String udid) {
        this.udid = udid;
    }

    public boolean isIs_rewarded() {
        return is_rewarded;
    }

    public void setIs_rewarded(boolean is_rewarded) {
        this.is_rewarded = is_rewarded;
    }
}
