package com.oyeapps.quotest.services;

import com.google.firebase.firestore.PropertyName;

public class FSQuote {

    @PropertyName("category")
    public String category = "";

    @PropertyName("id")
    public int id;

    @PropertyName("language")
    public int language = 1;

    @PropertyName("level")
    public int level;

    @PropertyName("quote")
    public String quote;

    @PropertyName("quoteholder")
    public String quoteholder = "";

    public FSQuote() {
    }

    public String getCategory() {
        return category;
    }

    public int getId() {
        return id;
    }

    public int getLanguage() {
        return language;
    }

    public int getLevel() {
        return level;
    }

    public String getQuote() {
        return quote;
    }

    public String getQuoteholder() {
        return quoteholder;
    }
}
