package com.oyeapps.quotest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.database.Quote;
import com.oyeapps.quotest.managers.ExceptionManager;
import com.oyeapps.quotest.managers.SoundManager;
import com.oyeapps.quotest.textviews.AutoResizeTextView;
import com.oyeapps.quotest.utils.Consts;
import com.oyeapps.quotest.utils.Logger;
import com.oyeapps.quotest.utils.MyUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.oyeapps.quotest.utils.Consts.CLEAR_UNNECESSARY_LETTERS_PERCENT;
import static com.oyeapps.quotest.utils.Consts.SOLUTION_LETTER_REVEAL;
import static com.oyeapps.quotest.utils.Consts.SOUND_CLICK;
import static com.oyeapps.quotest.utils.MyUtils.getScreenWidth;
import static com.oyeapps.quotest.utils.MyUtils.isSeparator;

public class RandomLettersLayout extends LinearLayout implements View.OnTouchListener {

    public interface RandomLettersListener {
        void onRandomLetterSelected(String letter);
    }

    public RandomLettersListener mListener;

    private static float MAX_TEXT_SIZE;

    private static final int NUM_OF_ROWS = 2;
    private static int LETTERS_IN_ROW;
    private static int LETTERS_COUNT;

    private static final int LETTERS_PADDING = 2;
    private static int LETTERS_MARGIN;
    public static int ROW_PADDING;

    private int mLetterTvSize, mLastPositionTouched = -1;

    private Quote mQuote;

    private List<AutoResizeTextView> mArrayOfLettersTvs;

    public RandomLettersLayout(Context context, RandomLettersListener listener, Quote quote) {
        super(context);
        this.mListener = listener;
        this.mQuote = quote;
        init();
    }

    public RandomLettersLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void init() {
        try {
            setOrientation(LinearLayout.VERTICAL);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
            setLayoutParams(layoutParams);
            setGravity(Gravity.CENTER);

            int wordLength = mQuote.getLettersCountWithoutRevealedLetters();
            if (wordLength > 14)
                LETTERS_IN_ROW = 8;
            else {
                LETTERS_IN_ROW = 7;
            }
            LETTERS_COUNT = NUM_OF_ROWS * LETTERS_IN_ROW;

            ROW_PADDING = (int) getResources().getDimension(R.dimen.random_row_padding);
            LETTERS_MARGIN = (int) getResources().getDimension(R.dimen.random_letters_margin);

            MAX_TEXT_SIZE = getResources().getDimension(R.dimen.random_and_solution_max_text_size);

            mLetterTvSize = RandomLettersLayout.getLetterTvSize();

            mArrayOfLettersTvs = new ArrayList<>();

            setOnTouchListener(this);

            setRandomLettersLayout();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
            ExceptionManager.getInstance().handleError(ExceptionManager.errorTypes.NAVIGATION, getName(), ((FragmentActivity) getContext()));
        }
    }

    private void setRandomLettersLayout() {
        try {
            LinearLayout.LayoutParams layoutParams;

            int pos = 0;
            for (int i = 0; i < NUM_OF_ROWS; i++) {
                layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                LinearLayout rowLayout = new LinearLayout(getContext());
                rowLayout.setLayoutParams(layoutParams);
                rowLayout.setOrientation(LinearLayout.HORIZONTAL);
                rowLayout.setPadding(0, ROW_PADDING, 0, 0);
                rowLayout.setGravity(Gravity.CENTER_HORIZONTAL);

                for (int j = 0; j < LETTERS_IN_ROW; j++) {
                    final int currentPos = pos;

                    layoutParams = new LinearLayout.LayoutParams(mLetterTvSize, mLetterTvSize);
                    layoutParams.setMargins(0, 0, LETTERS_MARGIN, 0);

                    AutoResizeTextView letterTv = new AutoResizeTextView(getContext());

                    letterTv.setLayoutParams(layoutParams);
                    letterTv.setTextColor(getResources().getColor(R.color.random_letter_text_color));
                    letterTv.setBackgroundResource(R.drawable.background_letter_random);
                    letterTv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, MAX_TEXT_SIZE);
                    letterTv.setPadding(LETTERS_PADDING, LETTERS_PADDING, LETTERS_PADDING, LETTERS_PADDING);
                    letterTv.setGravity(Gravity.CENTER);
                    if (mQuote.getmEditedRandomLetters().charAt(currentPos) == Consts.RANDOM_LETTER_HIDDEN) {
                        letterTv.setVisibility(View.INVISIBLE);
                        letterTv.setText(String.valueOf(mQuote.getmOriginalRandomLetters().charAt(currentPos)));
                    } else if (mQuote.getmEditedRandomLetters().charAt(currentPos) == Consts.REMOVED_RANDOM_LETTER_BY_HELP) {
                        letterTv.setVisibility(View.INVISIBLE);
                    } else {
                        letterTv.setText(String.valueOf(mQuote.getmEditedRandomLetters().charAt(currentPos)));
                    }

                    mArrayOfLettersTvs.add(letterTv);
                    rowLayout.addView(letterTv);

                    pos++;
                }
                addView(rowLayout);
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
            ExceptionManager.getInstance().handleError(ExceptionManager.errorTypes.NAVIGATION, getName(), ((FragmentActivity) getContext()));
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        try {
            float downX = event.getX();
            float downY = event.getY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_MOVE:
                    markRandomLetter(downX, downY);
                    break;
                case MotionEvent.ACTION_UP:
                    if (mLastPositionTouched != -1) {
                        mArrayOfLettersTvs.get(mLastPositionTouched).setBackgroundResource(R.drawable.background_letter_random);
                        if (!mQuote.isSolutionLayoutFull() && mArrayOfLettersTvs.get(mLastPositionTouched).getVisibility() == View.VISIBLE) {
                            onRandomLetterSelected(mLastPositionTouched);
                        }
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
            ExceptionManager.getInstance().handleError(ExceptionManager.errorTypes.TERMINAL, getName(), ((FragmentActivity) getContext()));
        }
        return true;
    }

    private void markRandomLetter(float downX, float downY) {
        int i = 0, currentPos = -1;
        for (TextView textView : mArrayOfLettersTvs) {
            int top = (int) (textView.getTop() + mLetterTvSize * 0.75), bottom = (int) (textView.getBottom() + mLetterTvSize * 0.75);
            //int top = textView.getTop() + mLetterTvSize / 4, bottom = textView.getBottom() + mLetterTvSize / 4;
            if (i >= LETTERS_IN_ROW) {
                top += textView.getBottom() + ROW_PADDING - textView.getTop();
                bottom += textView.getBottom() + ROW_PADDING - textView.getTop();
            }
            if (downX >= textView.getLeft() && downX <= textView.getRight() && downY >= top && downY <= bottom) {
                currentPos = i;
            }
            i++;
        }
        if (mLastPositionTouched != -1) {
            mArrayOfLettersTvs.get(mLastPositionTouched).setBackgroundResource(R.drawable.background_letter_random);
            if (currentPos != -1) {
                mArrayOfLettersTvs.get(currentPos).setBackgroundResource(R.drawable.background_letter_random_pressed);
                mLastPositionTouched = currentPos;
            } else {
                mLastPositionTouched = -1;
            }
        } else {
            if (currentPos != -1) {
                mArrayOfLettersTvs.get(currentPos).setBackgroundResource(R.drawable.background_letter_random_pressed);
                mLastPositionTouched = currentPos;
            }
        }
    }

    private void onRandomLetterSelected(int pos) {
        try {
            SoundManager.getInstance().playSound(SOUND_CLICK);
            String letter = mArrayOfLettersTvs.get(pos).getText().toString();
            mArrayOfLettersTvs.get(pos).setVisibility(View.INVISIBLE);
            mQuote.setmEditedRandomLetters(mQuote.getmEditedRandomLetters().substring(0, pos) + Consts.RANDOM_LETTER_HIDDEN + mQuote.getmEditedRandomLetters().substring(pos + 1));
            mListener.onRandomLetterSelected(letter);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
            ExceptionManager.getInstance().handleError(ExceptionManager.errorTypes.TERMINAL, getName(), ((FragmentActivity) getContext()));
        }
    }

    public void showLetter(int position) {
        MyUtils.popLetter(mArrayOfLettersTvs.get(position), getContext());
        mArrayOfLettersTvs.get(position).setVisibility(View.VISIBLE);
    }

    public void solveOneLetter(int letterAtRandom) {
        if (letterAtRandom == mQuote.getmEditedRandomLetters().length() - 1)
            mQuote.setmEditedRandomLetters(mQuote.getmEditedRandomLetters().substring(0, letterAtRandom) + Consts.REMOVED_RANDOM_LETTER_BY_HELP);
        else
            mQuote.setmEditedRandomLetters(mQuote.getmEditedRandomLetters().substring(0, letterAtRandom) + Consts.REMOVED_RANDOM_LETTER_BY_HELP + mQuote.getmEditedRandomLetters().substring(letterAtRandom + 1));
        mArrayOfLettersTvs.get(letterAtRandom).setVisibility(View.INVISIBLE);
    }

    public boolean toShowClearIv() {
        return mQuote.getmEditedRandomLetters().contains(String.valueOf(Consts.RANDOM_LETTER_HIDDEN));
    }

    public void clearUnnecessaryLetters() {
        try {
            ArrayList<Integer> positionsOfLogoName = new ArrayList<>();

            for (int i = 0; i < mQuote.getmQuoteHolder().length(); i++) {
                if (mQuote.getmLastTry().charAt(i) != Consts.REMOVED_RANDOM_LETTER_BY_HELP) {
                    char letter = mQuote.getmQuoteHolder().charAt(i);
                    if (!isSeparator(letter) && mQuote.getmLastTry().charAt(i) != SOLUTION_LETTER_REVEAL) {
                        String tempRandomLetters = mQuote.getmEditedRandomLetters();
                        boolean exists = true;
                        int pos = 0;
                        while (exists) {
                            exists = false;
                            pos = tempRandomLetters.indexOf(String.valueOf(letter));
                            for (int solutionLetterPos : positionsOfLogoName) {
                                if (pos == solutionLetterPos)
                                    exists = true;
                            }
                            if (pos == tempRandomLetters.length() - 1)
                                tempRandomLetters = tempRandomLetters.substring(0, pos) + "$";
                            else
                                tempRandomLetters = tempRandomLetters.substring(0, pos) + "$" + tempRandomLetters.substring(pos + 1);
                        }
                        positionsOfLogoName.add(pos);
                    } else {
                        positionsOfLogoName.add(-1);
                    }
                }
            }

            Random random = new Random();
            int removeLettersCount;
            removeLettersCount = (LETTERS_COUNT - mQuote.getLettersCountWithoutRevealedLetters()) * CLEAR_UNNECESSARY_LETTERS_PERCENT / 100;
            int count = 0;
            while (count < removeLettersCount) {
                int rnd = random.nextInt(LETTERS_COUNT);
                boolean isSolutionLetter = false;
                for (int j = 0; j < positionsOfLogoName.size(); j++) {
                    if (positionsOfLogoName.get(j) == rnd)
                        isSolutionLetter = true;
                }

                if (!isSolutionLetter && mQuote.getmEditedRandomLetters().charAt(rnd) != '@') {
                    count++;
                    if (rnd == mQuote.getmEditedRandomLetters().length() - 1)
                        mQuote.setmEditedRandomLetters(mQuote.getmEditedRandomLetters().substring(0, rnd) + Consts.REMOVED_RANDOM_LETTER_BY_HELP);
                    else
                        mQuote.setmEditedRandomLetters(mQuote.getmEditedRandomLetters().substring(0, rnd) + Consts.REMOVED_RANDOM_LETTER_BY_HELP + mQuote.getmEditedRandomLetters().substring(rnd + 1));
                    mArrayOfLettersTvs.get(rnd).setVisibility(View.INVISIBLE);
                }
            }
            mQuote.setRemoveUnnecessaryLettersHint(true);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
            ExceptionManager.getInstance().handleError(ExceptionManager.errorTypes.NAVIGATION, getName(), ((FragmentActivity) getContext()));
        }
    }

    public static int getLetterTvSize() {
        return getScreenWidth() / 9;
    }

    public void animateAllLettersBackToScreen() {
        try {
            for (int i = 0; i < mArrayOfLettersTvs.size(); i++) {
                if (mArrayOfLettersTvs.get(i).getVisibility() == View.INVISIBLE && mQuote.getmEditedRandomLetters().charAt(i) != Consts.REMOVED_RANDOM_LETTER_BY_HELP) {
                    MyUtils.popLetter(mArrayOfLettersTvs.get(i), getContext());
                    mArrayOfLettersTvs.get(i).setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private String getName() {
        return getClass().getName();
    }
}