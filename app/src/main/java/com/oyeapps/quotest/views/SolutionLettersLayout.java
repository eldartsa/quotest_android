package com.oyeapps.quotest.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentActivity;

import com.oyeapps.quotest.BuildConfig;
import com.oyeapps.quotest.R;
import com.oyeapps.quotest.database.Quote;
import com.oyeapps.quotest.managers.ExceptionManager;
import com.oyeapps.quotest.managers.SoundManager;
import com.oyeapps.quotest.textviews.AutoResizeTextView;
import com.oyeapps.quotest.utils.Consts;
import com.oyeapps.quotest.utils.Logger;

import java.util.ArrayList;
import java.util.Random;

import static com.oyeapps.quotest.utils.Consts.APOSTROPHE;
import static com.oyeapps.quotest.utils.Consts.SOLUTION_LETTER_REVEAL;
import static com.oyeapps.quotest.utils.Consts.SOLUTION_LETTER_WITHOUT_VALUE;
import static com.oyeapps.quotest.utils.Consts.SOLVED_SOLUTION_LETTER_BY_HELP;
import static com.oyeapps.quotest.utils.Consts.SOUND_CLICK;
import static com.oyeapps.quotest.utils.Consts.SPACE;
import static com.oyeapps.quotest.utils.Consts.STATUS_SOLVED;
import static com.oyeapps.quotest.utils.MyUtils.getScreenWidth;
import static com.oyeapps.quotest.utils.MyUtils.isSeparator;

public class SolutionLettersLayout extends LinearLayout implements View.OnTouchListener {

    public interface SolutionLettersListener {
        void onSolutionLetterClicked();
    }

    public SolutionLettersListener mListener;

    private static int REMOVED_SOLUTION_LETTER_TEXT_COLOR;
    private static int SEPARATOR_TEXT_COLOR;
    private static int LETTER_TEXT_COLOR;
    private static int LETTERS_MARGIN;
    public static int ROWS_MARGIN;
    private static float MAX_TEXT_SIZE;

    private int rowSpacePos = -1;
    private int lastPosition = -1;

    private Quote mQuote;

    private RandomLettersLayout mRandomLayout;

    private static final int DEFAULT_LETTER_SIZE = getScreenWidth() / 13;

    private String mLogoName;
    private int mScreenWidth = getScreenWidth();
    private int mHeight;
    private ArrayList<AutoResizeTextView> mArrayOfLettersTvs;

    private int mLetterSize;

    public void setRandomLayout(RandomLettersLayout randomLayout) {
        this.mRandomLayout = randomLayout;
    }

    public SolutionLettersLayout(Context context) {
        super(context);
    }

    public SolutionLettersLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ArrayList<AutoResizeTextView> getmArrayOfLettersTvs() {
        return mArrayOfLettersTvs;
    }

    public void init(Quote quote, SolutionLettersListener listener) {
        this.mQuote = quote;
        this.mListener = listener;

        removeAllViews();
        mArrayOfLettersTvs = new ArrayList<>();

        LETTERS_MARGIN = (int) getResources().getDimension(R.dimen.solution_letters_margin);
        ROWS_MARGIN = LETTERS_MARGIN * 2;

        LETTER_TEXT_COLOR = getResources().getColor(R.color.solution_letter_text_color);
        REMOVED_SOLUTION_LETTER_TEXT_COLOR = getResources().getColor(R.color.solution_removed_letter_text_color);
        MAX_TEXT_SIZE = getResources().getDimension(R.dimen.random_and_solution_max_text_size);
        SEPARATOR_TEXT_COLOR = getResources().getColor(R.color.solution_separator_text_color);

        mLogoName = mQuote.getmQuoteHolder();

        setOnTouchListener(this);

        setLayoutProperties();
        buildSolutionLettersLayout();
    }

    private void setLayoutProperties() {
        try {
            int height = (DEFAULT_LETTER_SIZE * 3);
            mHeight = height;
            getLayoutParams().height = height;
            setGravity(Gravity.CENTER);
            setOrientation(VERTICAL);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage() + ", Quote: " + mLogoName, e);
        }
    }

    private void buildSolutionLettersLayout() {
        try {
            int maximumCharsInRow, letterTvSize;
            if (mLogoName.length() >= 14) {
                maximumCharsInRow = 13;
            } else {
                maximumCharsInRow = 11;
            }

            letterTvSize = DEFAULT_LETTER_SIZE;

            LinearLayout firstRow = new LinearLayout(getContext());
            LinearLayout secondRow = new LinearLayout(getContext());

            firstRow.setOrientation(LinearLayout.HORIZONTAL);
            secondRow.setOrientation(LinearLayout.HORIZONTAL);

            if (BuildConfig.FLAVOR.equals(Consts.FLAVOR_ISRAEL)) {
                if (mQuote.getmLanguage() == Consts.LANGUAGE_ENGLISH) {
                    setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                } else {
                    setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                }
            } else {
                setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }

            boolean isFirstRow = true;
            boolean isSeparator;
            boolean isAfterSpace = false;
            int firstRowLength = 0;
            rowSpacePos = -1;
            lastPosition = -1;

            for (int i = 0; i < mLogoName.length(); i++) {
                LinearLayout.LayoutParams layoutParamsLetterTv = new LinearLayout.LayoutParams(letterTvSize, letterTvSize);
                layoutParamsLetterTv.setMargins(LETTERS_MARGIN, 0, LETTERS_MARGIN, 0);

                AutoResizeTextView solutionLetterTv = new AutoResizeTextView(getContext());

                solutionLetterTv.setLayoutParams(layoutParamsLetterTv);
                solutionLetterTv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, MAX_TEXT_SIZE);
                solutionLetterTv.setEms(1);
                solutionLetterTv.setGravity(Gravity.CENTER);
                solutionLetterTv.setTextColor(LETTER_TEXT_COLOR);
                solutionLetterTv.setTag(false);
                solutionLetterTv.setVisibility(View.VISIBLE);

                isSeparator = false;
                if (mQuote.getmLastTry().charAt(i) == Consts.SOLVED_SOLUTION_LETTER_BY_HELP) {
//                    solutionLetterTv.setTextColor(REMOVED_SOLUTION_LETTER_TEXT_COLOR);
                    solutionLetterTv.setText(String.valueOf(mLogoName.charAt(i)));
                    solutionLetterTv.setBackgroundResource(R.drawable.background_letter_solution_reveal);
                } else if (mQuote.getmLastTry().charAt(i) == Consts.SOLUTION_LETTER_WITHOUT_VALUE) {
                    solutionLetterTv.setText("");
                    solutionLetterTv.setBackgroundResource(R.drawable.background_letter_solution);
                } else if (isSeparator(mQuote.getmLastTry().charAt(i))) {
                    solutionLetterTv.bringToFront();
                    solutionLetterTv.setTag(true);
                    solutionLetterTv.setText(String.valueOf(mQuote.getmLastTry().charAt(i)));
                    solutionLetterTv.setTextColor(SEPARATOR_TEXT_COLOR);
                    if (mQuote.getmLastTry().charAt(i) == APOSTROPHE) {
                        LayoutParams layoutParams = (LayoutParams) solutionLetterTv.getLayoutParams();
                        layoutParams.width = (letterTvSize / 2);
                    } else {
                        isSeparator = true;
                        if (mQuote.getmLastTry().charAt(i) == SPACE)
                            solutionLetterTv.setVisibility(View.INVISIBLE);
                    }
                } else if (mQuote.getmLastTry().charAt(i) == SOLUTION_LETTER_REVEAL) {
                    solutionLetterTv.setText(String.valueOf(mQuote.getmQuoteHolder().charAt(i)));
                    solutionLetterTv.setBackgroundResource(R.drawable.background_letter_solution_reveal);
                    solutionLetterTv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.dark_gray, null));
                } else {
                    solutionLetterTv.setText(String.valueOf(mQuote.getmLastTry().charAt(i)));
                    solutionLetterTv.setBackgroundResource(R.drawable.background_letter_solution);
                }

                if (mQuote.getmStatus() == STATUS_SOLVED && !isSeparator(mQuote.getmLastTry().charAt(i))) {
                    solutionLetterTv.setTextColor(LETTER_TEXT_COLOR);
                    solutionLetterTv.setText(String.valueOf(mLogoName.charAt(i)));
                }

                if (isFirstRow && isAfterSpace) {
                    int l = i;
                    String tempLogoName = mLogoName;
                    while (l < tempLogoName.length() && (!isSeparator(tempLogoName.charAt(l)) || tempLogoName.charAt(l) == APOSTROPHE))
                        l++;
                    int nextWordLength = l - i + 1;
                    if (nextWordLength + firstRowLength > maximumCharsInRow) {
                        isFirstRow = false;
                        if (mArrayOfLettersTvs.get(i - 1).getText().charAt(0) == SPACE)
                            mArrayOfLettersTvs.get(i - 1).setVisibility(GONE);
                        rowSpacePos = i - 1;
                    }
                    isAfterSpace = false;
                }

                if (isSeparator)
                    isAfterSpace = true;

                if (isFirstRow) { //while first row
                    firstRowLength++;
                    firstRow.addView(solutionLetterTv);
                } else { //while second row
                    secondRow.addView(solutionLetterTv);
                }
                mArrayOfLettersTvs.add(solutionLetterTv);
            }

            if (secondRow.getChildCount() == 0) { //There is only 1 row
                firstRow.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                firstRow.setGravity(Gravity.CENTER);

                addView(firstRow);

                if (mLogoName.length() > 11) {
                    for (TextView textView : mArrayOfLettersTvs) {
                        letterTvSize = mScreenWidth / (mLogoName.length() + 2);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(letterTvSize, letterTvSize);
                        lp.setMargins(LETTERS_MARGIN, 0, LETTERS_MARGIN, 0);
                        textView.setLayoutParams(lp);
                    }
                }
            } else { // There are 2 rows
                LinearLayout.LayoutParams rowsParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                firstRow.setGravity(Gravity.CENTER);
                secondRow.setGravity(Gravity.CENTER);

                firstRow.setLayoutParams(rowsParams);
                secondRow.setLayoutParams(rowsParams);

                boolean changeSize = false;
                int firstRowSize = firstRow.getChildCount(), secondRowSize = secondRow.getChildCount();
                if (firstRowSize > 11 || secondRowSize > 11) {
                    changeSize = true;
                    if (firstRowLength > secondRowSize) {
                        letterTvSize = mScreenWidth / (firstRowLength + 2);
                    } else {
                        letterTvSize = mScreenWidth / (secondRowSize + 2);
                    }
                }

                if (changeSize) {
                    for (int i = 0; i < firstRow.getChildCount(); i++) {
                        AutoResizeTextView textView = (AutoResizeTextView) firstRow.getChildAt(i);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(letterTvSize, letterTvSize);
                        lp.setMargins(LETTERS_MARGIN, 0, LETTERS_MARGIN, 0);
                        textView.setLayoutParams(lp);
                    }

                    for (int i = 0; i < secondRow.getChildCount(); i++) {
                        AutoResizeTextView textView = (AutoResizeTextView) secondRow.getChildAt(i);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(letterTvSize, letterTvSize);
                        lp.setMargins(LETTERS_MARGIN, 0, LETTERS_MARGIN, 0);
                        textView.setLayoutParams(lp);
                    }
                }


                View rowsSpace = new View(getContext());
                LayoutParams rowsSpaceParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ROWS_MARGIN);
                rowsSpace.setLayoutParams(rowsSpaceParams);

                addView(firstRow);
                addView(rowsSpace);
                addView(secondRow);

                mLetterSize = letterTvSize;
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage() + ", Quote: " + mLogoName, e);
            ExceptionManager.getInstance().handleError(ExceptionManager.errorTypes.NAVIGATION, getName(), ((FragmentActivity) getContext()));
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (mQuote.getmStatus() == STATUS_SOLVED)
            return true;
        int x = (int) event.getX();
        int y = (int) event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                markLetter(x, y);
                break;
            case MotionEvent.ACTION_MOVE:
                markLetter(x, y);
                break;
            case MotionEvent.ACTION_UP:
                if (lastPosition != -1) {
                    mArrayOfLettersTvs.get(lastPosition).setBackgroundResource(R.drawable.background_letter_solution);
                    if (!mArrayOfLettersTvs.get(lastPosition).getText().toString().isEmpty()) {
                        SoundManager.getInstance().playSound(SOUND_CLICK);
                        onSolutionLetterClicked(lastPosition, false);
                    }
                }
                break;
            default:
                break;
        }

        return true;
    }

    private void markLetter(int eventX, int eventY) {
        int i = 0;
        int last = -1;
        for (AutoResizeTextView textView : mArrayOfLettersTvs) {
            if (!(boolean) textView.getTag()) {
                int y = eventY, x = eventX;
                if (rowSpacePos != -1)
                    y -= ((mLetterSize / 2) + (DEFAULT_LETTER_SIZE - mLetterSize) - ROWS_MARGIN);
                if (rowSpacePos != -1 && i > rowSpacePos)
                    y -= getChildAt(0).getHeight() + ROWS_MARGIN;
                if (!isPointWithin(x, y, textView.getLeft(), textView.getRight(), textView.getTop(),
                        textView.getBottom())) {
                    if (!isRemovedLetter(mQuote.getmLastTry().charAt(i))) {
                        textView.setBackgroundResource(R.drawable.background_letter_solution);
                    }
                }

                if (isPointWithin(x, y, textView.getLeft(), textView.getRight(), textView.getTop(),
                        textView.getBottom())) {
                    if (!isRemovedLetter(mQuote.getmLastTry().charAt(i))) {
                        textView.setBackgroundResource(R.drawable.background_letter_solution_pressed);
                        last = i;
                    }
                }
            }
            i++;
        }

        lastPosition = last;
    }

    static boolean isPointWithin(int x, int y, int x1, int x2, int y1, int y2) {
        return (x <= x2 && x >= x1 && y <= y2 && y >= y1);
    }

    private void onSolutionLetterClicked(int pos, boolean hide) {
        clearLetterFromSolution(pos, hide, true);
        try {
            mListener.onSolutionLetterClicked();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage() + ", Quote: " + mLogoName, e);
            ExceptionManager.getInstance().handleError(ExceptionManager.errorTypes.NAVIGATION, getName(), ((FragmentActivity) getContext()));
        }
    }

    private void clearLetterFromSolution(int mLastTryPos, boolean hide, boolean animate) {
        try {
            int posOfLetterToAnimate = -1;
            String letter = String.valueOf(mQuote.getmLastTry().charAt(mLastTryPos));
            boolean found = false;
            String tempEditedRandomLetters = mQuote.getmEditedRandomLetters();
            while (!found) {
                int position = tempEditedRandomLetters.indexOf("#");
                String letterAtOriginal = "";
                if (position == -1)
                    break;
                else
                    letterAtOriginal = String.valueOf(mQuote.getmOriginalRandomLetters().charAt(position));
                if (letterAtOriginal.equals(letter)) {
                    mArrayOfLettersTvs.get(mLastTryPos).setText("");
                    mQuote.setmLastTry(mQuote.getmLastTry().substring(0, mLastTryPos) + "#" + mQuote.getmLastTry().substring(mLastTryPos + 1));
                    if (!hide) {
                        posOfLetterToAnimate = position;
                    }
                    mQuote.setmEditedRandomLetters(mQuote.getmEditedRandomLetters().substring(0, position) + letter + mQuote.getmEditedRandomLetters().substring(position + 1));
                    found = true;
                } else {
                    tempEditedRandomLetters = tempEditedRandomLetters.substring(0, position) + "?" + tempEditedRandomLetters.substring(position + 1);
                }
            }
            if (posOfLetterToAnimate != -1 && animate)
                mRandomLayout.showLetter(posOfLetterToAnimate);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage() + ", Quote: " + mLogoName, e);
            ExceptionManager.getInstance().handleError(ExceptionManager.errorTypes.NAVIGATION, getName(), ((FragmentActivity) getContext()));
        }
    }

    public void clearSolution() {
        try {
            for (int i = 0; i < mQuote.getmLastTry().length(); i++) {
                if (mQuote.getmLastTry().charAt(i) != '#') {
                    clearLetterFromSolution(i, false, false);
                }
            }
            mRandomLayout.animateAllLettersBackToScreen();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage() + ", Quote: " + mLogoName, e);
            ExceptionManager.getInstance().handleError(ExceptionManager.errorTypes.NAVIGATION, getName(), ((FragmentActivity) getContext()));
        }
    }

    public void solveLetterByHelp() {
        try {
            Random random = new Random();

            int rndPos;
            char solutionLetter;
            rndPos = random.nextInt(mLogoName.length());
            solutionLetter = mLogoName.charAt(rndPos);
            while (isSeparator(solutionLetter)
                    || isRemovedLetter(mQuote.getmLastTry().charAt(rndPos))
                    || mQuote.getmLastTry().charAt(rndPos) == mLogoName.charAt(rndPos)) {
                rndPos = random.nextInt(mLogoName.length());
                solutionLetter = mLogoName.charAt(rndPos);
            }

            if (!mArrayOfLettersTvs.get(rndPos).getText().toString().equals("")) {
                onSolutionLetterClicked(rndPos, false);
            }

            if (rndPos == mQuote.getmLastTry().length() - 1)
                mQuote.setmLastTry(mQuote.getmLastTry().substring(0, rndPos) + SOLVED_SOLUTION_LETTER_BY_HELP);
            else
                mQuote.setmLastTry(mQuote.getmLastTry().substring(0, rndPos) + SOLVED_SOLUTION_LETTER_BY_HELP + mQuote.getmLastTry().substring(rndPos + 1));

            mArrayOfLettersTvs.get(rndPos).setText(String.valueOf(solutionLetter));
            mArrayOfLettersTvs.get(rndPos).setBackgroundResource(R.drawable.background_letter_solution_reveal);
//            mArrayOfLettersTvs.get(rndPos).setTextColor(REMOVED_SOLUTION_LETTER_TEXT_COLOR);
            int letterAtRandom = mQuote.getmEditedRandomLetters().indexOf(solutionLetter);
            if (letterAtRandom == -1) {
                int position = mQuote.getmLastTry().indexOf(solutionLetter);
                while (mLogoName.charAt(position) == solutionLetter) {
                    position = mQuote.getmLastTry().indexOf(solutionLetter, position + 1);
                }
                onSolutionLetterClicked(position, true);
                letterAtRandom = mQuote.getmEditedRandomLetters().indexOf(solutionLetter);
            }
            mRandomLayout.solveOneLetter(letterAtRandom);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage() + ", Quote: " + mLogoName, e);
            ExceptionManager.getInstance().handleError(ExceptionManager.errorTypes.NAVIGATION, getName(), ((FragmentActivity) getContext()));
        }
    }

    private String getLastTryWithSolvedLetters(String lastTry) {
        String lastTryWithSolvedLetters = "";
        for (int i = 0; i < lastTry.length(); i++) {
            if (isRemovedLetter(lastTry.charAt(i)))
                lastTryWithSolvedLetters += String.valueOf(mLogoName.charAt(i));
            else lastTryWithSolvedLetters += String.valueOf(mQuote.getmLastTry().charAt(i));
        }
        return lastTryWithSolvedLetters;
    }

    public void solveLogoByHelp() {
        try {
            int pos = 0;
            for (AutoResizeTextView solutionLetter : mArrayOfLettersTvs) {
                if (solutionLetter != null) {
                    solutionLetter.setTextColor(LETTER_TEXT_COLOR);
                    solutionLetter.setText(String.valueOf(mLogoName.charAt(pos)));
                }
                pos++;
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage() + ", Quote: " + mLogoName, e);
            ExceptionManager.getInstance().handleError(ExceptionManager.errorTypes.NAVIGATION, getName(), ((FragmentActivity) getContext()));
        }
    }

    public void onRandomLetterSelected(String letter) {
        int position = mQuote.getmLastTry().indexOf(SOLUTION_LETTER_WITHOUT_VALUE);
        mArrayOfLettersTvs.get(position).setText(letter);
        String newLastTry = mQuote.getmLastTry().substring(0, position) + letter + mQuote.getmLastTry().substring(position + 1);
        mQuote.setmLastTry(newLastTry);
        //mLogo.setmLastTry(mLogo.getmLastTry().substring(0, position) + letter + mLogo.getmLastTry().substring(position + 1));
    }

    public boolean isLogoSolvedNow() {
        String lastTryWithSolvedLetters = getLastTryWithSolvedLetters(mQuote.getmLastTry());
        if (!lastTryWithSolvedLetters.contains(String.valueOf(SOLUTION_LETTER_WITHOUT_VALUE))) {
            if (lastTryWithSolvedLetters.equals(mLogoName)) {
                return true;
            }
        }
        return false;
    }

    public void onLogoSolvedChangeLayout() {
        for (AutoResizeTextView solutionLetterTv : mArrayOfLettersTvs) {
            if (solutionLetterTv != null) {
                solutionLetterTv.bringToFront();
                if (!(boolean) (solutionLetterTv.getTag())) {
                    solutionLetterTv.setTextColor(LETTER_TEXT_COLOR);
                    solutionLetterTv.setBackgroundResource(R.drawable.background_letter_solution);
                }
            }
        }
    }

    public int getLayoutHeight() {
        return mHeight;
    }

    private String getName() {
        return getClass().getName();
    }

    private boolean isRemovedLetter(char c) {
        return c == SOLVED_SOLUTION_LETTER_BY_HELP
                || c == SOLUTION_LETTER_REVEAL;
    }
}