package com.oyeapps.quotest.widgets;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.oyeapps.quotest.managers.SoundManager;

import static com.oyeapps.quotest.utils.Consts.SOUND_TYPE;

public class TypeWriter extends AppCompatTextView {

    MediaPlayer mp;

    private CharSequence mText;
    private int mIndex;
    private long mDelay = 500; //Default 500ms delay

    public TypeWriter(Context context) {
        super(context);
    }

    public TypeWriter(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private Handler mHandler = new Handler();
    private Runnable characterAdder = new Runnable() {
        @Override
        public void run() {
            setText(mText.subSequence(0, mIndex++));
            if (mIndex <= mText.length()) {
                mHandler.postDelayed(characterAdder, mDelay);
            } else {
                releaseSound();
            }
        }
    };

    public void releaseSound() {
        if (mp != null)
            mp.release();
    }

    public void animateText(CharSequence text) {
        mText = text;
        mIndex = 0;
        playTypeSound();
        setText("");
        mHandler.removeCallbacks(characterAdder);
        mHandler.postDelayed(characterAdder, mDelay);
    }

    public void playTypeSound() {
        if (!SoundManager.getInstance().ismMuted()) {
            mp = MediaPlayer.create(getContext(), SOUND_TYPE);
            mp.setLooping(true);
            mp.start();
        }
    }

    public void setCharacterDelay(long millis) {
        mDelay = millis;
    }
}
