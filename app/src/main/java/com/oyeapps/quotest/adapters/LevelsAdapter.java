package com.oyeapps.quotest.adapters;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.oyeapps.quotest.BuildConfig;
import com.oyeapps.quotest.R;
import com.oyeapps.quotest.interfaces.ILevel;
import com.oyeapps.quotest.managers.SoundManager;
import com.oyeapps.quotest.type_obj.LevelAd;
import com.oyeapps.quotest.type_obj.LevelLock;
import com.oyeapps.quotest.type_obj.LevelOpen;
import com.oyeapps.quotest.type_obj.LevelSoon;
import com.oyeapps.quotest.utils.Logger;

import java.util.List;

import static com.oyeapps.quotest.utils.Consts.SOUND_LOCK;

/**
 * Created by eldarts on 21/11/2017.
 */

public class LevelsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final static String TAG = LevelsAdapter.class.getSimpleName();

    private List<ILevel> mData;

    public interface LevelsListener {
        void onOpenLevelClicked(int levelNum);

        void onLevelSpecialClick();
    }

    private LevelsAdapter.LevelsListener mListener;

    public LevelsAdapter(LevelsAdapter.LevelsListener listener, List<ILevel> mData) {
        this.mListener = listener;
        this.mData = mData;
    }

    public void updateList(List<ILevel> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    public void onNativeAdLoaded() {

    }

    public void onNativeAdDestory() {
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case ILevel.TYPE_LEVEL_OPEN:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_level_open, parent, false);
                return new OpenLevelViewHolder(itemView);
            case ILevel.TYPE_LEVEL_LOCK:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_level_lock, parent, false);
                return new LockLevelViewHolder(itemView);
            case ILevel.TYPE_LEVEL_SOON:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_level_soon, parent, false);
                return new SoonLevelViewHolder(itemView);
            case ILevel.TYPE_LEVEL_AD:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_level_ad, parent, false);
                return new LevelAdViewHolder(itemView);
            default:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_level_specials, parent, false);
                return new LevelSpecialsViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int position) {
        switch (getItemViewType(position)) {
            case ILevel.TYPE_LEVEL_OPEN:
                ((OpenLevelViewHolder) viewHolder).bindView(position);
                break;
            case ILevel.TYPE_LEVEL_LOCK:
                ((LockLevelViewHolder) viewHolder).bindView(position);
                break;
            case ILevel.TYPE_LEVEL_SOON:
                ((SoonLevelViewHolder) viewHolder).bindView(position);
                break;
            case ILevel.TYPE_LEVEL_SPECIALS:
                ((LevelSpecialsViewHolder) viewHolder).bindView(position);
                break;
            case ILevel.TYPE_LEVEL_AD:
                ((LevelAdViewHolder) viewHolder).bindView(position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mData.get(position).getType();
    }

    @Override
    public int getItemCount() {
        if (mData == null) {
            return 0;
        } else {
            return mData.size();
        }
    }

    private class OpenLevelViewHolder extends RecyclerView.ViewHolder {

        TextView levelNumTv, progressTv;
        RoundCornerProgressBar progressBar;
        RelativeLayout levelCompleteRL, progressRL;

        OpenLevelViewHolder(View itemView) {
            super(itemView);

            levelNumTv = itemView.findViewById(R.id.tv_level_num);
            progressTv = itemView.findViewById(R.id.tv_progress);
            progressBar = itemView.findViewById(R.id.pb_progress);
            levelCompleteRL = itemView.findViewById(R.id.rl_level_completed);
            progressRL = itemView.findViewById(R.id.rl_progress);
        }

        void bindView(final int position) {
            final LevelOpen levelOpen = (LevelOpen) mData.get(position);

            final String level = itemView.getContext().getString(R.string.level, levelOpen.getLevelNum());
            levelNumTv.setText(level);

            if (levelOpen.isLevelCompleted()) {
                levelCompleteRL.setVisibility(View.VISIBLE);
                progressRL.setVisibility(View.GONE);
            } else {
                levelCompleteRL.setVisibility(View.GONE);
                progressRL.setVisibility(View.VISIBLE);
                progressBar.setProgress(levelOpen.getCompletionPercent());
                String progress = levelOpen.getTotalSolvedQuotesCount() + "/" + levelOpen.getTotalQuotesCount();
                progressTv.setText(progress);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onOpenLevelClicked(levelOpen.getLevelNum());
                }
            });
        }
    }

    private class LockLevelViewHolder extends RecyclerView.ViewHolder {

        TextView levelRequirementsTv, levelNumTv;

        LockLevelViewHolder(View itemView) {
            super(itemView);

            levelRequirementsTv = itemView.findViewById(R.id.tv_level_requirements);
            levelNumTv = itemView.findViewById(R.id.tv_level_num);
        }

        void bindView(int position) {
            final LevelLock levelLock = (LevelLock) mData.get(position);

            String level = itemView.getContext().getString(R.string.level, levelLock.getLevelNum());
            levelNumTv.setText(level);

            String quotesToSolve = "<font color=\"#749778\"><bold>" + levelLock.getLogosToOpen() + "</bold></font>";
            String lockedTxt = itemView.getContext().getString(R.string.locked_level_text, quotesToSolve);
            levelRequirementsTv.setText(Html.fromHtml(lockedTxt));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (BuildConfig.DEBUG) {
                        mListener.onOpenLevelClicked(levelLock.getLevelNum());
                    } else {
                        shakeAnimation(view);
                    }
                }
            });
        }
    }

    private class SoonLevelViewHolder extends RecyclerView.ViewHolder {

        TextView levelNumTv;

        SoonLevelViewHolder(View itemView) {
            super(itemView);

            levelNumTv = itemView.findViewById(R.id.tv_level_num);
        }

        void bindView(int position) {
            LevelSoon levelSoon = (LevelSoon) mData.get(position);

            String level = itemView.getContext().getString(R.string.level, levelSoon.getLevelNum());
            levelNumTv.setText(level);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shakeAnimation(view);
                }
            });
        }
    }

    private class LevelSpecialsViewHolder extends RecyclerView.ViewHolder {

        LevelSpecialsViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        void bindView(int position) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onLevelSpecialClick();
                }
            });
        }
    }

    private class LevelAdViewHolder extends ItemVisibilityViewHolder {

        private boolean isAdLoaded = false;

        LevelAdViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        void bindView(final int position) {
            final LevelAd levelAd = (LevelAd) mData.get(position);

            TemplateView template = itemView.findViewById(R.id.my_template);

            final int adHeight = template.getLayoutParams().height;

            Context context = itemView.getContext();
            MobileAds.initialize(context, context.getString(R.string.admob_app_id));
            AdLoader.Builder builder = new AdLoader.Builder(
                    context,
                    context.getString(BuildConfig.DEBUG ? R.string.native_ad_levels_test : R.string.native_ad_levels));

            final int greenColor = ContextCompat.getColor(context, R.color.green1);

            if (!isAdLoaded) {
                setVisibility(false, adHeight);
                builder.forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                    @Override
                    public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                        try {
                            if (isAdLoaded)
                                return;
                            Logger.d(Logger.TEST, TAG, "Native ad loaded");
                            isAdLoaded = true;
                            NativeTemplateStyle styles = new
                                    NativeTemplateStyle.Builder()
                                    .withMainBackgroundColor(new ColorDrawable(greenColor))
                                    .withPrimaryTextBackgroundColor(new ColorDrawable(greenColor))
                                    .withSecondaryTextBackgroundColor(new ColorDrawable(greenColor))
                                    .build();

                            TemplateView template = itemView.findViewById(R.id.my_template);
                            template.setNativeAd(unifiedNativeAd);
                            template.setStyles(styles);
                            setVisibility(true, adHeight);
                        } catch (Exception e) {
                            Logger.e(Logger.TEST, e.getMessage(), e);
                        }
                    }
                }).withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        isAdLoaded = false;
                        setVisibility(false, adHeight);
                    }
                });
            } else {
                setVisibility(true, adHeight);
            }

            AdLoader adLoader = builder.build();
            adLoader.loadAd(new AdRequest.Builder().build());
        }
    }

    private void shakeAnimation(View view) {
        try {
            SoundManager.getInstance().playSound(SOUND_LOCK);
            final Animation shakeAnimation = AnimationUtils.loadAnimation(view.getContext(), R.anim.shake);
            view.startAnimation(shakeAnimation);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }
}
