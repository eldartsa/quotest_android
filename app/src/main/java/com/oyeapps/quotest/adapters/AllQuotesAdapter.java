package com.oyeapps.quotest.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.database.Quote;
import com.oyeapps.quotest.managers.SoundManager;

import java.util.List;

import static com.oyeapps.quotest.utils.Consts.SOUND_CLICK;
import static com.oyeapps.quotest.utils.Consts.STATUS_SOLVED;

public class AllQuotesAdapter extends RecyclerView.Adapter<AllQuotesAdapter.ViewHolder> {

    private List<Quote> mQuotes;

    public interface IOnQuoteClickListener {
        void onQuoteClick(Quote quote, int pos);

        void onQuoteLongClick(Quote quote);
    }

    private IOnQuoteClickListener mListener;

    public AllQuotesAdapter(List<Quote> quotes, IOnQuoteClickListener listener) {
        this.mQuotes = quotes;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public AllQuotesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_badge, parent, false);
        return new ViewHolder(view);
    }

    public void update(List<Quote> quotes) {
        this.mQuotes = quotes;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull final AllQuotesAdapter.ViewHolder holder, final int position) {
        final Quote quote = mQuotes.get(position);

        String quoteNumStr = String.valueOf(position + 1);
        holder.quoteNumTv.setText(quoteNumStr);

        holder.rootCv.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mListener.onQuoteLongClick(quote);
                return true;
            }
        });

        if (quote.getmStatus() == STATUS_SOLVED) {
            holder.rootCv.setAlpha(0.5f);
        } else {
            holder.rootCv.setAlpha(1f);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SoundManager.getInstance().playSound(SOUND_CLICK);
                mListener.onQuoteClick(quote, holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mQuotes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private CardView rootCv;
        private TextView quoteNumTv;

        ViewHolder(View itemView) {
            super(itemView);

            rootCv = itemView.findViewById(R.id.cv_root);
            quoteNumTv = itemView.findViewById(R.id.tv_quote);
        }
    }
}
