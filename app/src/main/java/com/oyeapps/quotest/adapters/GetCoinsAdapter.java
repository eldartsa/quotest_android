package com.oyeapps.quotest.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.interfaces.IGetCoins;
import com.oyeapps.quotest.type_obj.GetCoinsHeader;
import com.oyeapps.quotest.type_obj.GetCoinsInAppBilling;
import com.oyeapps.quotest.type_obj.GetCoinsNormal;
import com.oyeapps.quotest.type_obj.GetCoinsSpecialDeal;

import java.util.List;

import static com.oyeapps.quotest.managers.PurchasesManager.SKU_REMOVE_ADS;
import static com.oyeapps.quotest.utils.MyUtils.dpToPx;

/**
 * Created by eldarts on 21/11/2017.
 */

public class GetCoinsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<IGetCoins> mData;

    public interface IOnNormalClickListener {
        void onNormalClick(GetCoinsNormal.rowType rowTypes);

        void onRankUsClick();
    }

    private IOnNormalClickListener iOnNormalClickListener;

    public interface IOnInAppBillingClickListener {
        void onInAppBillingClick(GetCoinsInAppBilling getCoinsInAppBilling);

        void onSpecialDealClick();
    }

    private IOnInAppBillingClickListener iOnInAppBillingClickListener;

    public GetCoinsAdapter(List<IGetCoins> mData, IOnNormalClickListener iOnNormalClickListener, IOnInAppBillingClickListener iOnInAppBillingClickListener) {
        this.mData = mData;
        this.iOnNormalClickListener = iOnNormalClickListener;
        this.iOnInAppBillingClickListener = iOnInAppBillingClickListener;
    }

    public void update(List<IGetCoins> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    public void onRemoveAds() {
        for (IGetCoins iGetCoins : mData) {
            if (iGetCoins instanceof GetCoinsInAppBilling) {
                GetCoinsInAppBilling getCoinsInAppBilling = (GetCoinsInAppBilling) iGetCoins;
                if (getCoinsInAppBilling.getProductId().equals(SKU_REMOVE_ADS)) {
                    getCoinsInAppBilling.setVisible(false);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void onPurchaseSpecials() {
        for (IGetCoins iGetCoins : mData) {
            if (iGetCoins instanceof GetCoinsSpecialDeal) {
                GetCoinsSpecialDeal getCoinsSpecialDeal = (GetCoinsSpecialDeal) iGetCoins;
                getCoinsSpecialDeal.setVisible(false);
            }
        }
        notifyDataSetChanged();
    }

    public void onNormalRowTypeRewarded(GetCoinsNormal.rowType rowType) {
        for (IGetCoins iGetCoins : mData) {
            if (iGetCoins instanceof GetCoinsNormal) {
                GetCoinsNormal getCoinsNormal = (GetCoinsNormal) iGetCoins;
                if (getCoinsNormal.getRowType() == rowType) {
                    getCoinsNormal.setRewarded(true);
                }
            }
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case IGetCoins.TYPE_HEADER:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_get_coins_header, parent, false);
                return new HeaderViewHolder(itemView);
            case IGetCoins.TYPE_IN_APP_BILLING:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_get_coins_in_app_billing, parent, false);
                return new InAppBillingViewHolder(itemView);
            case IGetCoins.TYPE_NORMAL:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_get_coins_normal, parent, false);
                return new NormalViewHolder(itemView);
            case IGetCoins.TYPE_SPECIAL_DEAL:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_get_coins_special_deal, parent, false);
                return new SpecialDealViewHolder(itemView);
            default:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_get_coins_rank_us, parent, false);
                return new RankUsViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int position) {
        switch (getItemViewType(position)) {
            case IGetCoins.TYPE_HEADER:
                ((HeaderViewHolder) viewHolder).bindView(position);
                break;
            case IGetCoins.TYPE_IN_APP_BILLING:
                ((InAppBillingViewHolder) viewHolder).bindView(position);
                break;
            case IGetCoins.TYPE_NORMAL:
                ((NormalViewHolder) viewHolder).bindView(position);
                break;
            case IGetCoins.TYPE_RANK_US:
                ((RankUsViewHolder) viewHolder).bindView(position);
                break;
            case IGetCoins.TYPE_SPECIAL_DEAL:
                ((SpecialDealViewHolder) viewHolder).bindView(position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mData.get(position).getType();
    }

    @Override
    public int getItemCount() {
        if (mData == null) {
            return 0;
        } else {
            return mData.size();
        }
    }

    private class HeaderViewHolder extends ItemVisibilityViewHolder {

        TextView titleTv;

        HeaderViewHolder(View itemView) {
            super(itemView);

            titleTv = itemView.findViewById(R.id.tv_title);
        }

        void bindView(final int position) {
            GetCoinsHeader getCoinsHeader = (GetCoinsHeader) mData.get(position);

            titleTv.setText(getCoinsHeader.getTitle());
        }
    }

    private class InAppBillingViewHolder extends ItemVisibilityViewHolder {

        CardView rootCv;
        TextView descTv, priceTv;
        ImageView imageView;

        InAppBillingViewHolder(View itemView) {
            super(itemView);

            descTv = itemView.findViewById(R.id.tv_desc);
            priceTv = itemView.findViewById(R.id.tv_price);
            imageView = itemView.findViewById(R.id.iv);
            rootCv = itemView.findViewById(R.id.cv_root);
        }

        void bindView(final int position) {
            final GetCoinsInAppBilling getCoinsInAppBilling = (GetCoinsInAppBilling) mData.get(position);

            if (getCoinsInAppBilling.isVisible()) {
                setVisibility(true, dpToPx(60));
            } else {
                setVisibility(false, dpToPx(60));
            }

            descTv.setText(getCoinsInAppBilling.getDesc());
            priceTv.setText(getCoinsInAppBilling.getPrice());
            imageView.setImageResource(getCoinsInAppBilling.getImgRes());

            rootCv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iOnInAppBillingClickListener.onInAppBillingClick(getCoinsInAppBilling);
                }
            });
        }
    }

    private class NormalViewHolder extends RecyclerView.ViewHolder {

        CardView rootCv;
        TextView descTv, rewardAmountTv;
        ImageView imageView;

        NormalViewHolder(View itemView) {
            super(itemView);

            descTv = itemView.findViewById(R.id.tv_desc);
            rewardAmountTv = itemView.findViewById(R.id.tv_reward_amount);
            imageView = itemView.findViewById(R.id.iv);
            rootCv = itemView.findViewById(R.id.cv_root);
        }

        void bindView(final int position) {
            final GetCoinsNormal getCoinsNormal = (GetCoinsNormal) mData.get(position);

            descTv.setText(getCoinsNormal.getDesc());
            rewardAmountTv.setText(getCoinsNormal.getRewardAmount());
            imageView.setImageResource(getCoinsNormal.getImgRes());
            rootCv.setCardBackgroundColor(getCoinsNormal.getBgColor());

            if (getCoinsNormal.isRewarded() && (getCoinsNormal.getRowType() == GetCoinsNormal.rowType.FACEBOOK
                    || getCoinsNormal.getRowType() == GetCoinsNormal.rowType.INSTAGRAM
                    || getCoinsNormal.getRowType() == GetCoinsNormal.rowType.DOWNLOAD_LOGOTEST)) {
                rootCv.setAlpha(0.5f);
            } else {
                rootCv.setAlpha(1f);
            }
            rootCv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iOnNormalClickListener.onNormalClick(getCoinsNormal.getRowType());
                }
            });
        }
    }

    private class RankUsViewHolder extends ItemVisibilityViewHolder {

        CardView rootCv;

        RankUsViewHolder(@NonNull View itemView) {
            super(itemView);

            rootCv = itemView.findViewById(R.id.cv_root);
        }

        void bindView(final int position) {
            rootCv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iOnNormalClickListener.onRankUsClick();
                }
            });
        }
    }

    private class SpecialDealViewHolder extends ItemVisibilityViewHolder {

        CardView rootCv;

        SpecialDealViewHolder(@NonNull View itemView) {
            super(itemView);

            rootCv = itemView.findViewById(R.id.cv_root);
        }

        void bindView(final int position) {
            final GetCoinsSpecialDeal getCoinsSpecialDeal = (GetCoinsSpecialDeal) mData.get(position);

            if (getCoinsSpecialDeal.isVisible()) {
                setVisibility(true, dpToPx(160));
            } else {
                setVisibility(false, dpToPx(160));
            }

            rootCv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iOnInAppBillingClickListener.onSpecialDealClick();
                }
            });
        }
    }
}
