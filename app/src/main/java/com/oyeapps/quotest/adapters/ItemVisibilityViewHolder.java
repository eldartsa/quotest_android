package com.oyeapps.quotest.adapters;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static com.oyeapps.quotest.utils.MyUtils.dpToPx;

public class ItemVisibilityViewHolder extends RecyclerView.ViewHolder {

    ItemVisibilityViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public void setVisibility(boolean isVisible, int height) {
        RecyclerView.LayoutParams param = (RecyclerView.LayoutParams) itemView.getLayoutParams();
        if (isVisible) {
            param.height = height;
            param.width = RecyclerView.LayoutParams.MATCH_PARENT;
            param.topMargin = dpToPx(5);
            param.bottomMargin = dpToPx(5);
            itemView.setVisibility(View.VISIBLE);
        } else {
            param.height = 0;
            param.width = 0;
            param.topMargin = dpToPx(0);
            param.bottomMargin = dpToPx(0);
            itemView.setVisibility(View.GONE);
        }
        itemView.setLayoutParams(param);
    }
}
