package com.oyeapps.quotest.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.bumptech.glide.Glide;
import com.oyeapps.quotest.R;
import com.oyeapps.quotest.database.SpecialLevel;

import java.util.List;

public class SpecialLevelsAdapter extends RecyclerView.Adapter<SpecialLevelsAdapter.ViewHolder> {

    private List<SpecialLevel> mData;

    public interface IOnSpecialLevelClickListener {
        void onLevelClick(SpecialLevel specialLevel);
    }

    private IOnSpecialLevelClickListener iOnSpecialLevelClickListener;

    public SpecialLevelsAdapter(List<SpecialLevel> mData, IOnSpecialLevelClickListener iOnSpecialLevelClickListener) {
        this.mData = mData;
        this.iOnSpecialLevelClickListener = iOnSpecialLevelClickListener;
    }

    public void updateList(List<SpecialLevel> specialLevelList) {
        mData = specialLevelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_special, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final SpecialLevel specialLevel = mData.get(position);

        holder.newIv.setVisibility(specialLevel.isLevelNew() ? View.VISIBLE : View.GONE);

        float completionPercent = ((float) specialLevel.getTotalSolvedQuotes() / specialLevel.getTotalQuotes()) * 100;
        holder.progressBar.setProgress(completionPercent);
        String progress = specialLevel.getTotalSolvedQuotes() + "/" + specialLevel.getTotalQuotes();
        holder.progressTv.setText(progress);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iOnSpecialLevelClickListener.onLevelClick(specialLevel);
            }
        });

        Glide.with(holder.itemView.getContext())
                .load(specialLevel.getImageUrl())
                .into(holder.levelIv);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView progressTv;
        ImageView newIv, levelIv;
        RoundCornerProgressBar progressBar;

        ViewHolder(View itemView) {
            super(itemView);

            progressTv = itemView.findViewById(R.id.tv_progress);
            newIv = itemView.findViewById(R.id.iv_new);
            progressBar = itemView.findViewById(R.id.pb_progress);
            levelIv = itemView.findViewById(R.id.iv_level);

        }
    }
}
