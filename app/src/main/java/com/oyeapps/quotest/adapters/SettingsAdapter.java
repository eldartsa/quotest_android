package com.oyeapps.quotest.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.interfaces.IGetCoins;
import com.oyeapps.quotest.interfaces.ISettings;
import com.oyeapps.quotest.type_obj.SettingsHeader;
import com.oyeapps.quotest.type_obj.SettingsNormal;
import com.oyeapps.quotest.utils.Logger;

import java.util.List;

/**
 * Created by eldarts on 21/11/2017.
 */

public class SettingsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ISettings> mData;

    public interface IOnNormalClickListener {
        void onNormalClick(SettingsNormal.RowType rowType);
    }

    private IOnNormalClickListener iOnNormalClickListener;

    public SettingsAdapter(List<ISettings> mData, IOnNormalClickListener iOnNormalClickListener) {
        this.mData = mData;
        this.iOnNormalClickListener = iOnNormalClickListener;
    }

    public void updateSound(boolean sound) {
        try {
            ISettings iSettings = mData.get(0);
            if (iSettings instanceof SettingsNormal) {
                SettingsNormal settingsNormal = (SettingsNormal) iSettings;
                settingsNormal.setIconRes(sound ? R.drawable.ic_sound_on : R.drawable.ic_sound_off);
            }
            notifyItemChanged(0);
        } catch (Exception e){
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case IGetCoins.TYPE_HEADER:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_settings_header, parent, false);
                return new HeaderViewHolder(itemView);
            default:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_settings_normal, parent, false);
                return new NormalViewHolder(itemView);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int position) {
        switch (getItemViewType(position)) {
            case ISettings.TYPE_HEADER:
                ((HeaderViewHolder) viewHolder).bindView(position);
                break;
            case ISettings.TYPE_NORMAL:
                ((NormalViewHolder) viewHolder).bindView(position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mData.get(position).getType();
    }

    @Override
    public int getItemCount() {
        if (mData == null) {
            return 0;
        } else {
            return mData.size();
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        TextView titleTv;

        HeaderViewHolder(View itemView) {
            super(itemView);

            titleTv = itemView.findViewById(R.id.tv_title);
        }

        void bindView(final int position) {
            SettingsHeader settingsHeader = (SettingsHeader) mData.get(position);

            titleTv.setText(settingsHeader.getTitle());
        }
    }

    private class NormalViewHolder extends RecyclerView.ViewHolder {

        CardView rootCv;
        TextView titleTv;
        ImageView iconIv;

        NormalViewHolder(View itemView) {
            super(itemView);

            rootCv = itemView.findViewById(R.id.cv_root);
            titleTv = itemView.findViewById(R.id.tv_title);
            iconIv = itemView.findViewById(R.id.iv_icon);
        }

        void bindView(final int position) {
            final SettingsNormal settingsNormal = (SettingsNormal) mData.get(position);

            titleTv.setText(settingsNormal.getTitle());
            if (settingsNormal.isShouldShowIcon()) {
                iconIv.setVisibility(View.VISIBLE);
                iconIv.setImageResource(settingsNormal.getIconRes());
            } else {
                iconIv.setVisibility(View.GONE);
            }
            rootCv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iOnNormalClickListener.onNormalClick(settingsNormal.getRowType());
                }
            });
        }
    }
}
