package com.oyeapps.quotest.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.managers.PurchasesManager;
import com.oyeapps.quotest.managers.SoundManager;
import com.oyeapps.quotest.utils.Logger;

import static com.oyeapps.quotest.utils.Consts.SOUND_CLICK;

public class PurchaseSpecialsDialog extends Dialog implements View.OnClickListener {

    public interface SubscriptionDialogListener {
        void onStartSubscriptionClicked();
    }

    private SubscriptionDialogListener mListener;

    public PurchaseSpecialsDialog(Context context) {
        super(context);
    }

    public void setmListener(SubscriptionDialogListener mListener) {
        this.mListener = mListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_purchase_specials);
            if (getWindow() != null) {
                getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
                getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }

            initView();
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void initView() {
        LinearLayout purchaseLl = findViewById(R.id.ll_purchase);
        ImageView closeIv = findViewById(R.id.iv_close);
        TextView buyNowTv = findViewById(R.id.tv_buy_now);

        buyNowTv.setText(getContext().getString(R.string.buy_now_for, PurchasesManager.getInstance().getGetCoinsSpecialDeal().getPrice()));

        purchaseLl.setOnClickListener(this);
        closeIv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        try {
            SoundManager.getInstance().playSound(SOUND_CLICK);
            switch (v.getId()) {
                case R.id.ll_purchase:
                    dismiss();
                    mListener.onStartSubscriptionClicked();
                    break;
                case R.id.iv_close:
                    dismiss();
                    break;
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

}
