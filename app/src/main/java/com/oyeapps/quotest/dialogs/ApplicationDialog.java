package com.oyeapps.quotest.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.managers.SoundManager;

import java.util.Objects;

import static com.oyeapps.quotest.utils.Consts.SOUND_CLICK;

public class ApplicationDialog extends Dialog implements View.OnClickListener {

    private String mTitle, mMessage, mPositiveTxt, mNegativeTxt;

    public interface ApplicationDialogListener {
        void onPositiveClicked(ApplicationDialog dialog);

        void onNegativeClicked(ApplicationDialog dialog);
    }

    private ApplicationDialogListener listener;

    public ApplicationDialog(Context mContext, String mTitle, String mMessage, String mPositiveTxt, String mNegativeTxt) {
        super(mContext);
        this.mTitle = mTitle;
        this.mMessage = mMessage;
        this.mPositiveTxt = mPositiveTxt;
        this.mNegativeTxt = mNegativeTxt;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_app);
        Objects.requireNonNull(getWindow()).setBackgroundDrawableResource(android.R.color.transparent);

        initView();
    }

    private void initView() {
        TextView titleTv = findViewById(R.id.tv_title);
        TextView messageTv = findViewById(R.id.tv_message);
        TextView positiveTv = findViewById(R.id.tv_positive);
        TextView negativeTv = findViewById(R.id.tv_negative);

        titleTv.setText(mTitle);
        messageTv.setText(mMessage);
        positiveTv.setText(mPositiveTxt);
        negativeTv.setText(mNegativeTxt);

        titleTv.setVisibility(mTitle.isEmpty() ? View.GONE : View.VISIBLE);
        messageTv.setVisibility(mMessage.isEmpty() ? View.GONE : View.VISIBLE);
        positiveTv.setVisibility(mPositiveTxt.isEmpty() ? View.GONE : View.VISIBLE);
        negativeTv.setVisibility(mNegativeTxt.isEmpty() ? View.GONE : View.VISIBLE);

        positiveTv.setOnClickListener(this);
        negativeTv.setOnClickListener(this);

        positiveTv.setBackgroundResource(negativeTv.getVisibility() == View.VISIBLE
                ? R.drawable.background_dialog_positive : R.drawable.background_dialog_neutral);
    }

    public void setOnClickListener(ApplicationDialogListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        SoundManager.getInstance().playSound(SOUND_CLICK);
        if (listener == null) {
            dismiss();
            return;
        }
        switch (view.getId()) {
            case R.id.tv_positive:
                listener.onPositiveClicked(this);
                break;
            case R.id.tv_negative:
                listener.onNegativeClicked(this);
                break;
            default:
                break;
        }
    }
}
