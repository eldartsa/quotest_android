package com.oyeapps.quotest.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import com.oyeapps.quotest.R;

public class AppLoader extends Dialog {

    public AppLoader(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(false);
        setContentView(R.layout.loader);
        if (getWindow() != null)
            getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }
}
