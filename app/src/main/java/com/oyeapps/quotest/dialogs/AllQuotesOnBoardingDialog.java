package com.oyeapps.quotest.dialogs;

import android.app.Dialog;
import android.content.Context;

import androidx.annotation.NonNull;

import com.oyeapps.quotest.R;

public class AllQuotesOnBoardingDialog extends Dialog {
    public AllQuotesOnBoardingDialog(@NonNull Context context) {
        super(context);
    }

    private void init() {
        setContentView(R.layout.view_on_boarding_all_logos_fragment);
    }

}
