package com.oyeapps.quotest.activities;

import android.os.Bundle;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.interfaces.IOnActionBarChangeListener;
import com.oyeapps.quotest.managers.AdsManager;
import com.oyeapps.quotest.managers.NavigationManager;

/**
 * Created by eldarts on 13/08/2017.
 */

public class SettingActivity extends BaseActivity implements IOnActionBarChangeListener {


    @Override
    protected int getActivityLayout() {
        return R.layout.activity_setting;
    }

    @Override
    protected void initActivityView(Bundle savedInstanceState) {
        mAdView = findViewById(R.id.adView);
        AdsManager.getInstance().loadBannerAd(this, mAdView);

        NavigationManager.getInstance().goToSettingFragment(this);
    }

    @Override
    public void handleBackPress(boolean isFromGoToScreen, boolean isMainFragmentInActivity) {
        if (isMainFragmentInActivity) {
            NavigationManager.getInstance().goToMainActivity(this);
        } else {
            super.handleBackPress(isFromGoToScreen, false);
        }
    }
}
