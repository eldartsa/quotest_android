package com.oyeapps.quotest.activities;

import android.os.Bundle;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.managers.NavigationManager;

public class InviteFriendActivity extends BaseActivity {

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_invite_friend;
    }

    @Override
    protected void initActivityView(Bundle savedInstanceState) {
        NavigationManager.getInstance().goToInviteFriendFragment(this);
    }

    @Override
    public void handleBackPress(boolean isFromGoToScreen, boolean isMainFragmentInActivity) {
        if (isMainFragmentInActivity && isFromGoToScreen) {
            NavigationManager.getInstance().goToMainActivity(this);
        } else {
            super.handleBackPress(isFromGoToScreen, isMainFragmentInActivity);
        }
    }
}
