package com.oyeapps.quotest.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.transition.Slide;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.gms.ads.AdView;
import com.oyeapps.quotest.R;
import com.oyeapps.quotest.data_obj.ActionBarItem;
import com.oyeapps.quotest.dialogs.AppLoader;
import com.oyeapps.quotest.fragments.BaseFragment;
import com.oyeapps.quotest.fragments.BasePurchasesFragment;
import com.oyeapps.quotest.interfaces.IOnActionBarChangeListener;
import com.oyeapps.quotest.managers.DataManager;
import com.oyeapps.quotest.managers.ExceptionManager;
import com.oyeapps.quotest.managers.GamesServicesManager;
import com.oyeapps.quotest.managers.NavigationManager;
import com.oyeapps.quotest.managers.SoundManager;
import com.oyeapps.quotest.utils.Logger;
import com.oyeapps.quotest.utils.MyUtils;
import com.oyeapps.quotest.widgets.LocaleUtils;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;

import static com.oyeapps.quotest.managers.GamesServicesManager.RC_SIGN_IN;
import static com.oyeapps.quotest.utils.Consts.SOUND_CLICK;

/**
 * Created by Eldar Tsafar on 13/10/2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements ExceptionManager.ExceptionManagerHolder, IOnActionBarChangeListener, View.OnClickListener {

    public final String name = getClass().getName();
    public final String simpleName = getClass().getSimpleName();

    private Realm realm;

    private boolean mIsActivityRunning = false;

    private long startMilliseconds = 0;

    private AppLoader mLoader;
    protected AdView mAdView;

    protected abstract int getActivityLayout();

    protected void initBundleData(Bundle bundle) {
    }

    protected abstract void initActivityView(Bundle savedInstanceState);

    public void handleBackPress(boolean isFromGoToScreen, boolean isMainFragmentInActivity) {
        getOnBackPressedDispatcher().onBackPressed();
    }

    protected ExceptionManager.viewErrorType getViewErrorType() {
        return ExceptionManager.viewErrorType.NAVIGATION;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            //setAnimation();
            setContentView(getActivityLayout());
            getExceptionManager().addActivityToList(name, getViewErrorType());
            Logger.d(Logger.TEST, simpleName, "onCreate");

            realm = Realm.getDefaultInstance();

            mLoader = new AppLoader(this);

            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread t, Throwable e) {
                    Logger.e(Logger.TEST, e.getMessage(), e);
                }
            });

            Bundle bundle = MyUtils.getNonNullBundle(getIntent().getExtras());
            initBundleData(bundle);
            initActivityView(savedInstanceState);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
            getExceptionManager().handleError(ExceptionManager.errorTypes.TERMINAL, name, this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (realm != null && !realm.isClosed()) {
                realm.close();
            }
            if (mAdView != null) {
                mAdView.destroy();
            }
            dismissLoader();
            getExceptionManager().removeActivityFromList(name, getViewErrorType());
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public BaseActivity() {
        LocaleUtils.updateConfig(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTimer();
    }

    public void startTimer() {
        startMilliseconds = Calendar.getInstance().getTimeInMillis();
    }

    public void stopTimer() {
        long end = Calendar.getInstance().getTimeInMillis();
        long secs = TimeUnit.MILLISECONDS.toSeconds(end - startMilliseconds);
        DataManager.getInstance().addToGameTime(secs);
    }

    public Realm getRealm() {
        return realm;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mIsActivityRunning = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mIsActivityRunning = false;
    }

    @Override
    public ExceptionManager getExceptionManager() {
        return ExceptionManager.getInstance();
    }

    public boolean ismIsActivityRunning() {
        return mIsActivityRunning;
    }

    public void showLoader() {
        if (mLoader != null)
            mLoader.show();
    }

    public void dismissLoader() {
        if (mLoader != null)
            mLoader.dismiss();
    }

    @Override
    public void onActionBarStateChange(ActionBarItem actionBarItem) {
        View header = findViewById(R.id.header);
        if (actionBarItem != null) {
            header.setVisibility(View.VISIBLE);

            TextView titleTv = findViewById(R.id.tv_title);
            TextView coinsTv = findViewById(R.id.tv_coins);
            ImageView backIv = findViewById(R.id.iv_back);

            backIv.setVisibility(actionBarItem.isShouldShowBack() ? View.VISIBLE : View.GONE);
            titleTv.setText(actionBarItem.getTitle());
            coinsTv.setText(actionBarItem.getCoins());

            backIv.setOnClickListener(this);
            coinsTv.setOnClickListener(this);
        } else {
            header.setVisibility(View.GONE);
        }
    }

    public TextView getActionBarCoinsTv() {
        return (TextView) findViewById(R.id.tv_coins);
    }

    @Override
    public void onClick(View v) {
        SoundManager.getInstance().playSound(SOUND_CLICK);
        switch (v.getId()) {
            case R.id.tv_coins:
                NavigationManager.getInstance().goToCoinsActivity(this);
                break;
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == RC_SIGN_IN) {
                GamesServicesManager.onSignInActivityResult(this, data);
            }
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment);
            if (fragment instanceof BasePurchasesFragment) {
                BasePurchasesFragment basePurchasesFragment = (BasePurchasesFragment) fragment;
                basePurchasesFragment.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            SoundManager.getInstance().playSound(SOUND_CLICK);
        }
        return super.onKeyDown(keyCode, event);
    }

    public void setAnimation() {
        if (Build.VERSION.SDK_INT > 20) {
            Slide slide = new Slide();
            slide.setSlideEdge(Gravity.START);
            slide.setDuration(400);
            slide.setInterpolator(new DecelerateInterpolator());
            getWindow().setExitTransition(slide);
            getWindow().setEnterTransition(slide);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment);
        if (fragment instanceof BaseFragment) {
            if (NavigationManager.getInstance().isCurrentFragmentHandleBackPress(fragment))
                return;
            NavigationManager.getInstance().navigateBack(this);
        }
    }
}
