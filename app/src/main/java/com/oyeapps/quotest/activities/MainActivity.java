package com.oyeapps.quotest.activities;

import android.os.Bundle;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.managers.GeneralManager;
import com.oyeapps.quotest.managers.NavigationManager;

public class MainActivity extends BaseActivity {

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void initActivityView(Bundle savedInstanceState) {
        GeneralManager.getInstance().checkRewardForInvitees(this);
        GeneralManager.getInstance().checkForAppNewUpdateAvailable(this);
        NavigationManager.getInstance().goToMainFragment(this);
    }

    @Override
    public void handleBackPress(boolean isFromGoToScreen, boolean isMainFragmentInActivity) {
        finishAffinity();
    }
}