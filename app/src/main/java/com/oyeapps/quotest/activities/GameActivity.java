package com.oyeapps.quotest.activities;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.jinatonic.confetti.CommonConfetti;
import com.google.android.gms.ads.InterstitialAd;
import com.oyeapps.quotest.R;
import com.oyeapps.quotest.database.MyDatabase;
import com.oyeapps.quotest.interfaces.IInterstitialAdListener;
import com.oyeapps.quotest.interfaces.ILogoSolvedListener;
import com.oyeapps.quotest.managers.AdsManager;
import com.oyeapps.quotest.managers.GeneralManager;
import com.oyeapps.quotest.managers.NavigationManager;
import com.oyeapps.quotest.managers.SoundManager;
import com.oyeapps.quotest.utils.AnimUtils;
import com.oyeapps.quotest.utils.DialogUtils;
import com.oyeapps.quotest.utils.Logger;

import static com.oyeapps.quotest.utils.Consts.SOUND_CHEERS;
import static com.oyeapps.quotest.utils.MyUtils.slideUpFromScreen;

/**
 * Created by Eldar Tsafar on 12/08/2017.
 */

public class GameActivity extends BaseActivity implements IInterstitialAdListener, ILogoSolvedListener {

    private static final int CONFETTI_TIME = 5000;

    private InterstitialAd mInterstitialAd;
    private TextView mNewLevelOpenedTv;
    private RelativeLayout mRootLayout;

    private Handler mHandler = new Handler();

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_game;
    }

    @Override
    protected void initActivityView(Bundle savedInstanceState) {
        mRootLayout = findViewById(R.id.root);
        mNewLevelOpenedTv = findViewById(R.id.tv_level_opened);
        mAdView = findViewById(R.id.adView);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
        AdsManager.getInstance().loadLogosActivityInterstitialAd(this, mInterstitialAd);

        AdsManager.getInstance().loadBannerAd(this, mAdView);

        NavigationManager.getInstance().goToLevelsFragment(this);
    }

    private void setAndShowOpenedLevelView(int levelOpened) {
        try {
            String text = getString(R.string.new_level_opened_text, levelOpened + 1);
            mNewLevelOpenedTv.setText(text);
            mNewLevelOpenedTv.setVisibility(View.VISIBLE);
            AnimUtils.slideDownAnimation(mNewLevelOpenedTv);
            startConffeti();
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        slideUpFromScreen(mNewLevelOpenedTv, GameActivity.this);
                    } catch (Exception ignored) {
                    }
                }
            }, CONFETTI_TIME);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void newLevelOpenedCheck() {
        try {
            final int levelOpened = MyDatabase.getInstance().isNewLevelOpened(getRealm());
            if (levelOpened != -1) {
                SoundManager.getInstance().playSound(SOUND_CHEERS);
                setAndShowOpenedLevelView(levelOpened);
            }
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    private void startConffeti() {
        int[] colors = {getResources().getColor(R.color.lt_orange), getResources().getColor(R.color.lt_pink),
                getResources().getColor(R.color.lt_purple), getResources().getColor(R.color.lt_yellow)};
        CommonConfetti.rainingConfetti(mRootLayout, colors)
                .oneShot();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            mHandler.removeCallbacksAndMessages(null);
        } catch (Exception e) {
            Logger.e(Logger.TEST, e.getMessage(), e);
        }
    }

    public void levelOrGameCompletedCheck(final int level) {
        if (MyDatabase.getInstance().isGameCompleted(getRealm())) {
            GeneralManager.getInstance().onGameFinished(this);
            startConffeti();
        } else if (MyDatabase.getInstance().isLevelCompleted(getRealm(), level)) {
            DialogUtils.showLevelCompletedDialog(this, level);
        }
    }

    @Override
    public void showInterstitialAd() {
        AdsManager.getInstance().showLogosActivityInterstitialAd(mInterstitialAd, ismIsActivityRunning());
    }

    @Override
    public void onLogoSolved(int level) {
        newLevelOpenedCheck();
        levelOrGameCompletedCheck(level);
    }

    @Override
    public void handleBackPress(boolean isFromGoToScreen, boolean isMainFragmentInActivity) {
        if (isMainFragmentInActivity) {
            NavigationManager.getInstance().goToMainActivity(this);
        } else {
            super.handleBackPress(isFromGoToScreen, false);
        }
    }
}