package com.oyeapps.quotest.activities;

import android.os.Bundle;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.managers.NavigationManager;

public class CoinsActivity extends BaseActivity {
    @Override
    protected int getActivityLayout() {
        return R.layout.activity_coins;
    }

    @Override
    protected void initActivityView(Bundle savedInstanceState) {
        NavigationManager.getInstance().goToCoinsFragment(this);
    }

    @Override
    public void handleBackPress(boolean isFromGoToScreen, boolean isMainFragmentInActivity) {
        if (isMainFragmentInActivity && isFromGoToScreen) {
            NavigationManager.getInstance().goToMainActivity(this);
        } else {
            super.handleBackPress(isFromGoToScreen, isMainFragmentInActivity);
        }
    }
}
