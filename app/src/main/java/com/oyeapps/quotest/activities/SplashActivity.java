package com.oyeapps.quotest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.oyeapps.quotest.R;
import com.oyeapps.quotest.managers.ExceptionManager;
import com.oyeapps.quotest.managers.GeneralManager;
import com.oyeapps.quotest.managers.NavigationManager;
import com.oyeapps.quotest.utils.Logger;
import com.oyeapps.quotest.utils.TextUtils;

import static com.oyeapps.quotest.utils.Consts.NOTIFICATION_REWARD_CODE;
import static com.oyeapps.quotest.utils.Consts.NOTIFICATION_TO_SCREEN_CODE;
import static com.oyeapps.quotest.utils.Consts.NOTIFICATION_URL_CODE;

public class SplashActivity extends BaseActivity {

    private String mRewardAmount = "0";
    private String mToScreenCode = "0";
    private String mPushUrl = "";

    private TextView mVersionTv;

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initActivityView(Bundle savedInstanceState) {
        mVersionTv = findViewById(R.id.tv_version);

        mVersionTv.setText(TextUtils.getAppVersionsText());

        GeneralManager.getInstance().notificationRewardCheck(mRewardAmount);
        NavigationManager.getInstance().setmScreenCode(mToScreenCode, mPushUrl);
        GeneralManager.getInstance().onAppCreation(this);
    }

    @Override
    protected ExceptionManager.viewErrorType getViewErrorType() {
        return ExceptionManager.viewErrorType.TERMINAL;
    }

    @Override
    protected void initBundleData(Bundle bundle) {
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) != 0) {
            Logger.d(Logger.TEST, getClass().getSimpleName(), "Called from history");
        } else {
            mRewardAmount = bundle.getString(NOTIFICATION_REWARD_CODE, "0");
            mToScreenCode = bundle.getString(NOTIFICATION_TO_SCREEN_CODE, "0");
            mPushUrl = bundle.getString(NOTIFICATION_URL_CODE, "");
            getIntent().removeExtra(NOTIFICATION_REWARD_CODE);
            getIntent().removeExtra(NOTIFICATION_TO_SCREEN_CODE);
            getIntent().removeExtra(NOTIFICATION_URL_CODE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mVersionTv = null;
    }
}
