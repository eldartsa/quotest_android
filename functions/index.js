const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);

const firestoreDb = admin.firestore();

exports.addInviteeToUser = functions.firestore
    .document('Users/{deviceId}')
    .onCreate((snap, context) => {

      const deviceId = snap.data().udid;
      const referredBy = snap.data().referred_by;

      var dbRef = firestoreDb.collection('Users');

      // perform desired operations ...

      if(referredBy){
        const usersRef = dbRef.doc(referredBy);
        let getDoc = usersRef.get()
          .then(doc => {
            if (!doc.exists) {
              console.log('No such document!');
              let referrerUser = {
                              udid: referredBy,
                              referred_by: ''
                          };
              usersRef.set(referrerUser).then(ref => {
                  console.log('User added successfully');
                  addInvitee(referredBy, deviceId, dbRef);
                  return null;
                    }).catch(err => {
                        console.log('Error adding user', err);
                        return null;
                    });
              return null;
            } else {
              console.log('Document data:', doc.data());
              addInvitee(referredBy, deviceId, dbRef);
              return null;
            }
          })
          .catch(err => {
            console.log('Error getting document', err);
            return null;
          });
      }
      return;
    });

function addInvitee(referrer, invitee, dbRef){
            dbRef.doc(referrer).collection('invitees').doc(invitee).set({
            udid: invitee,
            is_rewarded: false });
            return;
}

exports.addInviteeToUserQa = functions.firestore
    .document('UsersQa/{deviceId}')
    .onCreate((snap, context) => {

      const deviceId = snap.data().udid;
      const referredBy = snap.data().referred_by;

      var dbRef = firestoreDb.collection('UsersQa');

      // perform desired operations ...

      if(referredBy){
        const usersRef = dbRef.doc(referredBy);
        let getDoc = usersRef.get()
          .then(doc => {
            if (!doc.exists) {
              console.log('No such document!');
              let referrerUser = {
                              udid: referredBy,
                              referred_by: ''
                          };
              usersRef.set(referrerUser).then(ref => {
                  console.log('User added successfully');
                  addInvitee(referredBy, deviceId, dbRef);
                  return null;
                    }).catch(err => {
                        console.log('Error adding user', err);
                        return null;
                    });
              return null;
            } else {
              console.log('Document data:', doc.data());
              addInvitee(referredBy, deviceId, dbRef);
              return null;
            }
          })
          .catch(err => {
            console.log('Error getting document', err);
            return null;
          });
      }
      return;
    });